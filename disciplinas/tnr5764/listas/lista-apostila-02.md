# TNR5764 -- Fundamentos de Tecnologia Nuclear -- Reatores

1) Quais são os reatores de pesquisa do Brasil? Onde estão localizados? Qual é
a potência térmica máxima de operação de cada um deles? Dentre esses reatores,
qual foi o único projetado e construído inteiramente com tecnologia nacional?

* IEA-R1     -- 1957 -- IPEN  -- São Paulo      -- 5MW
* IPR-R1     -- 1960 -- IPR   -- Belo Horizonte -- 100kW
* Argonauta  -- 1965 -- IEN   -- Rio de Janeiro
* IPEN/MB-01 -- 1988 -- CTMSP -- São Paulo

O único com tecnologia inteiramente nacional é o IPEN/MB-01.

---

2) Em relação ao reator IEA-R1 pergunta-se:

a) o tipo de reator de pesquisa;

Tipo piscina.

b) o material de revestimento dos elementos combustíveis;

Alumínio.

c) os tipos de combustível nuclear utilizados atualmente;

Ligas metálicas de Urânio-Alumínio ou compostos de Urânio dispersos em
Alumínio.

d) a composição dos elementos refletores;

Blocos de grafite revestidos em Alumínio ou blocos de Berílio.

e) para que serve a água da piscina;

Tanto como moderador de nêutrons quanto refrigerante da atividade radioativa.

f) o material usado nas barras de controle;

Ligas de Prata-Índio-Cádmio.

g) a atual potência térmica máxima de operação.

5MW

---

3) Faça uma tabela com os seguintes tipos de reatores nucleares de potência
desenvolvidos até hoje em todo o mundo: PWR, BWR, GCR, AGR, HTGR, PHWR (CANDU),
FBR, IRIS, mostrando o combustível, o grau de enriquecimento, o moderador e o
refrigerante empregados em cada um.


| Reator | Combustível | Enriquecimento | Moderador | Refrigerante |
|--------|-------------|----------------|-----------|--------------|
| PWR    | UO2         | 2-4%           | H2O       | H2O          |
| BWR    | UO2         | 2-4%           | H2O       | H2O          |
| GCR    | U metálico  | -              | Grafite   | CO2          |
| AGR    | UO2         | 2-4%           | Grafite   | CO2          |
| HTGR   | ThC2 e UC2  | 93%            | Grafite   | He           |
| PHWR   | UO2         | -              | D2O       | D2O          |
| FBR    | UO2 e PuO2  | -              | -         | Na líquido   |

---

4) Em relação às usinas nucleoelétricas do Brasil, Angra 1 e Angra 2, pergunta-se:

a) a localização geográfica das usinas;

Complexo Nuclear Almirante Álvaro Alberto, Praia de Itaorna, Município de Angra
dos Reis, Estado do Rio de Janeiro

b) o tipo de reator nuclear de potência que equipa as duas usinas;

PWR

c) a substância que, neste tipo de reator, tanto refrigera o núcleo quanto
modera os nêutrons;

H2O

d) o combustível nuclear utilizado;

Pastilhas cilíndricas de UO2 com enriquecimento médio de 2,5%.

e) o material de revestimento das varetas combustíveis;

Varetas de Zircaloy-4 de 10,75mm de diâmetro e 3,9m de comprimento,
acondicionadas em 16x16 em cada núcleo combustível.

f) o material utilizado nas barras de controle;

Varetas de Prata-Índio-Cádmio de formato idêntico às que acondicionam o
combustível.

g) a função da água do mar;

Usada no circuito terciário no resfriamento e condensação do vapor gerado no
circuito secundário.

h) a potência elétrica máxima gerada em cada usina.

1876MW térmicos e 626MW elétricos.

---

5) Em um reator nuclear do tipo PWR, explique a função de cada um dos seguintes
componentes: núcleo, vaso de pressão, pressurizador, gerador de vapor,
bombas, refrigerante, moderador. Faça um esquema mostrando cada um
desses componentes.

* Núcleo: Pode ter dimensões de até 2m de altura por 3m de diâmetro, mas
  normalmente se situa entre 3,7m de altura e 3,8m de diâmetro e é responsável
  pelo acondicionamento do combustível, das estruturas de controle e pela
  realização da fissão.
* Vaso de pressão: Com 215mm de espessura, é feito de aço carbono revestido
  internamente por uma camada de aço inoxidável, e serve de proteção para o
  núcleo em relação à alta pressão e temperatura a que todo o sistema é submetido.
* Pressurizador: Garantir que a água possa ser mantida em estado líquido mesmo
  nas altas temperaturas de 100 a 370 graus Celsius, e regular a pressão do
  sistema através do nível da água no seu interior.
* Gerador de vapor: São trocadores de calor entre o sistema primário e
  secundário, fazendo com que a temperatura de até 330 graus Celsius da água
  pressurizada que veio do núcleo se transforme em vapor de até 290 graus a 73
  atm com baixa fração de umidade.
* Bombas: Fazem com que a água pressurizada recircule pelo sistema,
  refrigerando frações do sistema para que mais calor possa ser adquirido junto
  ao núcleo.
* Refrigerante: Substância utilizada para assegurar que o reator opere dentro dos
  limites termodinâmicos dos materiais utilizados em sua construção. No caso
  dos reatores PWR, este elemento é a água.
* Moderador: Substância utilizada para impedir que exista emissão de nêutrons
  fora de certo perímetro de segurança. Esta substância deve ter elevada seção
  de choque, condição necessária para a asborveção de nêutron. Em reatores PWR,
  este elemento é a água, o que faz que ela tenha dupla finalidade --
  refrigeração e moderação.

---

6) Quais são as vantagens que um reator nuclear do tipo BWR apresenta em
relação a reatores nucleares refrigerados a água pressurizada?

A ebulicação da água pode ocorrer no núcleo e ser enviada diretamente para as
turbinas, não sendo necessários os trocadores de calor -- um dos maiores
componentes dos reatores PWR -- que aumentam os cursos e acarretam em perdas
termodinâmicas na passagem de energia para o sistema secundário.
