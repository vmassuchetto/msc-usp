## TNA 5781 Fundamentos de Tecnologia Nuclear - Proteção Radiológica

## Lista de Exercícios

---

1) Para que tipo de radiação e meio de medida a grandeza querma (K) pode ser
utilizada?

Querma é uma unidade expressada em J/kg, ou Gy (Gray). Seu nome vem da sigla
'kerma', que significa 'kinetic energy released per unit of mass'.

A equação de obtenção é dE/dm, onde dE é a soma das energias cinéticas iniciais
de todas as partículas ionizantes eletricamente carregadas, liberadas pelas
partículas ionizantes sem carga num material de massa dm.

O querma pode ser utilizada para qualquer radiação não diretamente ionizante
(sem carga) e em qualquer meio.

---

2) Para que tipo de radiação e meio de medida a grandeza dose absorvida (D)
pode ser utilizada?

A equação base de dose absorvida é dE/dm, onde dE é a energia média cedida pela
radiação ionizante para a matéria de massa dm.

A dose absorvida pode ser usada para qualquer tipo de radiação, e não somente
R-X e R-gama, e pode ser medida em qualquer meio, e não somente no ar.

---

3) Para que tipo de radiação e meio de medida a grandeza exposição (X) pode ser
utilizada?

A equação base para exposição é dQ/dm, em que dQ é o valor absoluto de todas as
cargas elétricas dos íons de um mesmo sinal produzido no ar quando todos os
elétrons liberados por fótons num elemento de massa dm são completamente
freados no ar.

Serve para medir a radiação eletromagnética no ar.

---

4) Em que condições querma é igual à dose absorvida?

Quando duas condições são atendidas:

* Quando existe equilíbrio eletrônico;
* Quando a radiação de frenamento é desprezível -- até 3MeV.

---

5) Quais os principais efeitos biológicos nas gônodas? (ver no material de
apoio "Estabelecimento dos Limites Anuais de Dose")

Os efeitos biológicos são classificados em:

* Efeitos somáticos;
* Efeitos genéticos ou hereditários.

Os efeitos somáticos em organismos complexos ficam restritos à ele, enquanto os
hereditários são transferidos a seus descendentes.

Nas gônodas, especificadamente, temos os seguintes efeitos:

* Indução de tumor;
* Diminuição da fertilidade;
* Efeitos hereditários nos descendentes.

---

6) Como é definido o risco?

Risco é a probabilidade de ocorrência de um efeito deletério por uma dose de
radiação, sem depender da gravidade deste efeito.

---

7) O que é detrimento?

O detrimento é a probabilidade de ocorrência de um efeito deletério,
considerando a gravidade deste efeito, composta pelos atributos:

* Indução de câncer fatal;
* Efeito genético nos descendentes;
* Morbidade por câncer;
* Anos de vida perdidos.

---

8) Em que casos há restrições aos limites anuais de dose e o que recomenda a
norma CNEN-NN-3.01 para estes casos?

As restrições de dose se aplicam nos seguintes casos:

* Para menores de 18 anos -- não devem tomar dose alguma;
* Mulheres grávidas -- o feto não deve receber doses maiores do que 1mSv;
* Para acompanhantes e voluntários de pacientes -- não devem receber doses
  maiores do que 5mSv durante o período de tratamento.

---

9) Quais são os três tipos de monitoração do local de trabalho de caráter
preventivo para trabalhadores?

* Radiação externa;
* Contaminação da superfície;
* Contaminação do ar.

---

10) Quais são os três tipos de monitoração de caráter confirmatório para
trabalhadores?

* Irradiação externa;
* Contaminação interna;
* Contaminação de pele.

---

11) Defina o que é o grupo crítico.

Grupo de indivíduos homogêneo, nos hábitos e costumes que vem sofrer as maiores
consequências em virtude daquela atividade com radiação. Em outras palavras, é
aquele grupo que recebe a maior dose de radiação.

---

12) O que é o nivel de investigação?

É um nível referência que, quando atingido ou excedido, torna necessária a
avaliação das causas e conseqüências dos fatos que levaram à detecção deste
nível, bem como a proposição de ações corretivas necessárias.

3/10 da dose anual.

---

13) O que é o nível de registro?

É o valor de dose, ou grandeza a ela relacionada, obtido em um programa de
monitoração, cuja magnitude seja relevante para justificar o seu registro.

1/10 da dose anual.

---

14) O que é nível de ação?

É um nível de dose evitável, que leva à implementação de uma ação remediadora
ou protetora específica, em uma situação de emergência ou exposição crônica.

---

15) Quais as técnicas de calibração de instrumentos medidores portáteis de
radiação mais utilizados na indústria e na medicina?

* Uso de dosímetros padrões -- técnica de substituição;
* Uso de campos padrões -- campo de radiação calibrado.

---

16) Quais são as duas hipóteses básicas (imposições) assumidas pelo ICRP que
levam à formulação dos princípios básicos de radioproteção?

* Conceito de risco -- por menor que seja a dose, sempre há possibilidade de um
  efeito deletério;
* Conceito de linearidade -- a relação dose-efeito para doses pequenas é
  linear, para altas chega a ser quadrádica.

---

17) Quais os três princípios do Sistema de Proteção Radiológica?

* Justificação da prática -- nenhuma atividade com radiação deve ser aceita se
  ela não introduz um benefício líquido positivo;
* Otimização da proteção -- todas as radiações devem ser mantidas no nível mais
  baixo quanto for razoavelmente exequível, levando em consideração aspectos
  econômicos e sociais.
* Limites individuais -- as doses equivalentes aos indivíduos não devem ser
  excedidas aos limites estipulados pela ICRP.

---

18) Sabe-se que dois indivíduos ocupacionalmente expostos (IOE) exercem
atividades em um laboratório onde estão sujeitos as seguintes condições de
trabalho:

O IOE-A recebe uma dose anual no cristalino de 15 mSv além de receber  200 mSv
nas gônadas e o IOE-B recebe uma dose anual de 510 mSv nas mãos e 300 mSv/ano
na tireóide.

Sabe-se que ambos os IOE acumulou uma dose de 70 mSv nos últimos 4 anos de
trabalho (estamos no 5ºano).

Conhecendo-se que $w_T$ para as gônadas é de 0,20 e para a tireóide é de 0,05
responda:

a) Os trabalhadores A e B obedecem os limites para evitar o aparecimento dos
efeitos determinísticos?

Trabalhador A:

* Cristalino: 15mSv/ano -- 135mSv abaixo do limite

Trabalhador B:

* Extremidade: 510mSv/ano -- 10mSv/ano acima do limite

O trabalhador B não obedece aos limites para extremidades de 500mSv/ano.

b) Determine a dose média dos Trabalhadores A e B ao final do quinto ano. Eles
obedecem os limites para evitar o aparecimento dos efeitos estocásticos?

Trabalhador A:

* Gônadas: 200mSv/ano -- $E = 200 \times 0,2 = 40mSv$
* Acumulado: $70mSv + 40mSv = 110mSv / 5 anos = 22mSv/ano$

Trabalhador B:

* Tireóide: 300mSv/ano -- $E = 300 \times 0,05 = 15mSv$
* Acumulado: $70mSv + 15mSv = 85mSv / 5 anos = 17mSv/ano$

O trabalhador A não obedece ao limite de 20mSv/ano para o limite de efeitos
estocásticos.

---

19) Quais os LD para evitar o aparecimento de efeitos estocásticos para o IOE e
para os indivíduos do público?

* Trabalhador: 20 mSv/ano
* Público: 1 mSv/ano

---

20) Quais os LD para evitar o aparecimento de efeitos determinísticos para o
IOE e para os indivíduos do público?

* Cristalino: 150 mSv/ano trabalhador, 15 mSv/ano indivíduo público
* Pele: 500 mSv/ano trabalhador, 50 mSv/ano indivíduo público
* Extremidade: 500 mSv/ano trabalhador, sem exposição para o público

---

21) Durante a manipulação de um frasco contendo I-131 há uma liberação de vapor
de iodo acarretando a contaminação interna de um trabalhador. Determinou-se,
por meio de medidas e cálculos, que a dose absorvida no pulmão foi de 100 mGy e
a dose absorvida na tireóide foi de 150 mGy. Qual a dose equivalente $H_T$ em
cada um desses órgãos e qual a dose efetiva $E$ recebida por este trabalhador?

O radionuclídeo em questão é o Iodo-131 para consideração do $w_R$. Utilize
estes dados:

* $w_T$ Tireóide = 0,05
* $w_T$ Pulmão = 0,12

* O cálculo de dose equivalente dá-se por $H_T = D \times w_R$, sendo $w_R$ o
  fator de ponderação para a radiação.
* O cálculo de dose efetiva dá-se por $E = \sum_{T}H_{T} \times w_T$

Assim, temos para a tireóide:

$H_T = D \times w_R = 300mGy \times 1 = 300mSv$

$E = H_T \times w_T = 300mSv \times 0,05 = 15mSv$

E para o pulmão:

$H_T = D \times w_R = 50mGy \times 1 = 50mSv$

$E = H_T \times w_T = 50mSv \times 0,12 = 6mSv$

A dose total:

$E = \sum_{T}H_{T} \times w_T = 15mSv + 6mSv = 21mSv$

---

22) Como o ICRP classifica os efeitos biológicos da radiação?

Em função da forma de resposta:

* Estocástico;
* Determinístico.

Em função do tempo de manifestação:

* Imediatos;
* Tardios.

Em função do nível orgânico atingido:

* Somático;
* Genético.

---

23) Explique os efeitos estocásticos e os determinísticos.

* Estocástico -- sem dose limiar para ocorrência dos efeitos;
* Determinístico -- com dose limiar para ocorrência dos efeitos.

---

24) Como limitar a probabilidade dos efeitos estocásticos?

* Manter as doses tão baixas quanto exequível considerando os fatores
  econômicos e sociais;
* Obedecer os limites de dose.

---

25) Como evitar efeitos determinísticos?

* Obedecer os limites de dose.

---

26) O que é a dose efetiva? Como ela é definida?

A soma das doses equivalentes de todos os tecidos multiplicado pelo respectivo
fator de ponderação ($E$), o que pode ser expressado pela equação $E =
\sum_{T}H_T \times w_T$

---

27) Como os rejeitos radioativos sólidos gerados numa instalação que produz
Iodo-131 e Molibdênio-99m podem ser tratados?

* São materiais com meia-vida curta;
* Espera-se o decaimento radioativo;
* Eliminam-se todos os símbolos e etiquetas de materiail radioativo;
* Descarta-se em lixo urbano.

---

28) Qual o procedimento para a redução de volume para os rejeitos radioativos
como papéis, luvas de borracha, tecidos e plásticos provenientes de instalações
radiativas?

* Compactação dos materiais antes do descarte.

---

29) Quais os principais meios de proteção à radiação para trabalhadores?
Comente cada uma delas.

* Tempo de exposição -- a dose é diretamente proporcional ao tempo -- quanto
  menos tempo perto da fonte, melhor;
* Distância da fonte -- a dose é proporcional ao inverso do quadrado da
  distância, o que é válido para distâncias curtas -- quanto maior a distância
  da fonte, melhor;
* Blindagem -- diminui o nível de radiação, mas depende tanto do tipo de
  radiação quando do tipo de material a ser utilizado. Alpha -- folha de papel,
  Beta -- Alumínio, Gama -- chapas de chumbo, concreto, ferro, Nêutrons --
  espalhamento elástico em material hidrogenado (parafina, polietileno),
  concreto, ferro -- quanto mais eficiente a blindagem, melhor;

---

30) O que é área livre?

a) área isenta dos serviços de monitoração individual
b) área onde é necessário o controle de acesso
c) área isenta de regras especiais de segurança, mas onde é necessário a
monitoração individual
d) * área isenta de regras especiais de segurança, onde as HE anuais não
   ultrapassam o limite primário para indivíduos do público.
e) N.R.A.

* Área livre -- aquela em que são obedecidos os limites para os indivíduos do
  público. Nesta área não é obrigatória a presença da proteção radiológica;
* Área supervisionada -- aquela que por decorrência de exposição dos indivíduos
  torna possível somente os efeitos estocásticos;
* Área controlada -- aquela que por decorrência de exposição dos indivíduos
  torna possível os efeitos determinísticos.

---

31) Quais os principais detrimentos biológicos considerados na atribuição dos
riscos resultantes de uma dose de radiação no indivíduo?

* R1 -- morte por câncer fatal;
* R2 -- encurtamento da vida;
* R3 -- morbidade por câncer;
* R4 -- efeito genético nos descendentes.

---

32) O princípio da Limitação de Dose Individual está baseado em quais
conceitos?

* Conceito de risco;
* Conceito de linearidade.

---

33) Como a a probabilidade de ocorrência dos efeitos estocásticos varia com a
dose? O conceito da Linearidade é válida para que níveis de dose e taxa de
dose?

* Varia linearmente;
* Válido para taxas baixas (ordem de 1 Gray).

---

34) Quais as doses que não são computadas para a Limitação de Dose Individual
em exposição normal?

* A radiação de fundo (natural);
* Doses resultantes de irradiações médicas -- pois ela ocorre em benefício da
  própria pessoa.
