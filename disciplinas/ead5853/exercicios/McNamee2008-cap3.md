Vinicius Massuchetto

Exercícios McNamee & Celona

### Exercício 3.7

A troca de dor por dinheiro a princípio me faz pouco sentido e me parece
minimalista e sem alcance, pois penso que uma sociedade saudável deve prover de
imediato os recursos necessários para recuperação a quem precisar -- o que não
necessariamente se traduz em dinheiro, mas sim em direitos.

Por exemplo: O que é uma compensação justa para quem perde as duas pernas em um
acidente? O causador, obviamente, deve ser responsabilizado e punido se for o
caso, mas e a vítima?  Até onde a compensação realmente a ajuda? O que é melhor
para que esta pessoa possa se recuperar psicologicamente? O direito de
locomoção, trabalho e respeito durante todo o resto de sua vida, ou uma
resposta imediatista sobre o evento de sua tragédia?

Nesse sentido, creio que qualquer resposta imediata não cause à vítima um
conforto significativo, pois o tamanho de sua perda pode facilmente exacerbar
qualquer valor estimável possível.

### Exercício 3.17

a) Diagrama de influência e árvore de decisão

![](McNamee2008-exe3.17-diagrama.png)

![](McNamee2008-exe3.17-arvore.png)

b) Quanto pagar: Contingência: $300 -- Máximo: $900
