## Bibliografia

1. I. KAPLAN, Nuclear Physics, Addison-Wesley Publishing Company Inc., Reading,
   1977.
2. G.FRIEDLANDER, J.W.KENNEDY, J.M.MILLER. Nuclear and Radiochemistry. John
   Wiley Inc, New York, 2nd ed., 1964.
3. W.E. MEYERHOF, Elements of Nuclear Physics, McGraw-Hill Book Company, New
   York, 1989.
4. K.S.KRANE, Introductory Nuclear Physics, John Wiley & Sons Inc., New York,
   1987.
5. Z.B. ALFASSI, Activation Analysis, Boca Raton, Fla.: CRC Press, 1990, v.1.
6. B.G. HARVEY, Introduction to Nuclear Physics and Chemistry, Prentice-Hall,
   New Jersey, 1976.
