<?php

function form_submit() {

    if ( empty( $_POST['form_submit'] ) )
        return false;

    global $config;

    $data = serialize( $_POST );
    $date = date('Y-m-d H:i:s');
    mysql_connect( $config['db_host'], $config['db_user'], $config['db_pass'] );
    mysql_select_db( $config['db_name'] );
    $sql = "INSERT INTO form (data, date) VALUES ('{$data}', '{$date}')";
    if ( mysql_query( $sql ) )
        echo 1;
    else
        echo 0;
    mysql_close();

    exit( 0 );

}
form_submit();

$domain = 'form';
$lang = !empty($_GET['lang']) && $_GET['lang'] == 'en' ? 'en' : 'pt';
$locales = array(
    'pt' => 'pt_BR',
    'en' => 'en_US'
);
$locale = $locales[ $lang ];
putenv("LANG=$locale");
putenv("LANGUAGE=$locale");
putenv("LC_ALL=$locale");
putenv("LC_MESSAGES=$locale");
setlocale( LC_ALL, $locale );
setlocale( LC_MESSAGES, $locale );
bindtextdomain( $domain, dirname( __FILE__ ) . '/lang' ); 
textdomain( $domain );
?>

<?php include( 'header.php' ); ?>

<div class="row">
    <div class="large-12 columns">

        <h2><?php _e('Pesquisa Sobre Perfil Comportamental em Ambientes Profissionais'); ?></h2>
        
        <p><?php _e('No ambiente de trabalho é muito comum termos que realizar tarefas junto com outras pessoas e a sinergia/facilidade que temos em trabalhar em parceria depende muito das características de nossos pares. Nesta pesquisa descrevemos alguns comportamentos que, em princípio, enquadrariam a maioria das pessoas que encontramos em nossa ocupações. Raramente uma pessoa se comporta sempre da mesma forma, mas a ideia é classificar as pessoas de acordo com o seu comportamento dominante'); ?>.</p>

        <p><?php _e('Por favor, leia com atenção e tente relacionar o comportamento usual com o qual você se depara nas suas atividades profissionais.'); ?></p>

        <p><?php _e('Todas as respostas são obrigatórias, exceto a participação no sorteio.'); ?></p>
    
    </div>
</div>

<div class="row main">
    <div class="large-12 columns">

        <form>

        <input type="hidden" name="form_submit" value="1" />
        <input type="hidden" name="ip" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>" />
        <input type="hidden" name="lang" value="<?php echo $lang ?>">

        <fieldset>

        <h4><?php _e('Qual a sua idade?'); ?></h4>

        <p>
        <label for="idade1"><input type="radio" id="idade1" name="idade" value="10-14"> <?php _e('10 a 14 anos'); ?></label>
        <label for="idade2"><input type="radio" id="idade2" name="idade" value="15-17"> <?php _e('15 a 17 anos'); ?></label>
        <label for="idade3"><input type="radio" id="idade3" name="idade" value="18-24"> <?php _e('18 a 24 anos'); ?></label>
        <label for="idade4"><input type="radio" id="idade4" name="idade" value="25-49"> <?php _e('25 a 49 anos'); ?></label>
        <label for="idade5"><input type="radio" id="idade5" name="idade" value="50+"> <?php _e('50 anos ou mais'); ?></label>
        </p>

        <h4><?php _e('Sexo'); ?></h4>

        <p>
        <label for="sexo1"><input type="radio" id="sexo1" name="sexo" value="m"> <?php _e('Masculino'); ?></label>
        <label for="sexo2"><input type="radio" id="sexo2" name="sexo" value="f"> <?php _e('Feminino'); ?></label>
        </p>

        <h4><?php _e('Escolaridade'); ?></h4>

        <p>
        <label for="escolaridade1"><input type="radio" id="escolaridade1" name="escolaridade" value="sem"> <?php _e('Sem Instrução Formal'); ?></label>
        <label for="escolaridade2"><input type="radio" id="escolaridade2" name="escolaridade" value="fundamental"> <?php _e('Ensino Fundamental Completo'); ?></label>
        <label for="escolaridade3"><input type="radio" id="escolaridade3" name="escolaridade" value="medio"> <?php _e('Ensino Médio Completo'); ?></label>
        <label for="escolaridade4"><input type="radio" id="escolaridade4" name="escolaridade" value="superior"> <?php _e('Ensino Superior Completo'); ?></label>
        </p>

        <h4><?php _e('Quanto tempo de estudo você tem?'); ?></h4>

        <p>
        <label for="tempo_de_estudo1"><input type="radio" id="tempo_de_estudo1" name="tempo_de_estudo" value="0-1"> <?php _e('Menos de 1 ano'); ?></label>
        <label for="tempo_de_estudo2"><input type="radio" id="tempo_de_estudo2" name="tempo_de_estudo" value="4-7"> <?php _e('4 a 7 anos'); ?></label>
        <label for="tempo_de_estudo3"><input type="radio" id="tempo_de_estudo3" name="tempo_de_estudo" value="8-10"> <?php _e('8 a 10 anos'); ?></label>
        <label for="tempo_de_estudo4"><input type="radio" id="tempo_de_estudo4" name="tempo_de_estudo" value="11+"> <?php _e('Mais de 11 anos'); ?></label>
        </p>

        <h4><?php _e('Em que tipo de empresa você trabalha?'); ?></h4>

        <p>
        <label for="empresa1"><input type="radio" id="empresa1" name="empresa" value="industria"> <?php _e('Indústria extrativa e de transformação e produção e distribuição de eletricidade, gás e água'); ?></label>
        <label for="empresa2"><input type="radio" id="empresa2" name="empresa" value="construcao"> <?php _e('Construção'); ?></label>
        <label for="empresa3"><input type="radio" id="empresa3" name="empresa" value="comercio"> <?php _e('Comércio, reparação de veículos automotores e de objetos pessoais e domésticos'); ?></label>
        <label for="empresa4"><input type="radio" id="empresa4" name="empresa" value="financeira"> <?php _e('Intermediação financeira e atividades imobiliárias, aluguéis e serviços prestados à empresa'); ?></label>
        <label for="empresa5"><input type="radio" id="empresa5" name="empresa" value="administracao"> <?php _e('Administração pública, defesa, seguridade social, educação, saúde e serviços sociais'); ?></label>
        <label for="empresa6"><input type="radio" id="empresa6" name="empresa" value="domestico"> <?php _e('Serviços domésticos'); ?></label>
        <label for="empresa7"><input type="radio" id="empresa7" name="empresa" value="consultoria"> <?php _e('Serviços de consultoria de projetos'); ?></label>
        <label for="empresa8"><input type="radio" id="empresa8" name="empresa" value="pesquisa"> <?php _e('Serviço público ou educação, pesquisa e desenvolvimento'); ?></label>
        <label for="empresa9"><input type="radio" id="empresa9" name="empresa" value="ti"> <?php _e('Tecnologia da Informação ( Computadores e Equipamentos, Programas e Serviços '); ?>)</label>
        <label for="empresa10"><input type="radio" id="empresa10" name="empresa" value="o_servicos"> <?php _e('Outros serviços'); ?></label>
        <label for="empresa11"><input type="radio" id="empresa11" name="empresa" value="o_atividades"> <?php _e('Outras atividades'); ?></label>
        </p>

        <?php
            $options = array(
                array(
                    'id' => 1,
                    'desc' => _('Estão sempre dispostas a cooperar, idependente de terem sido tapeadas pelas pessoas que pedem sua ajuda.')
                ), array(
                    'id' => 2,
                    'desc' => _('Nunca colaboram uma com as outras, e semprem tentam tirar vantagem dos colegas. Colocam sempre seu interesse e ambições em primeiro lugar.')
                ), array(
                    'id' => 3,
                    'desc' => _('Inicialmente sempre colaboram, mas devolvem na mesma moeda se forem tapeadas uma vez.')
                ), array(
                    'id' => 4,
                    'desc' => _('Inicialmente sempre colaboram, mas são tolerantes com quem não as ajuda e devolvem o comportamento na mesma moeda somente se forem tapeadas mais de uma vez.')
                ), array(
                    'id' => 5,
                    'desc' => _('Inicialmente não colaboram, só ajudam a quem os ajudar primeiro.')
                ), array(
                    'id' => 6,
                    'desc' => _('Inicialmente sempre colaboram, mas guardam ressentimento se forem tapearas e nunca mais cooperam com mais ninguém.')
                ), array(
                    'id' => 7,
                    'desc' => _('As pessoas cooperam de acordo com o humor, independente do histórico de relacionamento com as demais.')
                )
            );
            shuffle( $options );
        ?>

        <h4><?php _e('Há quanto tempo você exerce atividade profissional?'); ?></h4>

        <p>
        <label for="tempo_atividade1"><input type="radio" id="tempo_atividade1" name="tempo_de_atividade" value="0-1"> <?php _e('Menos de 1 ano'); ?></label>
        <label for="tempo_atividade2"><input type="radio" id="tempo_atividade2" name="tempo_de_atividade" value="1-3"> <?php _e('1 a 3 anos'); ?></label>
        <label for="tempo_atividade3"><input type="radio" id="tempo_atividade3" name="tempo_de_atividade" value="3-6"> <?php _e('3 a 6 anos'); ?></label>
        <label for="tempo_atividade4"><input type="radio" id="tempo_atividade4" name="tempo_de_atividade" value="6+"> <?php _e('Mais de 6 anos'); ?></label>
        </p>

        <h4><?php _e('Com quantas pessoas você interagiu profissionalmente nos últimos 3 anos?'); ?></h4>

        <p>
        <?php _e('Com aproximadamente'); ?>&nbsp;
        <input type="text" id="interacao_com_pessoas" name="interacao_com_pessoas" value="" style="width:48px; display:inline-block" />
        &nbsp;<?php _e('pessoas'); ?>.
        </p>

        <h4><?php _e('Qual seu tipo de alocação?'); ?></h4>
        
        <p>
        <label for="alocacao1"><input type="radio" id="alocacao1" name="alocacao" value="interno"> <?php _e('Predominantemente interno (Dentro de escritório, seja terceirizado ou não, temporário ou não, com local fixo de trabalho)'); ?></label>
        <label for="alocacao2"><input type="radio" id="alocacao2" name="alocacao" value="externo"> <?php _e('Predominantemente externo (Trabalha fora de escritório, não há um lugar fixo de trabalho, sempre junto a clientes, fornecedores, teletrabalho)'); ?></label>
        </p>

        <h4><?php _e('No seu trabalho, qual o comportamento predominante das pessoas? Marque três opções.'); ?></h4>

        <?php foreach ( $options as $o ) : ?>

        <label for="arquetipo<?php echo $o['id']; ?>" class="arquetipo" rel="<?php echo $o['id']; ?>">
            <input type="checkbox" class="arquetipo" name="arquetipo<?php echo $o['id']; ?>" id="arquetipo<?php echo $o['id']; ?>" value="<?php echo $o['id']; ?>" rel="<?php echo $o['id']; ?>"> <?php echo $o['desc']; ?>
        </label>

        <div class="row">
        <div class="large-10 large-offset-1 centered columns">
        <div class="panel arquetipo-slider-panel" id="arquetipo-slider-panel<?php echo $o['id']; ?>" style="display:none;">
            <div class="row">
                <div class="large-6 columns">
                    <label><?php _e('Com que intensidade este comportamento ocorre?'); ?></label>
                </div>
                <div class="large-5 columns">
                    <div class="arquetipo-slider" id="arquetipo-slider<?php echo $o['id']; ?>"></div>
                </div>
                <div class="large-1 columns arquetipo-slider-value">
                    <label>0%</label>
                    <input type="hidden" name="arq_n<?php echo $o['id']; ?>" id="arquetipo-intensidade<?php echo $o['id']; ?>" value="0">
                </div>
            </div>
            <div class="row">
                <div class="large-5 large-offset-6 columns">
                    <div class="row escalas">
                        <div class="large-3 columns" style="text-align:center"><small>|<br/><?php _e('Muito pouco'); ?></small></div>
                        <div class="large-3 columns" style="text-align:center"><small>|<br/><?php _e('Pouco'); ?></small></div>
                        <div class="large-3 columns" style="text-align:center"><small>|<br/><?php _e('Razoavelmente'); ?></small></div>
                        <div class="large-3 columns" style="text-align:center"><small>|<br/><?php _e('Muito'); ?></small></div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>

        <?php endforeach; ?>

        </fieldset>

        <?php if ( $lang == 'pt' ) : ?>

        <fieldset>

            <p>
            <label for="participar_promocao"><input type="checkbox" id="participar_promocao" name="participar_promocao" value="1"> Eu gostaria de participar do sorteio de uma caixa de chocolates! (opcional)</label>
            </p>

            <div class="row promocao">

                <div class="large-12 columns">

                <h3>Participação no sorteio:</h3>

                <p>Esta é uma pesquisa acadêmica na área de gestão do
                conhecimento do Programa de Pós-Graduação em Tecnologia Nuclear
                do Instituto de Pesquisas Energéticas e Nucleares e da
                Universidade de São Paulo, realizada pelos acadêmicos Ezequiel
                da Silva, Felipe Cintra, Fernando Bardella, Henrique Lopes e
                Vinicius Massuchetto.</p>

                <p>Estamos sorteando uma caixa de chocolates <i>Lindt
                Napolitains</i> como forma de agradecimento a todos que estão
                nos auxiliando com estas respostas, no entanto, para evitar
                trapaças pedimos que você confirme o seu e-mail e CPF abaixo.
                Estes dados são opcionais e não serão em nenhuma hipótese
                divulgados, sendo apagados permanentemente logo após a
                realização do sorteio.</p>

                <p style="text-align:center">
                <img src="img/chocolate.jpg" /><br/>
                <a href="http://www.lindt.com.au/swf/eng/products/pralines/napolitains/" target="_blank">Lindt Napolitains</a>
                </p>

                <p>
                <label>E-mail</label>
                <input type="text" name="email" />
                </p>

                <p>
                <label>CPF</label>
                <input type="text" name="cpf" />
                </p>

                <p>As informações acima serão conferidas no ato do sorteio. Se
                você não quiser fornecer os dados e não participar do sorteio,
                basta clicar no botão <i>Enviar dados</i> logo abaixo.</p>

                </div>

            </div>

        </fieldset>

        <?php endif; ?>

        <div class="row">
        <div class="large-12 columns">
        <div class="alert-box alert general-error" style="display:none;">
            <?php _e('Não foi possível enviar os dados do formulário. Por favor verifique os erros:'); ?><ul></ul>
        </div>
        </div>
        </div>

        <div class="row">
            <div class="large-2 large-offset-10 last-column columns">
                <input type="submit" class="button success" id="submit-button" value="<?php _e('Enviar dados'); ?>">
            </div>
        </div>

        </form>

    </div>
</div>

<div class="row alert-success" style="display:none;">
    <div class="alert-box success">
        <?php _e('Os dados foram enviados. Obrigado por participar da pesquisa.'); ?>
    </div>
</div>
    

<?php include( 'footer.php' ); ?>
