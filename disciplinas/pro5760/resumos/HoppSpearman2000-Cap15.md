## Hopp, Spearman -- Cap 15: Production Scheduling -- Vinicius Massuchetto

### Síntese

A busca de _lead times_ mais curtos, WIP mínimo e máxima utilização dos
recursos acabam sendo objetivos conflitantes em um sistema de produção, sendo
necessário balanceá-los para obtenção de bons resultados.

Os estudos tradicionais em agendamento de produção contribuem para as técnicas
modernas em sua percepção de redução do problema em proporções gerenciáveis,
como atribuir dimensões determinísticas e desconsiderar indisponibilidade dos
equipamentos ou o cancelamento de tarefas. Existem problemas também, como
utilizar mecanismos simples de controle como as listas FIFO, que tendem a não
funcionar bem em ambientes mais complexos.

Os problemas de agendamento de produção agrupam-se nas classes P (complexidade
polinomial) e, na maioria NP (complexidade exponencial). Assim, para problemas
reais utiliza-se a abordagem em heurística para encontrar uma solução razoável
-- mas não a melhor solução, que seria interessante somente do ponto de vista
dos modelos matemáticos.

Os cenários de planejamento são abordados com base no que é possível ser feito
com relação às datas de entrega, divisão das tarefas, viabilidade da agenda,
tratamento dos gargalos e controle da capacidade. Pode-se assim basear o
agendamento em uma simulação do sistema, que embora seja um procedimento
trabalhoso por demandar uma grande quantidade de dados, é um recurso mais fácil
de ser explicado e apresentado; assim como também pode-se basear em algoritmos
heurísticos que, segundo determinada estratégia, buscarão uma agenda otimizada
de acordo com os atributos do sistema, o que consistem em um procedimento cujo
desenvolvimento efetivo é complexo, e que não necessariamente representa o
sistema otimizado, mas somente satisfatório.

Devido ao trabalho excessivo de se gerar um agendamento de capacidade flexível,
passou-se a tratar a execução da produção de modo separado, e assim os modos de
se obter a redução do ciclo de tempo na produção. As abordagens dividem-se em
obter uma carga otimizada em série através da derivação da utilização das
estações de produção; ou na relação paralela de carga entre as capacidades.

O agendamento de problemas que envolvem datas de entrega e gargalos também são
considerados difíceis de se resolver, e por tal motivo busca-se simplificar o
planejamento do ambiente utilizando o sistema CONWIP, tornando-o mais
previsível para as datas de entrega e com somente uma estação determinística
para controle de gargalo.

Porém, nem todos os sistemas podem ser simplificados desta maneira, e uma
alternativa de variante do MRP é frequentemente usada através das estruturas de
um ERP em um gerenciamento que deve considerar a estimativa de dados e que
alguns aspectos do sistema não podem ser abordados através de modelos --
necessitando, neste caso, de uma intervenção administrativa com relação às
alternativas disponíveis.

Um dos sistemas criados para lidar com as inviabilidades deste tipo de
gerenciamento é o MRP-C, que é semelhante ao MRP mas que também considera de
maneira explícita a capacidade do sistema, tentando estabelecer um inventário
mínimo com uma agenda viável, e indicando os motivos no caso de falha para que
seus gestores possam tomar uma decisão. Este sistema mostra-se ainda mais
robusto quando é capaz de ser aplicado em sistemas com multiestágio com mais de
um produto.

### Dúvidas

* 15.1.3: Como a redução do WIP e dos ciclos de produção podem ajudar a fazer
  melhores previsões da produção?
* 15.2.3: Se o uso de FIFO ou outras regras para execução das tarefas não é uma
  boa saída para trabalhos complexos, é comum que estas regras sejam ajustadas
  caso a caso a fim de ajudar a criar um bom agendamento?
* 15.2.6: Porque a melhor agenda mais próxima tende a ser ruim no sentido de
  que a busca local não é eficiente para encontrar melhores agendamentos?
* Qual a expectativa de melhoria da heurística para sistemas de produção com a
  computação paralela e a computação quântica? Os problemas NP-Completos desta
  área têm sido significativamente ajudados pela evolução dos computadores?

### Críticas

* Achei interessante que o capítulo aborda as definições clássicas observando
  sobretudo as suas contribuições para os métodos mais modernos.
* Gostei da constante diferenciação entre modelos e realidade, e da postura de
  que não é interessante perseguir uma solução ótima para problemas reais,
  sendo até muitas vezes considerada somente a solução mais satisfatória.
* Em alguns pontos o capítulo interrompe a linha de raciocínio de uma seção com
  uma outra que não parece se relacionar muito, para retomá-la mais tarde,
  criando pequenas seções um pouco desconexas. Tenho a impressão que isto
  acontece por causa de muitas referências a outras partes do livro.
