Vinicius Massuchetto

Exercícios McNamee & Celona

### Exercício 3.17

Os pontos de cruzamento do gráfico representam o perfil da decisão que está
sendo tomada em relação aos riscos considerados. A decisão sobre $300, por
exemplo, oferece uma alta variação na sensibilidade em uma zona que não há
lucro, e possui alternativas com menor variação e que são mais rentáveis.

A variação da opção de $700, por sua vez, possui uma variação segura e de
resultado certamente rentável, porém bem menos vantajoso que a opção de $500,
que pode alcançar resultados muito melhores se um risco menor de perda de
lucratividade for assumido.

### Exercício 3.19

a)

![](McNamee2008-exe3.19-diagrama.png)

b)

![](McNamee2008-exe3.19-arvore.png)

Valor esperado:

* market share 10%: 0.12 *  1 + 0.18 *  6 =  1.20
* market share  6%: 0.20 * -1 + 0.30 *  2 =  0.40
* market share  2%: 0.08 * -3 + 0.12 * -2 = -0.48

É mais provável que os resultados sejam positivos, mas o investimento só
torna-se vantajoso se a rentabilidade mínima esperada for de $0.4.

c)

![](McNamee2008-exe3.19-grafico.png)

Geração do gráfico:

    echo -e "
        -3 0.08
        -2 0.20
        -1 0.40
         1 0.52
         2 0.72
         6 1.00" \
        | gnuplot -p -e "
            set grid;
            set offset graph 0.10, 0.10, 0.10, 0.10;
            set xlabel 'lucro';
            set ylabel 'probabilidade';
            plot '-' [1:2]
                title 'probabilidade acumulada'
                with steps"
