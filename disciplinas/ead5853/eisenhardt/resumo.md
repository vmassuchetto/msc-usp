# Politics of Strategy Decision Making in Hight Velocity Environments

##Toward a Midrange Theory

Kathlen M. Eisenhardt -- Stanford University

L. J. Burgeois -- University of Virginia

## Introdução

* _Politics_: Ocorrem em decisões estratégicas com resultados incertos, atores
  com visões conflitantes e resoluções através do exercício de poder. Exemplos:
  controle de agenda, retenção de informação e formação de coalizões informais.
* _Hight velocity_: Ambientes em que ocorre mudanças rápidas e descontínuas nas
  demandas, competidores, tecnologias e regulamentações -- tornando a
  informação inexata, indisponível ou obsoleta.
* Três perguntas básicas que motivam o estudo: Por que a política é usada? Qual
  o formato da política? Como a política afeta o desempenho de uma empresa?

## Método de Pesquisa

Obtenção dos dados

* 8 empresas estudadas
* 4 fontes de dados: entrevista com CEO e alta gerência, questionários para a
  alta gerência, e outros dados secundários -- informações internas e
  observações informais

Análise dos dados

* Mensuração dos conflitos, alianças, poder dentre as equipes entrevistadas.
* Elaboração de um perfil de cada executivo a partir da descrição dada pelos
  seus colegas.
* Elaboração de históricos de decisão com o momento de participação de cada
  executivo.
* Comparação dos dados aos pares de semelhanças e diferenças com as empresas.
* Discussão sobre 7 proposições sobre a análise dos dados e literatura.

## A origem da política

* P1: Quanto maior a centralização de poder, maior o uso da política pela alta
  gerência -- causando frustração por parte da gerência em não conseguir
  cumprir seus objetivos sem o uso da política.
* É possível que o uso da política esteja presente nos dois extremos: alta
  centralização ou alta descentralização do poder -- mas não aparece em
  ambientes com descentralização moderada.

Conflito, poder e política

* P2: O conflito não é uma condição suficiente para o uso da política. Por
  outro lado, o conflito traz o uso da política somente quando o poder é
  centralizado.
* Quantificação: desacordo nos objetivos da organização, desacordos na
  importância das decisões estratégicas, desacordos interpessoais dentre os
  executivos.
* Não foi possível verificar esta proposição por ela ser aplicável em casos
  diferentes -- votações legislativas, jogos de laboratório, alocação de
  recursos -- das enfrentadas pelas empresas, que são geralmente decisões para
  a sobrevivência, cujo melhor resultado possível é do interesse de todos.

## A organização da política

* P3: Quanto maior o uso da política, mais provável a existência de alianças
  estáveis na alta gerência.
* Resultados sugerem que quanto mais política, mais alianças estáveis.
* A atividade política é estressante, fazendo com que os executivos dependam de
  respaldos habituais como alianças estáveis.

Coalizões e demografia dos executivos

* P4: Quando o uso da política é alto, a base das alianças tende a ser similar
  aos atributos demográficos -- idade, localização do escritório, similaridade
  de cargos e experiências anteriores dos executivos.
* Resultados sugerem que os mesmos fatores que definem a amizade definem também
  as alianças entre os executivos.
* Alianças não se formam pelos problemas enfrentados, mas sim pelo desconforto
  com a política -- que deve ser usada somente quando necessário, e que neste
  caso deve ser feita da maneira mais segura possível ao envolver aliados
  confiáveis.
* A demografia pode ter um papel crucial na formação de alianças, mas não é
  responsável por um estímulo da prática da política.
* P5: Similaridades demográficas não é condição suficiente para a formação de
  coalizões estáveis, embora possa favorecer estes padrões quando o poder é
  centralizado e o uso da política é alto.

Efeitos do tempo

* P6: A formação de padrões estáveis de alianças dificultam as mudanças no uso
  da política.
* Equipes politicamente ativas são comumente organizadas em pequenas alianças
  baseadas em características demográficas. Ao longo do tempo, um padrão de
  alianças estáveis surge e permanece mesmo quando as razões que estimularam
  sua formação não estão mais presentes.

Política e desempenho da empresa

* P7: Quanto maior o uso da política, pior o desempenho da empresa.
* Mensuração: Relatórios de desempenho internos, comparação com classificação
  dos competidores pelo CEO, crescimento em vendas e receita.
* Resultados apontam forte relação do uso de política com baixo desempenho.
* Literatura diz que baixa performance relaciona-se com a centralização de
  poder, o que por consequência cria política.
* Os dados também sugerem que política leva à baixa performance -- gasto de
  muito tempo com política, distraindo os executivos, dissipando suas energias
  e afastando-os de suas responsabilidades funcionais.
* A política restringe o fluxo de informação -- o que é especialmente
  problemático em ambientes de rápida mudança como a informática, retirando dos
  executivos recursos úteis para a tomada de suas decisões.
* A formação de coalizões também distorce a percepção dos executivos, limitando
  suas possibilidades de formar alianças efetivas e produtivas, e também de
  colocar ideias de modo a ser levado em consideração por toda a alta gerência.

## Por uma teoria da política em ambientes de mudança constante

* A política ocorre mais em ambientes de poder centralizado.
* A política é organizada em coalizões estáveis dentre a alta gerência.
* Quanto mais política, pior o desempenho da empresa.



