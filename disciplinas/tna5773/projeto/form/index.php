<?php

function _e( $str ) {
    echo _( $str );
}

$config = array(
    'db_host' => 'localhost',
    'db_name' => 'tna5773form',
    'db_user' => 'root',
    'db_pass' => 'root',
    'app_pass' => 'tNa5773'
);

if ( !empty( $_GET['pass'] ) && $_GET['pass'] == $config['app_pass'] )
    include( 'report.php' );

else
    include( 'form.php' );
