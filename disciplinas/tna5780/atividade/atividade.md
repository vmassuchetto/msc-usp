# TNA5780 Fundamentos de Tecnologia Nuclear - Física Nuclear e Aplicações

\begin{center}

  Prof. Renato Semmler

  Alunos:

  Fernando Henrique Quinhoneiro \\
  Luiz Antônio Rossi Jazbinsek \\
  Vinicius André Massuchetto

\end{center}

---

## Atividade de Disciplina

A tabela abaixo representa medidas da atividade (em contagens por minuto) em
função do tempo, de uma amostra radioativa composta.

------- ----- ----- ---- ---- ---- ---- ---- ---- --- --- ---
t (min)     0    30   60   90  120  150  180  210 240 270 300
A (cps) 2163    902  455  298  225  183  162  145 133 120 110
------- ----- ----- ---- ---- ---- ---- ---- ---- --- --- ---

Assumindo que a amostra contenha 02 radionuclídeos, pede-se:

a) O gráfico $ln(A) = f(t)$ ou $log(A) = f(t)$.
b) As atividades iniciais (em $t = 0$) para os radionuclídeos 1 e 2.
c) As constantes de desintegração $\lambda_{1}$ e $\lambda_{2}$.
d) A meia-vida de cada um dos radionuclídeos.

---

\pagebreak

### (a)

![](./graph-lna.png)

    gnuplot -p -e '
        set terminal png;
        set grid; set xlabel "t (min)";
        set ylabel "ln(A)";
        plot \
            "data.dat" using 1:(log($2)) with points notitle pt 6,
            "data.dat" using 1:(log($2)) with lines title "ln(A)"
    ' > graph-lna.png

---

### (b)

Assumindo que no final das medições somente o nuclídeo 2 é encontrado, temos
que seu decaimento pode ser aproximado através da extrapolação da curva
logarítmica, tomando como linear a seção de $t = 210$ até $t = 300$:

\begin{center}

  $y(x_{*}) = y_{t210} + \displaystyle \frac{x_{*} - x_{t210}}{x_{t300} -
  x_{t210}}(y_{t300} - y_{t210})$

  $y(x_{*}) = ln(145) + \displaystyle \frac{x_{*} - 210}{300 - 210}(ln(110) -
  ln(145))$

  $y(x_{*}) = -0,0031x + 5,6333$

\end{center}

![](./graph-lna-ext.png)

    gnuplot -p -e '
        set terminal png;
        set grid;
        set xlabel "t (min)";
        set ylabel "ln(A)";
        plot \
            "data.dat" using 1:(log($2)) with points notitle pt 6,
            "data.dat" using 1:(log($2)) with lines title "ln(A)",
            -0.0031*x+5.6334 title "Extrapolação ln(A)"
    ' > graph-lna-ext.png

Assim, temos que para $t = 0$ a atividade do nuclídeo 2 é:

\begin{center}

  $y(x = 0) = -0,0031 \times 0 + 5,6333$

  $y(x = 0) = 5,6333$

  $ln(A_{2}) = 5,6333$

  $e^{ln(A_{2})} = e^{5,6333}$

  $A_{2} = 279,5832 cpm$

\end{center}

Com isso, conseguimos também determinar a atividade do nuclídeo 1:

\begin{center}

  $A_{total} = A_{1} + A_{2}$

  $2163 = A_{1} + 279,5832$

  $A_{1} = 1883,4168 cpm$

\end{center}

---

### (c)

Conforme a equação da reta para $A_{2}$ retirada em (b), temos que
$\lambda_{2}$ é o seu coeficiente linear:

\begin{center}

  $y(x_{*}) = -0,0031x + 5,6333$

  $\lambda_{2} = 0,0031 min^{-1}$

\end{center}

Para obter $\lambda_{1}$ assumimos o momento $t = 120$ em que:

\begin{center}

  $ln(A_{2}) = -0,0031x + 5,6333$

  $ln(A_{2}) = -0,0031(120) + 5,6333$

  $ln(A_{2}) = 5,2613$

  $e^{ln(A_{2})} = e^{5,2613}$

  $A_{2} = 192.7319 cpm$

\end{center}

A partir disto, temos que a atividade total na amostra em $t = 120$ é:

\begin{center}

  $A_{total} = A_{1} + A_{2}$

  $A_{total} = A_{1} + 192,7319$

  $225 = A_{1} + 192,7319$

  $A_{1} = 32,2681$

\end{center}

Como em (b) obtemos que para $t = 0$ a atividade $A_{1} = 1883,4168$, a sua
constante de desintegração $\lambda_{1}$ é dada por:

\begin{center}

  $\lambda_{1} = \displaystyle \frac{ln\left(\displaystyle
  \frac{A_{0}}{A_{1}}\right)}{t}$

  $\lambda_{1} = \displaystyle \frac{ln\left(\displaystyle
  \frac{1883,4168}{32,2681}\right)}{120}$

  $\lambda_{1} = \displaystyle \frac{ln(4,0668)}{120}$

  $\lambda_{1} = 0.0339 min^{-1}$

\end{center}

---

### (d)

Tendo as constantes de desintegração $\lambda_{1}$ e $\lambda_{2}$, podemos
calcular a meia-vida de cada nuclídeo:

\begin{center}

$(T_{1/2})_{1} = \displaystyle \frac{ln(2)}{\lambda_{1}} =
\frac{0,6931}{0,0339} = 20,4468 min$

$(T_{1/2})_{2} = \displaystyle \frac{ln(2)}{\lambda_{2}} =
\frac{0,6931}{0,0031} = 223,5959 min$

\end{center}
