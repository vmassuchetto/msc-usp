<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
    <title><?php _e('Pesquisa de Perfil Comportamental em Ambientes de Trabalho'); ?></title>
    <link rel="stylesheet" href="css/normalize.css" />
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <script src="js/vendor/custom.modernizr.js"></script>
    <style>
        .alert-box ul { margin:10px 20px; }
        .arquetipo-slider-panel { margin:20px 0; }
        .escalas { margin-top:5px !important; }
        .promocao { display:none; }
    </style>
</head>

<body>
