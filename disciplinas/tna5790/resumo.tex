% Classe do documento
% -------------------

\documentclass[
	12pt,                               % tamanho da fonte
	oneside,                            % impressão em um lado
	a4paper,                            % tamanho do papel.
	chapter=TITLE,                      % títulos de capítulos em letras maiúsculas
	english,                            % idioma adicional para hifenização
    spanish,                            % idioma adicional para hifenização
    brazil                              % o último idioma é o principal do documento
]{abntex2}

% Pacotes utilizados
% ------------------

\usepackage{helvet}                     % fonte helveltica
\usepackage[T1]{fontenc}                % seleçã de códigos de fonte
\usepackage[utf8]{inputenc}             % codificação UTF-8
\usepackage{lastpage}                   % usado pela ficha catalográfica
\usepackage{indentfirst}                % denteia o primeiro parágrafo de cada seção
\usepackage{color}                      % controle das cores
\usepackage{graphicx}                   % inclusão de figuras
\usepackage{geometry}                   % espaçamentos de página
\usepackage{microtype}                  % melhorias da justificação
\usepackage{titlesec}                   % tamanhos dos títulos
\usepackage{lipsum}                     % gera textos de exemplo
\usepackage[alf]{abntex2cite}	        % citações no padrão ABNT
\usepackage{ipen}                       % formatação da capa e da folha de rosto

% Configurações dos pacotes
% -------------------------

% Capa e folha de rosto

\titulo{Sistemas de apoio à decisão colaborativos: proposição de uma
  metodologia Web com o uso do brainstorming e da tabela de estratégias}
\autor{Vinicius Massuchetto}
\instituicao{Instituto de Pesquisas Energéticas e Nucleares}
\instituicaosub{Autarquia associada à Universidade de São Paulo}
\local{São Paulo}
\data{2014}
\orientador{Willy Hoppe de Sousa}
\preambulo{Resumo de dissertação apresentado para a disciplina TNA5790 --
  Tópicos Especiais em Tecnologia Nuclear - Aplicações, como parte dos
  requisitos para obtenção do Grau de Mestre em Ciências na Área de Tecnologia
  Nuclear – Aplicações.}

% Informações do PDF

\makeatletter
\hypersetup{
  pdftitle={\@title},
  pdfauthor={\@author},
  pdfsubject={\imprimirpreambulo},
  pdfcreator={LaTeX with abnTeX2},
  pdfkeywords={palavras}{chave}{aqui},
  colorlinks=false,
  hidelinks=true
}
\makeatother

\setlrmarginsandblock{1.5cm}{1.5cm}{*}  % margens direita e esquerda
\setulmarginsandblock{1.5cm}{1.5cm}{*}  % margens superior e inferior
\checkandfixthelayout
\setlength{\parindent}{1.0cm}           % recuo do parágrafo
\setlength{\parskip}{0.1cm}             % espaçamento entre parágrafos
\linespread{1.0}                        % espaçamento padrão de 1,5 entre linhas
\makeindex                              % índice

% Início do documento
% -------------------

\begin{document}

\frenchspacing                          % retira espaços extras entre as frases

\begin{center}
  \includegraphics[width=0.2\textwidth]{./ipen.png}

  {\MakeUppercase{\imprimirinstituicao}} \\
  {\imprimirinstituicaosub} \\ [0.4cm]
  RESUMO DE SEMINÁRIO DE ÁREA (TNA5790) \\ [0.4cm]
  {\MakeUppercase{\imprimirtitulo}} \\
  {\imprimirautor} \\
  {Orientador: \imprimirorientador}
\end{center}

A tomada de decisão é um procedimento constante na vida humana, pois é algo que
fazemos o tempo todo para realizar todo tipo de ações corriqueiras, e mesmo
assim não nos damos conta dos pequenos procedimentos aos quais nos submetemos
para obter e escolher nossas alternativas do dia a dia.  Estas decisões
normalmente possuem alternativas bem definidas e critérios simples, exigindo
pouca estruturação para que resultem em uma boa escolha.

As decisões complexas, no entanto, são aquelas mais difíceis de serem tomadas e
que exigem procedimentos mais analíticos para oferecer uma boa alternativa como
resultado. \citeonline{McNamee2008} introduzem o conceito de tomada de decisão
da seguinte forma:

\begin{citacao}

  A tomada de decisão é uma das coisas difíceis da vida. A verdadeira tomada de
  decisão ocorre não quando você sabe exatamente o que fazer, mas quando você
  não sabe. Quando é necessário equilibrar valores conflitantes, organizar
  situações complexas e lidar com reais incertezas, é quando se alcança a real
  tomada de decisão. E para tornar as coisas mais difíceis, as decisões mais
  importantes na vida pessoal ou profissional são normalmente aquelas que lhe
  colocam em situações que você menos sabe o que fazer. \cite[p.
  1]{McNamee2008}

\end{citacao}

Uma importante etapa dos procedimentos de tomada de decisão consiste na geração
de alternativas. A demanda por esforços criativos desta etapa depende também da
natureza da decisão, ou mais especificadamente da complexidade e subjetividade
do problema que está sendo tratado. Para \citeonline{Dacorso2005} esta é uma
etapa crítica, pouco estudada, altamente impactante na qualidade das decisões e
que exige a criação de mecanismos que favoreçam o exercício da criatividade
para ser bem executada.

Indo de encontro à esta necessidade, acredita-se que a combinação dos conceitos
de qualidade de decisão juntamente com as novas possibilidades de colaboração
postas pelo desenvolvimento tecnológico possam oferecer bons recursos no
processo de formulação de uma metodologia que contribua para o aprimoramento da
etapa de geração de alternativas no processo decisório.

A proposta de abordagem na estruturação do problema visa preparar um maior
número de decisões para a aplicação de métodos sistematizados.
\citeonline{Keeney2004a} afirma que grande parte das ferramentas não se
preocupam em discutir as reais complexidades envolvidas e acabam por
simplificar demais os problemas ao não realizar procedimentos como o
estabelecimento claro de critérios, quantificar as consequências das
alternativas e evitar subjetividades.

Proposta inicialmente em 1939, a técnica de \emph{brainstorming} consiste no
desenvolvimento da criatividade colaborativa de um grupo e consequente
organização e submissão destas ideias à determinado processo decisório ou de
resolução de problemas. Cada vez mais esta metodologia tem sido aplicada
juntamente à Internet, com reuniões virtuais e softwares específicos para
documentação de ideias \cite{Hyde2005}. Este método é capaz de estimular a
discussão e a criatividade entre seus utilizadores, mas há a necessidade de se
organizar as alternativas para um dado problema a partir das ideias geradas.

Para auxiliar nesta relação, \citeonline{Howard1988, Menke1994, McNamee2008}
apresentam um método chamado de tabela de estratégias, que é a especificação de
uma estratégia ao longo de diferentes critérios em formato tabular.
\citeonline{Menke1994} também reforça a capacidade de suporte à criatividade
que as tabelas de estratégia possuem ao esclarecer as diferenças de opinião e
os objetivos da discussão. A relativa liberdade de ideias proposta pelo
\emph{brainstorming} deve ser valorizada neste sentido, estimulando os
participantes a expor estas associações mentais.

O principal interesse da pesquisa está em promover um fluxo estruturado que
contribua para a qualidade das decisões ao atuar principalmente na definição do
problema e na etapa da geração de alternativas. Sobre o ferramental que pode
ser utilizado para isto, \citeonline{Dacorso2010a} argumenta que não somente
métodos prescritivos baseados em discussões e votações podem ser utilizados
para a geração de alternativas, mas também métodos específicos que busquem
mensurar o atendimento ao problema, completude e originalidade das ideias.

O sistema proposto baseia-se em um fluxo de trabalho iterativo para formulação
e discussão de seus elementos que vão desde a etapa de definição do problema
até a eleição das melhores alternativas geradas, com alguns elementos centrais
e outros auxiliares para delinear uma proposta de fluxo de trabalho. São eles:
problema, critério, ideia, comentário, alternativa e votações.

Para o sistema um problema nada mais é do que um descritivo do que deverá
nortear a criação dos demais elementos. Por este motivo, o problema é o
elemento central do sistema sobre o qual todos os demais estarão relacionados.
No fluxo de trabalho proposto tudo o que ocorre a partir da definição de um
problema refere-se à busca de uma boa alternativa para ele.

Para cada problema o sistema possui um escopo de dois tipos de usuário: gerente
e participante, cada qual com suas permissões. Ao abrir um novo problema no
sistema um usuário torna-se gerente dele, devendo então definir quais são os
participantes autorizados a intervir no fluxo de tratamento deste problema,
estabelecer os critérios de avaliação das ideias, e também gerar as
alternativas na tabela de estratégias.

A ideia é o segundo elemento principal, e competem às propostas de abordagem do
problema que está sendo discutido. Ao fornecer uma ideia para o problema, o
participante deve também descrever como a ideia cumpre cada critério deste
problema através da pontuação de cinco estrelas. O bom atendimento de uma ideia
ao problema segundo um critério deve ser representada por uma alta pontuação
deste critério. A fim de definir exatamente o atendimento das ideias ao
problema é que se descreve o significado, de um a cinco, de cada uma das
pontuações dos critérios.

As alternativas são a aplicação das ideias no contexto da tabela de estratégias
representadas como linhas, e consistem nada mais do que na combinação de várias
ideias. Quando o gerente julgar que há um bom número de ideias de qualidade já
fornecidas ele pode iniciar a construção da tabela de estratégias ao selecionar
um conjunto de ideias para o número de alternativas que ele julgue viável. Como
as alternativas podem conter um diferente número de ideias, a pontuação dos
critérios para cada uma das ideias resultará em uma média de aproveitamento da
alternativa, indicando de forma quantificada como é o atendimento desta
alternativa ao problema utilizando os critérios como métrica.

Entendendo que a revisitação de métodos já descritos e disseminados na
literatura pode oferecer novos elementos à medida que outras áreas também se
desenvolvem, o DNStorm busca inovar ao introduzir novas aplicações às
possibilidades tecnológicas de hoje, viabilizando a tomada de decisão
colaborativa sobre problemas mais complexos de forma não presencial e
descentralizada. As limitações do sistema podem ser melhor discutidas,
existindo a oportunidade de identificação dos vieses a que os usuários estão
sujeitos durante a utilização do software. Certamente existem outras limitações
que serão detectadas a medida que se percorrer a curva de aprendizado dessa
solução. Cabe, na medida do possível, ao gestor do problema e aos demais
participantes estarem atentos e evitar que vieses que podem ser evitados sejam
devidamente identificados de forma que a solução a ser adotada seja a melhor
possível.

\renewcommand{\bibname}{REFERÊNCIAS BIBLIOGRÁFICAS}
\bibliography{../../msc/bib}

\end{document}
