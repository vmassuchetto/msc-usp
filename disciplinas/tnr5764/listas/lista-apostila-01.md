# TNR5764 -- Fundamentos de Tecnologia Nuclear -- Reatores

1) Quais fenômenos que podem ocorrer como resultado da interação de um núcleo
atômico com um nêutron? Descreva resumidamente no que consiste cada um deles.

São as possibilidades:

* Nêutrons lentos com núcleos atômicos: predomina o espalhamento elástico,
  captura radiativa e captura. A perda de energia cinética por intermédio do
  espalhamento elástico faz com que os nêutrons entrem em equilíbrio térmico
  com o meio material, possibilitando que haja captura posteriormente.
* Nêutrons rápidos: predominam os fenômenos de espalhamento elástico e
  espalhamento inelástico. No espalhamento elástico uma parcela considerável da
  energia cinética inicial dos nêutrons é transferida aos núcleos-alvo. No
  espalhamento inelástico parte da energia cinética inicial dos nêutrons é
  consumida para levar o núcleo-alvo a um de seus estados excitados.

---

2) Defina o que é seção de choque.

Para nêutrons monoenergéticos (energia cinética bem definida) incidindo em um
núcleo atômico conhecido, a probabilidade de ocorrência correspondente a cada
um dos fenômenos mencionados na questão 1 é uma constante denominada seção de
choque.

---

3) O que são nêutrons térmicos? Por que são chamados assim?

Como resultado do processo de moderação, os nêutrons podem alcançar o estado em
que suas energias estejam em equilíbrio com aquela dos átomos ou moléculas do
moderador no qual eles se movem. Diz-se então que os nêutrons estão em
equilíbrio térmico com os átomos ou moléculas do moderador. O comportamento
destes nêutrons é similar àquele dos átomos de um gás e pode ser descrito
razoavelmente bem pela teoria cinética dos gases.

---

4) O que é moderação de nêutrons? Por que é importante?

Moderação é a desaceleração de nêutrons rápidos em nêutrons lentos, um fenômeno
importante porque a fissão e a captura radiativa aumentam com a diminuição da
energia cinética dos nêutrons. O processo mais comum é a moderação de
espalhamento elástico por núcleos leves.

---

5) Defina poder de moderação e razão de moderação.

O poder de moderação é uma medida expressa pelo inverso do centímetro, cuja
capacidade do material em questão de desacelerar nêutrons aumenta juntamente
com a medida. A razão de moderação consiste na mesma medida de forma relativa à
uma determinada substância.

---

9) Quais são os nuclídeos que podem ser utilizados para aplicações em larga
escala da fissão nuclear? De que maneira cada um deles é obtido?

Para núcleos pesados, o nuclídeo natural U-235, e os artificiais U-233 e
Pu-239, pois eles possuem seções de choque altas para fissão por nêutrons
térmicos, assim como meias-vidas longas, ocorrendo naturalmente ou podendo ser
produzidos em quantidades significativas durante intervalos de tempo
praticáveis.

Modos de obtenção:

* U-235: 0,72% do Urânio natural (U-238 é o predominante)
* U-233: Obtido no ciclo do Tório com enriquecimento do Th-232
* Pu-239: Enriquecimento do U-238 gerado como subproduto de fissão do U-235

---

10) Qual é o valor médio da energia liberada em uma fissão? Como é distribuída
essa energia?

A energia de uma fissão se decompõe entre:

* Energia cinética dos fragmentos de fissão
* Energia cinética dos nêutrons da fissão
* Energia dos raios-gama prontos
* Energia do decaimento

O valor da energia dos fragmentos de fissão depende do elemento fissionado, e o
valor médio da energia cinética carregada pelos nêutrons é igual ao número
médio de nêutrons emitidos por fissão multiplicado pela energia cinética média
dos nêutrons.

---

11) O que é criticalidade?

Criticalidade é o estado de um reator com relação à sua capacidade de sustentar
uma reação nuclear em cadeia. O termo 'massa crítica' remete à quantidade
mínima de material físsil necessária para que o reator possa operar neste tipo
de condição.

---

12) Quais são os materiais mais usados na fabricação de elementos de controle
para reatores nucleares? Qual a propriedade principal destes materiais?

Basicamente materiais que são fortes absorvedores de nêutrons, com elevada
seção de choque como o Boro e o Cádmio presentes em materiais como o Carbeto de
Boro e ligas de Prata-Índio-Cádmio.
