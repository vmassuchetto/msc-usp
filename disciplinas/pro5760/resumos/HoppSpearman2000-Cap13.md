# Capítulo 13 -- Um framework para planejamento puxado

## Introdução

Os elementos de gerenciamento de operação tratados no livro consistem em:

* Básico
* Intuição
* Síntese

O livro busca tratar das duas primeiras partes, e agora a estratégia para
tratamento da _síntese_ baseia-se em duas premissas:

1. Problemas em níveis diferentes da organização requerem um nível diferente de
   detalhes, premissas de modelagem e frequência de planejamento.
2. Ferramentas de planejamento e análise precisam ser consistentes ao longo
   destes níveis.

Para o desenvolvimento de métodos que atendam estas duas premissas,
recomenda-se:

1. Dividir o sistema todo apropriadamente de modo a tornar cada porção
   gerenciável;
2. Identificar os links entre as divisões;
3. Usar feedback para reforçar a consistência, acompanhando a análise, o
   planejamento e o controle com parâmetros estimados. O conhecimento sobre
   eles deve ser constantemente atualizado.


## Desagregação

Quebrar os diversos problemas de decisão entre subproblemas gerenciáveis
através de uma hierarquia formal de planejamento.

### Escalas de tempo no planejamento de produção

As _escalas de tempo_ dividem-se em grande (anos a décadas), média (semana para
ano) e pequena (hora para semana), e que devem estar alocados em diferentes
_horizontes de planejamento_. Diferentes horizontes de planejamento implicam em
diferentes _frequências de regeneração_,

### Outras dimensões de desagregação

1. Processos -- organização de plantas de acordo com o tipo de operação;
2. Produtos -- produtos em famílias de produtos;
3. Pessoas -- organização de recursos humanos entre departamentos e gerentes.

### Coordenação

O que distingue um bom sistema de um ruim não é uma boa divisão de seus
problemas, mas quão bem os subproblemas são resolvidos e, especialmente, quão
bem eles são coordenados entre si.


## Previsão

1. Previsão qualitativa -- utiliza o conhecimento das pessoas ao invés de
   modelos matemáticos, e pode usar métodos estruturados como o Delphi.
2. Previsão quantitativa -- utilização de mensuração numérica do histórico e
   outras fontes relacionas à produção. Subdivide-se em modelos casuais (com
   base em outros parâmetros) e modelos de série de tempo (com base em
   parâmetros históricos da produção).

Leis da previsão:

1. Previsões estão sempre erradas!
2. Previsões detalhadas são piores do que previsões agregadas!
3. Quanto mais avançadas no futuro, mais inexatas elas são.

### Previsões casuais

### Previsões em séries de tempo

### A arte de prever


## Planejamento puxado

Benefícios do planejamento puxado:

1. Mais eficientes;
2. Mais fácil de controlar;
3. Mais robusto;
4. Facilitadores em melhorar a qualidade.

Ao poder estimar com certa confiabilidade o tempo de saída de um trabalho
específico, podemos também:

1. Dar prazos factíveis para os clientes;
2. "Simular" a performance de uma linha;
3. Determinar quando que a conclusão dos trabalhos irá satisfazer as datas com
   os clientes, e desenvolver um modelo de otimização para ajustar os tempos de
   liberação dos trabalhos.


## Planejamento hierárquico

Proposta de planejamento para sistemas puxados subdividido em três níveis
correspondentes ao plano estratégico, tático e de controle.

### Planejamento de capacidade e estrutura

1. Tempo de vida dos produtos -- os equipamentos precisam se pagar durante o
   tempo de vida do produto ou ser suficientemente flexível para também
   fabricar outros futuros produtos;
2. Opções de fornecedores -- (a) comprar ou vender: decisão deve ser tomada não
   somente no custo, pois fazer internamente também tem seus benefícios como
   aprendizado e controle do tempo de entrega; (b) a transição crescente de
   fabricante para distribuir não é ruim, mas deve contar com uma análise
   cuidadosa com a mudança de mercado; (c) quando envolve em fazer ou não o
   produto, o que já consiste em um planejamento da capacidade -- e vale
   lembrar que dar um negócio a um fornecedor o torna um potencial competidor,
   vide exemplo da IBM e da Microsoft;
3. Preço -- saber o lucro sobre vendas dos custos de configuração de um
   equipamento, por exemplo;
4. Valor do tempo e dinheiro -- uma análise da depreciação dos equipamentos
   pode ter um impacto significativo na sua escolha;
5. Confiabilidade e manutenibilidade -- aumento do tempo de trabalho de um
   equipamento ao controlar seu tempo de manutenção;
6. Efeitos de gargalo -- o aumento da capacidade em recursos que consistem em
   pontos de gargalo tem um efeito muito maior naqueles que não consistem,
   porém altos investimentos nestes pontos não são simplesmente justificáveis;
7. Efeitos de congestão -- a variabilidade em equipamentos (normalmente falhas)
   é um fato equipamento em seu rendimento, e confiabilidade e manutenibilidade
   podem vir a ser importantes neste controle.

### Planejamento da força de trabalho

1. Disponibilidade do trabalhador -- estimativas devem considerar pausas,
   férias, treinamento e outros fatores que reduzem sua disponibilidade;
2. Estabilidade da força de trabalho -- a habilidade de uma empresa em recrutar
   pessoal qualificado, assim como todo o comportamento de seus trabalhadores
   podem ser bastante afetados pela mudança no tamanho da força de trabalho;
3. Treinamento de funcionários -- tempo e custo necessário para colocar um
   trabalhador em produtividade plena;
4. Flexibilidade de curto prazo -- o planejamento da força de trabalho precisa
   olhar além do plano de produção para considerar as contingências não
   planejadas, como pedidos emergenciais, sucesso de um novo produto, etc.
5. Flexibilidade de longo prazo -- trabalhadores não são uma mera entrada no
   campo de produção, mas sim uma fonte chave de agilidade, principalmente nas
   novas práticas que contam com o aprendizado conjunto de gerentes e
   trabalhadores;
6. Melhoria da qualidade -- muitos dos procedimentos de qualidade estão sob
   controle direto dos trabalhadores.

### Planejamento agregado

Planejamento quanto de cada produto deve ser feito de acordo com o tempo, o que
é dependente de cada tipo de fábrica e tipo de operação. Pode envolver o
planejamento da produção ao longo do tempo, balanceamento do tempo de produção,
aumento ou redução da equipe.

### WIP e sistemas de cotas

* Contagem de cartões -- não faz muito sentido desenvolver sistemas
  computacionais sofisticados para o controle de WIP; uma vez que o sistema
  está operante de modo estável, fazer reduções incrementais;
* Cotas de produção -- Ideia de estabelecer uma quantidade periódica de
  trabalho que será quase sempre completada durante um período estipulado;

### Controle de demanda

### Sequenciamento e agendamento

### Shop floor control

### Simulações em tempo real

### Acompanhamento da produção

# Glossário

* ERP -- enterprise resource planning, sistemas de informação que suportam os
  processos internos de uma empresa;
* FGI -- finished goods inventory, produtos realizados;
* JIT -- just in time, produção com redução de estoques e custos decorrentes ao
  determinar que nada deve ser produzido, transportado ou comprado antes da
  hora;
* MRP -- manufacturing resource planning, método para o planejamento efetivo de
  todos os recursos da produção com simulações
* Planejamento empurrado -- produção para pronta entrega;
* Planejamento puxado -- produção para entrega via pedido;
* WIP -- work in process, produtos parcialmente acabados ainda em produção;
