## TNA 5781 Fundamentos de Tecnologia Nuclear - Proteção Radiológica

## Resumo

: Grandezas para medição de radiação

| Nome           | Sigla | Unidade         | Tipo de radiação           | Meio     | Equação             |
|----------------|-------|-----------------|----------------------------|----------|---------------------|
| Querma         | $K$   | $Jkg^{-1} (Gy)$ | Não diretamente ionizante  | Qualquer | $K = \frac{dE}{dm}$ |
| Dose absorvida | $D$   | $Jkg^{-1} (Gy)$ | Qualquer                   | Qualquer | $D = \frac{dE}{dm}$ |
| Exposição      | $X$   | $Ckg^{-1}$      | Eletromagnética ($\gamma$) | Ar       | $X = \frac{dQ}{dm}$ |

: Limites de exposição CNEN-NN-3.01

| Grupo de risco                     | Limite   |
|------------------------------------|----------|
| Menores de 18 anos                 | 0mSv/ano |
| Gestantes                          | 1mSv/ano |
| Acompanhantes de pacientes médicos | 5mSv/ano |

: Tipos de monitoramento

| Caráter       | Tipo                          |
|---------------|-------------------------------|
| Preventivo    | - Irradiação externa          |
|               | - Contaminação de superfície  |
|               | - Contaminação do ar          |
| Confirmatório | Dosímetro pessoal             |

: Níveis de monitoramento

| Nível        | Descrição                                                                              |
|--------------|----------------------------------------------------------------------------------------|
| Registro     | Magnitude justifica o registro da medição                                              |
| Investigação | Avaliação de causas e consequências, e proposição de ações corretivas                  |
| Ação         | Que deve levar à uma ação específica, em situação de emergência e exposição crônica    |

: Princípios conceituais da radioproteção

| Conceito      | Descrição                                                                     |
|---------------|-------------------------------------------------------------------------------|
| Risco         | Por menor que seja a dose, sempre há possibilidade de um efeito deletério     |
| Linearidade   | A relação dose-efeito é linear                                                |

: Princípios dos sistemas de proteção radiológica

| Princípio                 | Descrição                                                                 |
|---------------------------|---------------------------------------------------------------------------|
| Justificação da prática   | A prática precisa resultar em um benefício líquido                        |
| Otimização da proteção    | Radiação tão baixa quanto for razoavelmente exequível                     |
| Limites individuais       | As doses equivalentes individuais não devem exceder os limites da ICRP    |

: Classificação ICRP de efeitos biológicos

| Função                | Classificação     | Precauções                                    |
|-----------------------|-------------------|-----------------------------------------------|
| Valor da dose         | Estocástico       | Sem dose limiar para ocorrência dos efeitos   |
|                       | Determinístico    | Com dose limiar para ocorrência dos efeitos   |
| Tempo de manifestação | Imediata          | Pouco tempo após a exposição                  |
|                       | Tardia            | Longo tempo após a exposição                  |
| Nível orgânico        | Somático          | Manifesta-se no indivíduo exposto             |
|                       | Genético          | Manifesta-se nos descendentes                 |

: Meios de proteção do trabalhador

| Método        | Função com a dose     | Descrição                                             |
|---------------|-----------------------|-------------------------------------------------------|
| Tempo         | $\frac{t}{D}$         | Diretamente proporcional ao tempo                     |
| Distância     | $d^{2}D$              | Inversamente proporcional ao quadrado da distância    |
| Blindagem     | -                     | Depende do tipo de radiação e do material             |

: Conceito de área

| Tipo              | Descrição                 |
|-------------------|---------------------------|
| Livre             | Limites públicos          |
| Supervisionada    | Limites estocásticos      |
| Controlada        | Limites determinísticos   |

: Principais detrimentos biológicos

| Sigla     | Detrimento                        |
|-----------|-----------------------------------|
| R1        | Morte por câncer                  |
| R2        | Encurtamento da vida              |
| R3        | Morbidade por câncer              |
| R4        | Efeito genético nos descendentes  |

: Doses não computadas em monitoramento

| Tipo      | Motivo                    | Exemplo           |
|-----------|---------------------------|-------------------|
| Fundo     | Inerente à natureza       | Radiação cósmica  |
| Médicas   | Em benefício da pessoa    | Raio-X            |
