# Respostas de Questões de Prova

Análise de incertezas no processo decisório

## 2008.03

Alguns vieses podem ser apontados, mas o que parece melhor se adequar à
descrição do estudo é aquele referente ao _status quo_ e à aversão a
mudanças.

A interpretação é um pouco diferente neste sentido, já que o viés coloca que as
pessoas normalmente preferem não agir. A atitude do goleiro, no caso, seria
comportar-se de maneira diferente ao não agir premeditadamente no ato do
pênalti e adotar uma postura mais defensiva, o que pode acaber não sendo visto
como uma atuação exemplar e prejudicando a reputação do goleiro.

## 2010.02

O problema foi diretamente atribuído à falta de um software adequado para
controle de acesso nas catracas, mas o enunciado não fornece evidências
suficientes para que isto seja afirmado. Para chegar nesta resolução, a reunião
do diretor de Eduardo deve ter analisado a maneira como o software é operado,
se o contingente de seguranças é adequado, se existe relatórios gerados pelo
sistema atual e onde eles são utilizados, e diversas outras questões não
necessariamente de ordem técnica da área de sistemas.

Os atores, por trabalharem com isto constantemente, podem ter aceitado
facilmente que a solução seria esta, pois os sistemas são algo com o que
trabalham todos os dias.

Alguns vieses podem ser enunciados:

* A _irracionalidade da soma das escolhas_ admite que a sequência natural das
  decisões que são tomadas podem influenciar o decisor. No caso, Eduardo
  trabalha com a implantação de software e possui facilidade nesta área, e pode
  ser relutante a adentrar em outras áreas porque elas oferecem risco já que
  ele não possui conhecimento nelas.
* A facilidade de aceitação pela TI de que o problema remete ao software pode
  ser advinda da supervalorização desta área pelos envolvidos, pois este é o
  campo em que trabalham e é interessante que ele seja pontuado com evidência.
