# Tópicos Especiais em Gestão do Conhecimento

## Lista de Exercícios 2

#### Vinicius Massuchetto

### Questão 1

Compare suas respostas das questões 2 a 6 com as do “professor” e atribua notas
de acordo com seu próprio julgamento, usando a escala abaixo.

...

| Questão |    0% |   25% |   50% |   75% |  100% |
|---------|-------|-------|-------|-------|-------|
| 2       |       |       |       |       |   X   |
| 3       |       |       |       |       |   X   |
| 4       |       |       |       |       |   X   |
| 5       |       |       |   X   |       |       |
| 6       |       |       |       |       |   X   |

### Questão 2

Supondo que a lista 1 valha 10 pontos no total, qual seria o valor justo de
cada questão:

...

|---------|---|---|---|---|---|---|---|---|
| Questão | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
| Pontos  | 2 | 1 | 1 | 1 | 1 | 1 | 1 | 2 |

### Questão 3

Suponha que você esteja morando em uma ilha isolada dos meios de comunicação,
exceto pelo correio. Nela residem 500 pessoas e que cada pessoa se comunique
semanalmente com apenas outras 10 pessoas. Suponha que você voltou de viagem e
trouxe notícias interessantes. Quanto tempo é necessário para que todos na ilha
saibam das novidades?

Suponha que sempre que 2 pessoas se comunicam elas contem todas as novidades
que sabem. Faça a estimativa mais otimista (rápida) e a mais pessimista
(lenta). Explique suas hipóteses.

Dica usar o ORA para gerar networks típicos e ...

...

Na melhor das hipóteses, as pessoas irão sempre interagir com 10 pessoas
diferentes cada. Com isto, temos que em menos de 3 semanas todas as pessoas
saberão das novidades.

* Semana 1. 10 pessoas
* Semana 2. $10 + 10 \times 10 = 110$ pessoas
* Semana 3. $110 + 110 \times 10 = 1210$ pessoas

Precisamos também definir o que é o pior caso. Em um ponto de vista extremo, a
notícia pode nunca chegar em mais do que 11 pessoas se as 10 pessoas que sabem
a notícia inicialmente interagirem somente com o mesmo conjunto de outras 10.

* Semana 1: 10 pessoas
* Semana 2: 11 pessoas
* Semana 3: 11 pessoas
* Semana $\infty$: 11 pessoas

Ainda pensando em um caso muito lento, a cada interação as pessoas podem
adicionar somente uma nova pessoa ao conjunto das que sabem a notícia. Isto
pode ocorrer se dentre todas as pessoas que sabem a notícia, somente uma delas
não repetir uma pessoa em sua próxima interação. Assim temos que serão
necessárias 490 semanas para propagar a informação para toda a ilha.

* Semana 1: 10 pessoas
* Semana 2: 11 pessoas
* Semana 3: 12 pessoas
* ...
* Semana 490: 500 pessoas

Mas pensando em uma 'pior hipótese' razoável, suponhamos que a cada interação
semanal cada pessoa do conjunto das que sabem a notícia encontre somente uma
pessoa que não sabe, e que as outras 9 sejam pessoas que sabem. O resultado é o
equivalente ao melhor caso com um fato de propagação igual a 1 ao invés de 10,
e temos que em menos de 7 semanas a ilha inteira saberá da novidade:

* Semana 1: 10 pessoas
* Semana 2: $10 + 10 = 20$ pessoas
* Semana 3: $20 + 20 = 40$ pessoas
* Semana 4: $40 + 40 = 80$ pessoas
* Semana 5: $80 + 80 = 160$ pessoas
* Semana 6: $160 + 160 = 320$ pessoas
* Semana 7: $320 + 320 = 640$ pessoas

### Questão 4

Considere a rede da figura 1.e faça os seguintes cálculos na mão (sem usar o
ORA):

a) Monte a matriz de adjacência equivalente

> ...

|---|---|---|---|---|---|---|---|---|---|
|   | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
| 1 |   | X |   |   |   |   | X |   |   |
| 2 | X |   | X | X | X |   |   |   |   |
| 3 |   | X |   | X | X | X |   |   | X |
| 4 |   | X | X |   |   |   |   |   |   |
| 5 |   | X | X |   |   |   |   |   | X |
| 6 |   |   | X |   |   |   |   |   |   |
| 7 | X |   |   |   |   |   |   | X |   |
| 8 |   |   |   |   |   |   | X |   | X |
| 9 |   |   | X |   | X |   |   | X |   |

b) Calcule a centralidade de grau de cada nó e depois a média geral, mostre os
valores absolutos e normalizados

> ...

> Número máximo de arestas: 72

| Nó | In   | In/72     | Out   | Out/72    | Total | Total/72  |
|----|------|-----------|-------|-----------|-------|-----------|
| 1  |  2   | 0.0277778 |  2    | 0.0277778 | 4     | 0.0555556 |
| 2  |  4   | 0.0555556 |  4    | 0.0555556 | 8     | 0.1111111 |
| 3  |  5   | 0.0694444 |  5    | 0.0694444 | 10    | 0.1388890 |
| 4  |  2   | 0.0277778 |  2    | 0.0277778 | 4     | 0.0555556 |
| 5  |  3   | 0.0416667 |  3    | 0.0416667 | 6     | 0.0833333 |
| 6  |  1   | 0.0138889 |  1    | 0.0138889 | 2     | 0.0277778 |
| 7  |  2   | 0.0277778 |  2    | 0.0277778 | 4     | 0.0555556 |
| 8  |  2   | 0.0277778 |  2    | 0.0277778 | 4     | 0.0555556 |
| 9  |  3   | 0.0416667 |  3    | 0.0416667 | 6     | 0.0833333 |

c) Calcule também a centralidade de proximidade (closeness) e de intermediação
(betweeness) para as pessoas 2, 3, 5 e 9

> ...

> _closeness_ = $\overline{d(i)}^{-1}$, sendo $d$ a distância entre nós

| Nó | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | $\sum$ | $\overline{d(i)}$ | _closeness_ |
|----|---|---|---|---|---|---|---|---|---|--------|-------------------|-------------|
| 1  |   | 1 | 2 | 2 | 2 | 3 | 1 | 2 | 3 | 16     | 2.0000000         | 0.5000000   |
| 2  | 1 |   | 1 | 1 | 1 | 2 | 2 | 3 | 2 | 13     | 1.6250000         | 0.6153846   |
| 3  | 2 | 1 |   | 1 | 1 | 1 | 3 | 2 | 1 | 12     | 1.5000000         | 0.6666667   |
| 4  | 2 | 1 | 1 |   | 2 | 2 | 3 | 3 | 2 | 16     | 2.0000000         | 0.5000000   |
| 5  | 2 | 1 | 1 | 2 |   | 2 | 3 | 2 | 1 | 14     | 1.7500000         | 0.5714286   |
| 6  | 3 | 2 | 1 | 2 | 2 |   | 4 | 3 | 2 | 19     | 2.3750000         | 0.4210526   |
| 7  | 1 | 2 | 3 | 3 | 3 | 4 |   | 1 | 2 | 19     | 2.3750000         | 0.4210526   |
| 8  | 2 | 3 | 2 | 3 | 2 | 3 | 1 |   | 1 | 17     | 2.1250000         | 0.4705882   |
| 9  | 3 | 2 | 1 | 2 | 1 | 2 | 2 | 1 |   | 14     | 1.7500000         | 0.5714286   |

> _betweeness_ = $\frac{\displaystyle\sum_{j,k}^{n}c_{j \rightarrow i
> \rightarrow k}}{\displaystyle\sum_{j,k}^{n}c_{j \rightarrow k}}$, sendo $c$ o
> caminho geodésico

| Nó | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | $\sum$ |
|----|---|---|---|---|---|---|---|---|---|--------|
| 1  |   | 1 | 1 | 1 | 1 | 1 | 1 | 1 | 2 | 9      |
| 2  | 1 |   | 1 | 1 | 1 | 1 | 1 | 2 | 1 | 9      |
| 3  | 1 | 1 |   | 1 | 1 | 1 | 2 | 1 | 1 | 9      |
| 4  | 1 | 1 | 1 |   | 2 | 1 | 1 | 1 | 1 | 9      |
| 5  | 1 | 1 | 1 | 2 |   | 1 | 2 | 1 | 1 | 10     |
| 6  | 1 | 1 | 1 | 1 | 1 |   | 2 | 1 | 1 | 9      |
| 7  | 1 | 1 | 2 | 1 | 2 | 2 |   | 1 | 1 | 11     |
| 8  | 1 | 2 | 1 | 1 | 1 | 1 | 1 |   | 1 | 9      |
| 9  | 2 | 1 | 1 | 1 | 1 | 1 | 1 | 1 | 1 | 9      |

> _betweeness_ 2: $20/84 = 0.2380952$

> _betweeness_ 3: $22/84 = 0.2619047$

> _betweeness_ 5: $6/84  = 0.0714285$

> _betweeness_ 9: $16/84 = 0,1904761$

d) Suponha que esta rede represente o relacionamento de “grande sinergia” para
trabalho em conjunto, em uma equipe de 9 pessoas. Suponha que esta equipe seja
essencial para o bom funcionamento de um evento, mas que o Diretor do evento
(por problemas de falta de pessoal) quer transferir uma pessoa para outra
equipe e ele lhe deu 3 opções para escolha (P2, P3 e P5), para qual delas a
perda teria menos impacto? Explique e justifique.

> ...

> A menor centralidade de intermediação dentre os nós citados é mantido pelo nó
> 5, e portanto esta pessoa seria a que menos teria a onerar sua equipe em
> informações.

### Questão 5

Considerando as respostas da pergunta 1 da lista 1 de  todos os membros montem
um texto e tabela de consenso para mim. Minha intenção é usar este texto bem
escrito, correto e bem referenciado que vocês vão produzir como nota
complementar na primeira aula do curso o ano que vem.

### Questão 6

Com as respostas de todos para a questão 7 façam um word cloud e vejam se vocês
conseguem tirar algumas conclusões do conjunto.

