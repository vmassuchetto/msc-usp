\chapter{Revisão da Literatura}

\section{Decisões}

  Seres humanos historicamente não têm lidado muito bem com decisões que
  envolvam incertezas, objetivos conflitantes e interações complexas, e por
  isso o estabelecimento de métodos claros e consistentes ajudam na escolha de
  soluções estratégicas que geram bons resultados e que possuem uma
  fundamentação defensável. Utilizar processos explícitos e com racionalidade
  adequada são objetivos a serem perseguidos para um bom tratamento das
  estratégias para tomada de decisões de qualidade \cite{Ralls1995}.

  Ao definir este conceito, \citeonline{Matheson1998} ilustram o que chamam de
  ``corrente da decisão de qualidade'' -- mostrada na figura
  \ref{Fig:DecisionQualityChain}, e apresentam seis principais requisitos --
  cada um como um elo de corrente -- a serem observados para assegurar que uma
  decisão apresente qualidade satisfatória. A representação de uma corrente
  baseia sua premissa em uma corrente real, pois ela ``é tão forte quanto o seu
  elo mais fraco''. Estes elos são estabelece que uma decisão deve:

  \begin{itemize}

    \item{ser aplicada no contexto apropriado, questionando problemas
    relevantes condizentes com os grandes objetivos em questão}

    \item{possuir alternativas criativas e factíveis, identificando-as e
    avaliando-as}

    \item{ser pautada sobre informações confiáveis e significativas,
    questionando os pontos corretos e obtendo as respostas certas, montando um
    comparativo abrangente de boas informações}

    \item{possuir valores claros para as alternativas, optando por resultados
    de curto e longo prazo conforme as incertezas que eles oferecem}

    \item{ser capaz de estabelecer uma linha lógica razoável, analisando os
    aspectos de experiências anteriores e determinando quais alternativas
    oferecem valor substancial}

    \item{envolver comprometimento para ser completada, ser representativa
    pelas pessoas certas, e capaz de se transformar em trabalho prático}

  \end{itemize}

  \begin{figure}[H]

    \centering
    \frame{\includegraphics[width=.6\textwidth]{./img/decision-quality.png}}

    \caption{Corrente da decisão de qualidade}

    \label{Fig:DecisionQualityChain}

  \end{figure}

  Uma das preocupações centrais que se pretende abordar no trabalho a ser
  desenvolvido trata da geração de alternativas nos processos de decisão. Para
  \citeonline{Dacorso2005} esta é uma etapa crítica, pouco estudada, e que
  exige a criação de mecanismos que favoreçam o exercício da criatividade.
  Esses autores, com base em uma análise bibliográfica selecionada, discutem a
  contribuição de um bom conjunto de alternativas à tomada de decisão:

  \begin{citacao}

    As pesquisas revelaram, segundo \citeonline{Butler2003}, que era possível
    melhorar consideravelmente a qualidade do conjunto de alternativas à medida
    que se oferecia ajuda ao decisor. Isso demonstrou que a geração de
    alternativas além de ser uma etapa crítica no processo de decisão era uma
    tarefa que podia ser significativamente melhorada com a intervenção
    externa. [\ldots] Segundo \citeonline{Adelman1995}, para se alcançar um
    resultado satisfatório na tomada de decisão é imprescindível um bom
    conjunto de alternativas. Em outras palavras, o bom resultado da decisão só
    poderia ocorrer se a boa opção fizesse parte do conjunto de alternativas
    gerado previamente. Curiosamente, a grande maioria das pesquisas sobre
    tomada de decisão refere-se às técnicas para escolha da melhor opção, uma
    vez dado certo conjunto de alternativas. \cite[p. 4, grifo
    nosso]{Dacorso2005}

  \end{citacao}

  O método de \emph{brainstorming} é apresentado -- dentre outros -- por
  \citeonline{Dacorso2005} como um modelo de geração de alternativas, e sua
  descrição é o objetivo da próxima seção.

\section{Brainstorming}

  Proposta inicialmente em 1939, a técnica de \emph{brainstorming} consiste no
  desenvolvimento da criatividade colaborativa de um grupo e consequente
  organização e submissão destas ideias à determinado processo decisório ou de
  resolução de problemas. Cada vez mais esta metodologia tem sido aplicada
  juntamente à Internet, com reuniões virtuais e softwares específicos para
  documentação de ideias \cite{Hyde2005}.

  O procedimento para a técnica em seu formato básico e presencial é descrito
  por \citeonline{Tague2005} como uma sequência de passos ao reunir um grupo de
  pessoas para discutir um problema:

  \begin{enumerate}

    \item{esclarecer as regras da discussão explicando a metodologia de
    \emph{brainstorming}}

    \item{esclarecer o problema a ser discutido em seus detalhes de interesse}

    \item{abrir a sessão de discussão com uma folga de tempo inicial até que os
    participantes possam pensar no problema e começar a fornecer as suas
    ideias}

    \item{registrar todas as ideias fornecidas da forma mais detalhada possível}

    \item{prosseguir coma a geração de ideias até um tempo limite
    pré-estabelecido}

  \end{enumerate}

  Na concepção da técnica, \citeonline{Osborn1963} estabelece que a eficiência
  das ideias fornecidas é regulada por dois principais fatores: adiamento do
  julgamento e quantidade das ideias. O objetivo de se gerar uma grande
  quantidade de ideias acompanha o princípio de que quanto mais ideias se
  possui, maior a probabilidade de se ter uma boa ideia. A técnica prevê que
  julgamentos sobre as ideias fornecidas não devem ser colocados em um primeiro
  momento, pois na ausência de críticas e posicionamentos contrários é possível
  obter um maior número de contribuições por parte dos participantes.

  Adaptações do método inicial foram feitas para as mais diferentes realidades,
  gerando soluções que podem seguir uma aproximação mais tradicional, ou
  introduzir elementos e artifícios inovadores que comumente competem ao estado
  tecnológico da sociedade. Estas variações podem também modificar o método em
  sua estrutura, propondo desde um simples procedimento adicional durante a
  geração de ideias, ou modificando a essência interativa do método, como por
  exemplo, tornando-o não presencial e submetido à novas e diferentes regras.

  Dentre as diversas variações, o \emph{brainstorming} eletrônico é descrito em
  \citeonline{WikiBrainstorming2012} como um procedimento em que uma lista de
  ideias é compartilhada através de uma rede eletrônica, e as contribuições
  tornam-se instantaneamente visíveis a todos os participantes, sendo
  frequentemente feitas de maneira anônima para encorajar a abertura das
  ideias.

  Estudos mostram que a aplicação do \emph{brainstorming} via mídias
  eletrônicas é capaz de suprimir muitas das ineficiências e controvérsias
  apresentadas para esta técnica. Neste aspecto, \citeonline{Gallupe1992}
  apresentam vantagens como a possibilidade dos participantes em expressar
  ideias simultaneamente, o grande número possível de participantes e a
  facilidade de controle do anonimato.

  Desvantagens a respeito do formato clássico da técnica também foram
  identificadas, tal como o fato de que indivíduos trabalhando sozinhos são
  capazes de gerar mais ideias do que se trabalhassem em grupo, ou mesmo de que
  existe a inibição por parte das pessoas em terem suas ideias julgadas.
  \cite{WikiBrainstorming2012}.

  Em relação ao \emph{brainstorming} eletrônico, as desvantagens que podem ser
  observadas competem principalmente ao tempo necessário para se concluir uma
  sessão de geração de ideias, à perda do contato presencial que pode vir a
  favorecer a extroversão e empatia -- e portanto um melhor rendimento -- do
  grupo, e aos incidentes de segurança e fraudes possíveis decorrentes do
  fornecimento de um acesso remoto autenticado aos participantes.

  \citeonline{Kraetschmer2002} esclarecem que o \emph{brainstorming} eletrônico
  é indiferente para grupos pequenos, e pode admitir uma perda para a variação
  de `grupos nominais'. No entanto, torna-se indiscutivelmente eficaz para um
  grande número de pessoas, e neste caso os ganhos são muito maiores do que as
  deficiências em relação aos métodos presenciais.

\section{Tabela de Estratégia}

  O método de \emph{brainstorming} é uma metodologia capaz de estimular a
  discussão e a criatividade entre seus utilizadores, mas há a necessidade de
  se organizar as alternativas para um dado problema a partir das ideias
  geradas.

  Para auxiliar nesta relação, \citeonline{Howard1988, Menke1994} propõem um
  método chamado de tabela de estratégias, que nada mais é do que a
  especificação de uma estratégia ao longo de diferentes áreas, com diferentes
  pautas -- cada qual podendo ser entendida como uma variável de decisão
  individual -- apresentadas na forma de uma tabela.

  Para \citeonline{Dacorso2005} a estruturação do conteúdo, da discussão e das
  estratégias em formato de tabela também são importantes devido às relações
  expostas entre estruturação de problemas e a criatividade cognitiva. Torna-se
  mais fácil para as pessoas buscarem alternativas através da associação com
  experiências anteriores quando o conteúdo encontra-se subdividido e
  estruturado.

  \citeonline{Menke1994} também reforça a capacidade de suporte à criatividade
  que as tabelas de estratégia possuem ao esclarecer as diferenças de opinião e
  os objetivos da discussão. A relativa liberdade de ideias proposta pelo
  \emph{brainstorming} deve ser valorizada neste sentido, estimulando os
  participantes a expor estas associações mentais. A proposta que segue é a
  combinação destes dois elementos, subsidiando uma tabela de estratégias como
  produto estruturado do \emph{brainstorming} remoto realizado por uma
  comunidade virtual.

  Um exemplo apresentado por \citeonline{Howard1988} na figura
  \ref{Fig:StrategyTable} descreve uma estratégia para um grupo corporativo e
  suas divisões dispostas em colunas: utilidades elétricas, exploração e
  produção, distribuição de óleo, dentre outros. De acordo com cada estratégia,
  as possíveis decisões de uma área indicam também decisões nas outras áreas, e
  este conjunto de decisões constitui em uma das alternativas de estratégia. O
  traçado corresponde, por exemplo, a um perfil de prestação de serviços
  (\emph{service business}) para a atividade da corporação.

  \begin{figure}[H]

    \centering
    \frame{\includegraphics[width=.8\textwidth]{./img/strategy-table.png}}

    \caption{Exemplo de tabela de estratégias em um contexto de negócios
    voltado a serviços. Fonte: \citeonline[p. 684]{Howard1988}}

    \label{Fig:StrategyTable}

  \end{figure}

\section{Sistemas de Suporte à Decisão}

  A apresentação da contextualização histórica dos sistemas de suporte à
  decisão é feita por \citeonline{Power2007} através da análise da literatura
  produzida desde a década de 40. Embora a construção de sistemas sofisticados
  fosse ainda muito custosa, foi somente na década de 70 que a demanda por
  tecnologia da informação acelerou e consolidou a necessidade de sistemas que
  fossem capazes de trabalhar com indicadores de previsibilidade voltados a
  eventos não rotineiros e de cunho mais estratégico, aspecto que foi
  beneficiado com o desenvolvimento de técnicas de \emph{data warehousing}
  durante a década de 80 e 90. Juntamente com o reconhecimento de que o tópico
  sobre sistemas desta natureza já tratava de uma linha de pesquisa científica,
  deram-se a estes sistemas o nome de sistemas de suporte à decisão.

  \citeonline{Gorry1971} apresentam um panorama desta necessidade de mudança,
  pois percebem a deficiência e obsolescência dos sistemas de informações
  gerenciais para com as decisões que não estão completamente estruturadas, e
  propõem uma nova plataforma de trabalho baseada no suporte à decisão, e que
  viria a auxiliar altas gerências em assuntos como fusões e aquisições de
  outras empresas, desenvolvimento de novos produtos e tecnologias, exercício
  de relações com outras organizações e atribuição de grandes
  responsabilidades à novas estruturas gerenciais.

  Nos anos 90 tornou-se ainda mais notória a permeabilização do avanço
  tecnológico destes sistemas, e a utilização de softwares distribuídos em
  ambientes de rede tem sido um mecanismo estratégico e de produção cada vez
  mais comum nas organizações. Neste contexto, a forte conversão para
  tecnologias Web, a rapidez e facilidade para armazenamento de grandes
  quantidades de dados, e a orientação descentralizada destes sistemas traz uma
  nova gama de possibilidades, assim como novas exigências funcionais e de
  segurança \cite{Bhargava2001}.

  No contexto aplicado à manutenção de bases de conhecimento,
  \citeonline{Holsapple1996} fornecem uma ampla definição de sistemas de
  suporte à decisão, e identificam cinco características que devem sempre estar
  presentes nestes sistemas:

  \begin{itemize}

    \item{incluir uma base de conhecimento que descreve alguns aspectos do
    universo de tomada de decisão;}

    \item{adquirir e manter o conhecimento descrito;}

    \item{apresentar o conhecimento em seu contexto prático;}

    \item{selecionar qualquer subconjunto do conhecimento armazenado tanto para
    apresentação quanto para derivação de novos conhecimentos;}

    \item{interagir diretamente com o tomador de decisão ou com um participante
    do processo de decisão.}

  \end{itemize}

  Em estudo sobre o compartilhamento de informações em comunidades virtuais,
  \citeonline{Chiu2006} analisam através de diferentes teorias os motivos que
  levam à obtenção de uma base de informações com qualidade nestas comunidades,
  e itens como o acúmulo de uma `reputação virtual' e a percepção de
  substancialidade dos esforços e procedimentos da comunidade são cruciais para
  motivar os usuários a ajudar a construir os objetivos delineados pela
  plataforma. Segundo a perspectiva apresentada, os critérios para se definir a
  quantidade e a qualidade de informação fornecida por usuários são:

  \begin{itemize}

    \item{interação social: força de relações, tempo gasto e frequência de
    comunicação entre os membros da comunidade}

    \item{confiança: expectativa de que os membros da comunidade sempre
    seguirão um conjunto de valores, normas e princípios}

    \item{identidade: característica dos membros em serem tomados como
    indivíduos ou grupos de indivíduos em termos de substancialidade real}

    \item{reciprocidade: troca de conhecimentos que é percebida de maneira
    justa entre os membros, constituindo ações contingentes em termos de
    compensação para as partes envolvidas}

    \item{linguagens comuns: não a língua em si, mas todo o conjunto de
    símbolos, jargões, siglas e expressões que torna possível a compreensão de
    um assunto ou área do conhecimento específica}

    \item{visões comuns: englobamento dos objetivos coletivos e que possibilita
    a combinação de recursos}

  \end{itemize}

  Gerentes que desejam promover plataformas de decisão coletiva e que dependam
  de uma base de conhecimento devem também preocupar-se em ter um ambiente
  atrativo para o trabalho e colaboração, tanto na organização de conteúdo e
  usabilidade quanto na qualidade das informações.
