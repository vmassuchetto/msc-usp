Evolutionary Psychology: A Primer
Leda Cosmides & John Tooby

## Passado e presente da psicologia evolucionária

* Os instintos humanos agem de forma diferente do que nos outros animais, pois
  estruturam o pensamento e auxiliam na sobrevivência.
* Psicologistas cognitivos gastam mais tempo pesquisando sobre problemas em que
  somos ruins em resolver, e não nos que somos bons.

## O modelo padrão

* O modelo antigo interpreta que a mente humana é vazia e preenche-se com a
  experiência advinda do ambiente com o qual o homem se relaciona.

1. O cérebro funciona como um computador, com circuitos projetados para gerar
   comportamentos em resposta à informação fornecida pelo ambiente.
2. O circuito neural foi projetado para resolver problemas de adaptação, e não
   problemas que nossos ancentrais tinham que resolver. As habilidades do homem
   atual são um efeito colateral disso.
3. A maior parte do pensamento do homem é inconsciente, o que pode levar ele a
   ver problemas complicados com uma simplicidade equivocada.
4. Diferentes circuitos neurais resolvem problemas diferentes: visão, audição,
   equilíbrio.
5. O processo de construção destes sistemas neurais é lento e demorou centenas
   de anos, deixando uma série de comportamentos voltados à resolução de
   problemas dos nossos ancestrais.

## Entendendo o projeto dos organismos

Lógica adaptacionista

* Phylogenética: que o homem possui semelhanças com outros serem por
  descenderem da mesma origem.
* A estrutura reflete a funcionalidade, sendo que um organismo pode ser
  particionado entre diferentes adptações. Vários projetos phylogenéticos podem
  ser comparados neste sentido.
* Pode se mostrar que uma característica é adaptativa por (1) possui muitas
  características que são complexamente especializadas para resolver um tipo de
  problema, (2) que impovavelmente surgiram sem um motivo, e (3) não são
  melhores explicadas por serem um subproduto da resolução de um problema
  alternativo.

## Perspectiva adaptacionista

* Foco em arquitetura: todos os seres possuem semelhanças dentro de um universo
  típico de arquitetura.
* Não se trata de genética comportamental -- que estuda as diferenças genéticas
  e que causam as variações de morfologia e personalidade.

## Instintos da razão

Motivos de se estudar:

1. As teorias de relação social são bem desenvolvidas e sem ou com pouca
   ambiguidade.
2. A adaptação social tem sido uma pressão de selação sobre os hominídeos por
   milhões de anos.
3. As relações sociais parecem ser parte central e inerente da vida social
   humana.
4. Investigar que a arquitetura mental não é somente uma predominância de um
   pequeno número de mecanismos com conteúdo independente e propósito geral.
