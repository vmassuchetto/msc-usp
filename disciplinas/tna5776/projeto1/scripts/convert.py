#!/usr/bin/python

import networkx as nx
from networkx.readwrite import graphml

def boundary():

    g = graphml.read_graphml('polblogs.graphml')
    ng = nx.Graph()

    for edge in g.edges_iter():
        n0 = g.node[edge[0]]
        n1 = g.node[edge[1]]
        if n0['value'] != n1['value']:
            ng.add_node(edge[0], attr_dict=n0)
            ng.add_node(edge[1], attr_dict=n1)
            ng.add_edge(edge[0], edge[1])

    graphml.write_graphml(ng, 'polblogs-boundary.graphml')

def source():

    g = graphml.read_graphml('polblogs.graphml')
    nodes = g.nodes(data=True)

    for node in nodes:
        for source in node[1]['source'].split(','):
            g.add_node(source, attr_dict={'value': '2.0'})
            g.add_edge(source, node[0])

    graphml.write_graphml(g, 'polblogs-source.graphml')

def reciprocity():

    g = graphml.read_graphml('polblogs.graphml')
    ng = nx.Graph()
    nodes = g.nodes(data=True)

    for node in nodes:
        n1 = node[0]
        for n2 in g.neighbors_iter(n1):
            match = False
            for n1_ in g.neighbors_iter(n2):
                if n1 == n1_:
                    match = True
            if match:
                ng.add_node(n1, attr_dict=nx.get_node_attributes(g, n1))
                ng.add_node(n2, attr_dict=nx.get_node_attributes(g, n2))
                ng.add_edge(n1, n2)

    graphml.write_graphml(ng, 'polblogs-reciprocity.graphml')

#boundary()
#source()
#reciprocity()
