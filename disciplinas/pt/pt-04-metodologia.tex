\chapter{Metodologia}

\section{Concepção de Projeto}

  Em um primeiro momento, o projeto constitui uma interface colaborativa de
  cadastro de informações, cujos conteúdos apropriadamente organizados devem
  estar disponíveis para referência de um módulo de suporte para análise de
  alternativas. Em outras palavras, através de uma funcionalidade similar à
  dinâmica de blogs, os conteúdos devem ser disponibilizados para a montagem de
  uma tabela de estratégias.

  Segundo os recursos de \emph{brainstorming} observados, espera-se que as
  discussões no sistema partam de um dado problema, e que existam diferentes
  ciclos e etapas de discussão que possam favorecer a gestão de ideias segundo
  um período de tempo específico. A demarcação das ideias e transferência para
  uma tabela de estratégias é tarefa de um grupo de usuários com permissão
  específica para tal -- os gestores das discussões.

  Outros métodos colocados por \citeonline{Dacorso2005} como o `\emph{Delphi}'
  e os `grupos focados' também funcionam à base de discussões colaborativas,
  mas o que se deseja preservar com o \emph{brainstorming} é o foco em soluções
  e a autonomia das ideias fornecidas em tempo real, sem que elas passem por
  nenhum tipo de compilação para apresentação ao grupo. Neste contexto, o
  \emph{brainstorming} se apresenta mais como um método referencial, fornecendo
  embasamento para a geração de alternativas à medida que a discussão é gerida
  no sistema e aproximando-se mais da dinâmica esperada para um aplicativo Web.

  A interface do sistema com os participantes torna-se então uma exposição de
  ideias estruturadas e classificadas, e que procuram ao mesmo tempo preservar
  os aspectos empiricamente consolidados do \emph{brainstorming}. Cada
  participante possui uma área própria contendo seu histórico de contribuições,
  enquanto também podem adentrar a área dos demais participantes e adicionar
  suas observações.

  A figura \ref{Fig:Workflow} contém uma representação do escopo geral de
  usabilidade esperada, as transições sinalizadas entre as áreas de `Interface
  de Alimentação' e o `Módulo de Decisão' é uma etapa crítica cuja
  especificação é parte do objetivo específico da pesquisa, e representa um dos
  principais êxitos a serem obtidos: a combinação adequada dentre as
  metodologias propostas para geração de alternativas. As `Entidades da
  Interface' são as maneiras pelas quais as informações podem ser inseridas no
  sistema, e a `Análise de Alternativas' é a montagem da tabela de estratégias
  propriamente dita.

  \begin{figure}[H]

    \centering
    \frame{\includegraphics[width=\textwidth]{./img/workflow.png}}

    \caption{Esquema de trabalho na ferramenta}

    \label{Fig:Workflow}

  \end{figure}

\section{Tecnologia, Arquitetura e Processo}

  Embora não seja obrigatória, há uma relação muito próxima dos blogs com as
  metodologias de estruturação de conteúdo. A publicação cronológica, frequente
  e de autoria pessoal é também classificada em taxonomias e palavras-chave de
  acordo com sua inserção, tratando-se de uma estruturação de maneira um pouco
  diferente, mais dinâmica e descentralizada, e que se bem feita rendem bases
  de conteúdo muito funcionais.

  As redes de blogs, ou planetas são de comum utilização corporativa, em que
  gestores escrevem sobre novidades, ideias e estratégias corporativas. É comum
  também dividir blogs entre departamentos, cada qual responsável por informar
  e analisar problemas de suas competências em escopo interno ou para clientes.

  A linha a ser adotada no sistema a ser desenvolvido, no entanto, procura
  trabalhar com características pontuais das funcionalidades de blog,
  principalmente com a possibilidade de autoria múltipla e descentralização na
  alimentação das informações.

  São variadas as soluções disponíveis para sistemas de blogs e que são
  passíveis de utilização para a proposição de um ambiente corporativo,
  seguindo o modelo aberto de produção de software. \citeonline{Pingdom2012}
  lista os principais sistemas como sendo o WordPress, MovableType e Drupal.

  No entanto, propor a aproximação de qualquer sistema de software em um modelo
  corporativo também traz dificuldades em relação à sua manutenção e ciclo de
  vida. Há um esforço corporativo para se unificar cadastros de usuário,
  plataformas e soluções de modo a não exigir a contínua mudança de ambientes
  de produção dos colaboradores. Neste sentido, embora os principais sistemas
  expostos possam, através de extensões apropriadas, atender demandas bastante
  específicas dos processos corporativos, seu uso é fortemente direcionado para
  metodologias de publicação de conteúdo, e não oferecem fácil integração e
  esforço de desenvolvimento para implementação de soluções deste tipo.

  Assim, entende-se que a utilização de \emph{frameworks} -- um conjunto de
  ferramentas e bibliotecas de software construído para auxiliar no
  desenvolvimento de outros aplicativos -- sejam muito mais favoráveis para a
  geração de um produto que possa incorporar outras aplicações úteis a um
  ambiente corporativo, compondo um sistema de suporte à decisão como sendo
  somente um dos aplicativos deste framework.

  A figura \ref{Fig:Architecture} contém um esquema representativo das relações
  estruturais do sistema de suporte à decisão a ser concebido. Para fins de
  extensibilidade e agilidade de desenvolvimento, o software em questão compõe
  um aplicativo de um framework de desenvolvimento.

  \begin{figure}[H]

    \centering
    \frame{\includegraphics[width=0.8\textwidth]{./img/architecture.png}}

    \caption{Organização de produto em desenvolvimento com frameworks}

    \label{Fig:Architecture}

  \end{figure}

  Embora um framework não possa impedir más práticas arquiteturais de
  desenvolvimento, é de grande importância considerar as premissas e
  organizações estruturais que são por ele propostas, pois isso auxilia a
  manutenção de padrões e implementação contínua de novas funcionalidades
  através da uniformização e organização do código, trazendo um baixo custo de
  desenvolvimento a longo prazo.

  \citeonline{Atwood2006} traz um resumo de dezenas de indicadores de qualidade
  de código, dentre eles a falta de reutilização de procedimentos, alta
  complexidade condicional e inconsistência de nomenclaturas. Muitas destas
  características podem ser resolvidas com um padrão comum inicial, e outras
  através da escolha de uma arquitetura apropriada.

  Dentre as opções de arquitetura possíveis para projetos web, a arquitetura
  MVC (\emph{model-view-controller}) destaca-se na engenharia de software por
  adequar-se bem à boa parte das necessidades de construção de esquemas e
  apresentação de dados. O principal objetivo desta organização é separar as
  interações dos usuários com o modo como as informações resultantes destas
  interações são armazenadas, ao mesmo tempo em que favorece a reutilização de
  código para os procedimentos que são comumente realizados. A figura
  \ref{Fig:MVC} contém uma representação clássica das interações entre os
  componentes de uma arquitetura MVC.

  \begin{figure}[H]

    \centering
    \frame{\includegraphics[width=0.6\textwidth]{./img/mvc.png}}

    \caption{Arquitetura MVC}

    \label{Fig:MVC}

  \end{figure}

  As opções de frameworks destinados ao desenvolvimento web são ainda mais
  diversas do que as soluções existentes para sistemas de blogs, e a maioria
  deles já preocupa-se em fornecer uma base própria para utilização da
  arquitetura MVC.

  Em lista mantida pelo site \citeonline{HotFrameworks2012}, os que mais
  destacam-se no mercado atualmente são o Django e o Ruby on Rails. Não existe
  vantagem técnica substancial de um sobre o outro, já que ambos são
  considerados ferramentas poderosas. Embora a comunidade de Ruby on Rails seja
  hoje mais ativa, optamos neste projeto pelo Django devido somente à possível
  integração com outras bibliotecas Python independentes de sistemas web, e que
  podem ser úteis no curso de desenvolvimento a nível de processamento e
  geração de resultados para sistemas de apoio à decisão de modo geral.

  O Django, assim como outros frameworks, utilizam a programação orientada a
  objetos como base de desenvolvimento. A figura \ref{Fig:Prototype} mostra um
  protótipo válido em UML para implementação de um sistema de suporte à
  decisão orientado a objetos, e que abrange três principais módulos:
  autenticação e gerenciamento de usuários; publicação de conteúdo no modelo de
  blogs e sites regidos por tags, categorias, revisões de conteúdo e anexos de
  arquivos; e um pequeno módulo para armazenamento das opções gerais da
  plataforma.

  \begin{landscape}

  \begin{figure}[H]

    \centering
    \frame{\includegraphics[width=1.5\textwidth]{./img/prototype.png}}

    \caption{Protótipo de diagrama de classes}

    \label{Fig:Prototype}

  \end{figure}

  \end{landscape}

  A proposição de um modelo inicial mesmo em fase de pré-projeto busca o
  atendimento à uma metodologia de desenvolvimento orientada à funcionalidades
  -- ou como é conhecida a FDD (\emph{feature driven development}). Embora o
  cenário planejado possua somente um desenvolvedor atuando no projeto,
  espera-se que seja possível utilizar as características deste modelo para
  auxiliar na construção do produto de maneira mais clara para com os
  requisitos esperados.

\section{Testes e Avaliação de Resultados}

  Testes automatizados serão escritos para os procedimentos da aplicação
  utilizando a própria interface de testes do Django, buscando assim eliminar
  erros de lógica e consistência. As frentes de avaliação, conforme considerada
  a multidisciplinaridade do estudo e do desenvolvimento, são as seguintes:

  \begin{itemize}

    \item{efetividade das decisões: através de métodos experimentais e estudos
    de usabilidade -- em partes auxiliado pelo método de orientação a
    funcionalidades, e assertividade no diagnóstico correto de casos simulados}

    \item{desempenho computacional: através do tempo de execução dos
    procedimentos, e do consumo de CPU e memória das instanciações de conexão
    da aplicação com o servidor, incluindo o framework e suas bibliotecas}

    \item{facilidade de uso: baseando-se em sistemas de gerenciamento de
    conteúdo consolidados no mercado e através da percepção de agilidade na
    conclusão das tarefas exigidas pelo sistema}

  \end{itemize}

  Para se ter um resultado direcionado destes indicadores, planeja-se a
  preparação de testes baseados em casos de uso realizado por voluntários em
  uma base de homologação. Neste processo, é requerido ao usuário a execução de
  um conjunto de procedimentos que, quando concluídos, devem ser avaliados
  segundo diferentes atributos que se pautam nos indicadores selecionados.

\section{Infraestrutura Técnica}

  O sistema será projetado para reprodução com todas as suas funcionalidades na
  maioria dos computadores modernos, o que possibilita o seu desenvolvimento em
  ambientes locais sem a necessidade de utilização de camadas remotas para
  atestar as implementações. É necessária a disponibilização de um servidor de
  acesso comum entre os envolvidos com o sistema para que seja possível a
  apresentação das funcionalidades e utilização tanto em âmbito de teste quanto
  de produção.

  A viabilização de um serviço desta natureza já foi consultada juntamente ao
  corpo técnico de tecnologia da informação do IPEN, e as informações a seguir
  seguem como uma primeira proposta de ambiente ainda a ser discutido. Mesmo
  que custos financeiros de projeto não sejam esperados para a viabilização da
  infraestrutura a ser apresentada, a eventual necessidade de disponibilização
  da ferramenta em serviços de hospedagem externos pode ser tranquilamente
  assumida.

  Embora os sistemas operacionais mais utilizados sejam compatíveis com os
  recursos a serem utilizados, será preferida a utilização de distribuições
  Linux atuais para o estabelecimento do serviço em produção. Com esta
  definição busca-se também manter a utilização das últimas versões do
  framework de desenvolvimento, a fim de retirar dele o máximo possível de
  funcionalidades.

  É desejável que seja disponibilizado de um ambiente de homologação para o
  sistema, pois os testes de integração com o ambiente não devem interferir na
  geração de informações dos usuários testadores.

  Diversas bibliotecas Python devem ser integradas à base de código para se
  evitar dependências de ambiente, e com isso espera-se também a modularização
  de um conjunto diferentes de versões de software para a eventual utilização
  de um mesmo servidor tanto para homologação quanto para produção. O servidor
  de banco de dados pode tanto pertencer ao mesmo ambiente como estar
  localizado em um outro servidor dedicado.

  Independentemente dos servidores que irão oferecer os recursos necessários, o
  gerenciamento do código será feito pelo sistema de controle de versões Git,
  com referência remota em uma nuvem pública e gratuita de serviços como
  Bitbucket ou Github. Esta configuração garante a possibilidade de entrega
  automatizada do código a partir do repositório de versionamento sem que o
  desenvolvedor ou a nuvem pública precise requisitar acesso específico aos
  servidores de homologação e produção.

  Este mesmo serviço de armazenamento de código possibilita também a criação e
  disponibilização de uma página web com referência às funcionalidades,
  download e documentação de utilização do sistema. Este conteúdo adicional
  será integrado à base de código como parte do trabalho.

  A documentação técnica será feita a partir da integração do Sphinx,
  uma ferramenta de documentação para Python que pode inspecionar o código e
  gerar páginas visualmente agradáveis e facilmente consultáveis.

  Frente a todo este conjunto de requisitos não-funcionais de infraestrutura,
  uma primeira relação de aplicativos de suporte pode ser proposta conforme a
  tabela \ref{Table:Infraestrutura}. Destes serviços, somente o framework de
  desenvolvimento será integrado à base de código, e portanto não possui
  necessidade de serviço nos servidores em questão.

  \begin{table}[H]

    \centering

    \caption{Requisitos de infraestrutura}
    \vspace{0.5cm}

    \begin{tabular}{
        >{\raggedright\arraybackslash}p{0.40\textwidth}
        >{\raggedright\arraybackslash}p{0.40\textwidth}
    }

      Serviço &
      Aplicativo recomendado \\

      \hline

      Framework de desenvolvimento &
      Django 1.5 \\

      Linguagem &
      Python entre 2.6.5 a 2.7 \\

      Servidor Web &
      Apache $>=$ 2 \\

      Adaptador WSGI para o Apache &
      mod\_wsgi 3.4 \\

      Servidor de banco de dados &
      PostgreSQL $>=$ 8.4 \\

      Sistema de controle de versões &
      Git $>=$ 1.7 \\

      \hline

    \end{tabular}

    \vspace{0.5cm}

    \label{Table:Infraestrutura}

  \end{table}
