## Hopp, Spearman -- Cap 16: Aggregate and Workforce Planning
## Vinicius Massuchetto

### Síntese

Devido à variedade de decisões de longo prazo a serem tomadas em ambientes
organizacionais, muitas formulações do AP (_aggregate planning_) tornam-se
possíveis. As questões referentes aos recursos humanos mostram-se como sendo de
relevância suficiente para tratamento em um único módulo e consequente
composição de uma destas formulações. Para ilustrar este processo, parte-se do
módulo AP mais simplificado possível, que é capaz de acusar problemas básicos,
é extensível para algumas outras aplicações, e é também capaz de demonstrar
como o LP (programação linear) funciona em conjunto com o AP.

Tomam-se diversas variáveis e constantes do processo em uma planilha
eletrônica, considerando atributos como o tempo, custo de inventário, demanda,
quantidade produzida e quantidade vendida. Este modelo é conveniente para
problemas pequenos, mas torna-se complicado de ser adaptado para grandes
escalas -- tópico de pesquisa em que são desenvolvidas linguagens de modelagem
para tornar sua descrição mais compreensível.

O LP funciona com base na busca de soluções ótimas dentro de um tempo pré
definido e algumas condições de parada. Estas condições são: (1) falha -- não
existem soluções viáveis para o dado problema; (2) sem conversão -- o algoritmo
não foi capaz de encontrar soluções ótimas dentro do tempo de computação pré
definido, ou caiu em uma busca indefinida de soluções; e (3) sucesso -- uma
solução ótima foi encontrada.

Modelos estendidos deste método também podem ser formulados através da análise
de algumas situações comuns, e observando os locais que apresentam possíveis
flexibilizações ou gargalos do sistema. Alguns exemplos fornecidos são os de
restrições de recursos humanos, matéria-prima e transporte; e a utilização
adequada dos recursos, tais como falhas de máquina, _setups_ e erros de
programação.

Para o WP (_workforce planning_) pode-se elaborar um modelo LP adicionando-se
uma série de variáveis ao sistema, tais como a capacidade dos postos de
trabalho e custos por hora dos trabalhadores, além de desconsiderar o caráter
variável dos custos de contratação, treinamento, adaptação e desligamento de
pessoal.

Como este tipo de planejamento é feito para longo prazo, a obtenção precisa de
resultados é impraticável ou impossível, e os modelos que venham a ser gerados
para auxiliar a tomada de decisões devem ser ajustados de acordo com o tempo a
fim de serem melhorados. O sucesso de um bom módulo AP é manter o foco no
planejamento de longo prazo, evitando a inserção de muitos controles de curto
prazo.

Algumas considerações gerais podem também ser apresentadas: (1) nenhum módulo
AP ou WP é o certo para todas as situações; (2) a simplicidade promove o
entendimento, sendo até mais importante manter a simplicidade do que cobrir
todos os diferentes problemas possíveis; (3) a programação linear é uma
ferramenta útil para AP e WP, e dado o caráter especulativo deste tipo de
planejamento, na maioria dos casos não vale a pena usar nada mais sofisticado
do que o LP; (4) a robustez é mais importante do que a precisão, pois a
sequência de produção muito provavelmente virá a ser afetada por fatores
difíceis de serem considerados, e o módulo deve ser capaz de ajudar os gestores
e tomarem boas decisões mesmo com a inserção destas restrições.

### Dúvidas

* 16.2.2: Como funcionam as linguagens de modelagem para descrição de problemas
  de larga escala?
* 16.2.2: O texto não cita (ou não pude encontrar) referências de limite de
  tempo para computação das soluções ótimas no LP. Existe uma relação esperada
  entre complexidade e tempo para este modelo?

### Críticas

* O capítulo é bastante prático e simula diversos cenários demonstrando os
  cálculos nas planilhas Excel, porém em alguns momentos assume que o
  entendimento das planilhas é automático por parte do leitor.
* Embora a simplicidade do LP seja recorrentemente citada no texto, seu
  entendimento provavelmente será muito mais facilitado com demonstrações em
  aula.
