Vinicius Massuchetto

Exercícios McNamee & Celona

### Exercício 2.5

A árvore de decisão é o resultado da ordenação e atribuição de pesos
probabilísticos na diagramação de incertezas e suas influências. A construção
de um diagrama de influências é um processo crucial no entendimento do panorama
geral do problema, e que oferece vias seguras para a concepção de árvores de
decisão.

O autor coloca de uma maneira mais específica que "seria muito difícil (senão
impossível) atribuir as articulações de probabilidades diretamente" a uma
árvore de decisão sem a redução do escopo e complexidade que possibilita o uso
efetivo da intuição.

### Exercício 2.9

![](McNamee2008-exe2.9.png)

* Probabilidade de que irá custar mais de $10: 33%
* Probabilidade de que não irá custar nada: 6,6%

### Exercício 2.12

![](McNamee2008-exe2.12.png)

* Probabilidade de 21,09%

Geração do gráfico:

    echo -e "-100 0.1625 \n0 0.5125 \n100 0.8375 \n300 1" | \
    gnuplot -p -e "
        plot '-' using 1:2 \
        smooth bezier notitle \
        with filledcurves above x2=130"
