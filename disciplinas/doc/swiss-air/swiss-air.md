Tabela de estratégias baseada no caso do Grupo SwissAir

### Exercício 1

Mario Corti recebe a incumbência de reverter o processo de falência da Swiss
Air no momento em que toda a diretoria executiva demite-se devido ao montante
de dívidas do grupo.

A sua decisão gira em torno principalmente de sua carreira que pode ser
prejudicada caso ele não consiga reverter a situação da Swiss Air.

Interesses dos stakeholders:

* Mario Corti: Ex-diretor da Nestlé que ingressa na SwissAir para implantar um
  processo de recuperação da empresa. Tem o interesse de reerguer a empresa
