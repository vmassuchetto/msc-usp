#!/usr/bin/python

import re
import csv
import networkx as nx
from latin1_to_ascii import latin1_to_ascii
from networkx.readwrite import graphml

def generate_networks():

    person = dict()
    knowledge = dict()

    # pessoa-pessoa
    k_networks = dict()

    # conhecimento-pessoa
    n_network = nx.Graph()
    # conhecimento-pessoa link > 1
    n1_network = nx.Graph()
    # conhecimento-pessoa link > 5
    n5_network = nx.Graph()

    fu = open('pessoas.csv')
    reader_person = csv.reader(fu)

    fl = open('respostas.csv')
    reader_link = csv.reader(fl)

    for k in reader_link:
        if k[1] not in knowledge:
            knowledge['k' + k[1]] = 0

    for p in reader_person:
        person[p[1]] = {
            'label': latin1_to_ascii(p[1]),
            'value': 'person',
            'location': latin1_to_ascii(p[2]),
            'dept': latin1_to_ascii(p[3]),
            'role': latin1_to_ascii(p[4])
        }
        person[p[1]] = dict(person[p[1]].items() + knowledge.items())

    fl.seek(0)
    for l in reader_link:
        k = l[1]
        node_1 = l[2]
        node_2 = l[3]

        # pessoa-pessoa
        if k not in k_networks:
            k_networks[k] = nx.Graph()
        k_networks[k].add_node(node_1, attr_dict = person[node_1])
        k_networks[k].add_node(node_2, attr_dict = person[node_2])
        k_networks[k].add_edge(node_1, node_2)

        # conhecimento-pessoa
        person[node_2]['k' + k] += 1

    regex = re.compile('^knowledge_(\d+)')
    for p in person:
        for k in knowledge:
            if person[p][k] > 0:
                n_network.add_node(k, attr_dict={'label': k, 'value': person[p][k]})
                n_network.add_node(p, attr_dict=person[p])
                n_network.add_edge(p, k, weight=person[p][k])
            if person[p][k] > 1:
                n1_network.add_node(k, attr_dict={'label': k, 'value': person[p][k]})
                n1_network.add_node(p, attr_dict=person[p])
                n1_network.add_edge(p, k, weight=person[p][k])
            if person[p][k] > 5:
                n5_network.add_node(k, attr_dict={'label': k, 'value': person[p][k]})
                n5_network.add_node(p, attr_dict=person[p])
                n5_network.add_edge(p, k, weight=person[p][k])

    for i in k_networks:
        graphml.write_graphml(k_networks[i], 'rede-pessoa-pessoa-c%s.graphml' % i)
    graphml.write_graphml(n_network, 'rede-conhecimento-pessoa.graphml')
    graphml.write_graphml(n1_network, 'rede-conhecimento-pessoa-n1.graphml')
    graphml.write_graphml(n5_network, 'rede-conhecimento-pessoa-n5.graphml')

generate_networks()
