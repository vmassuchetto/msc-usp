A New Path For Science?
Mark R. Abbott

* EOSDIS: Earth Observing System Data and Information System

Os desafios para ciência no século 21 envolverão a parceria entre governo,
indústria e a academia, sem que a academia monopolize o segmento de pesquisa
básica como é nos Estados Unidos desde a década de 50.

As mudanças computacionais e a intensividade da geração e diversidade de
informação têm modificado os métodos tradicionais de pesquisa científica.

Como a tecnologia da informação afetou a ciência:

1. Comodificação do armazenamento de informações -- que as universidade
   utilizam para serviços como e-mail, backup, etc;
2. Personalização do acesso à informação -- em que os cientistas podem criar
   suas próprias nuvens e empregá-las em pesquisas.

Velho paradigma:

* Interesse predominante do universo acadêmico;
* Foco nas ciências naturais;
* Pesquisas baseadas em processos rigorosos de teste de hipóteses;
* Mecanismos centralizados de publicação e revisão;
* Necessita de grandes recursos e iniciativas;
* Dificuldade em assumir grandes riscos.

Novo paradigma:

* Interesse compartilhado com indústria, governo e academia;
* Multidisciplianridade e ênfase em ciências socioeconômicas;
* Indisciplinado e menos previsível;
* Mecanismos semelhantes à um ecossistema de geração de conhecimento;
* Forte parceria entre geradores e utilizadores de conhecimento;
* Recursos distribuídos em uma rede de serviços;
* Facilidade em assumir grandes riscos.

Os bancos de dados relacionais podem atender os grandes volumes de dados
advindos dos satélites de EOS, mas não capturam o potencial inovativo ao redor
desta ciência.

É provável que a comunidade científica seja relutante em adotar semânticas de
rede e ontologias que possibilitem a geração de conhecimento e colaboração.

Modelos digitais rídigos são difíceis de serem representados, e novas
ferramentas para grandes quantidades de dados podem ajudar a filtrar estes
volumes para um nível gerenciável, e oferecer serviços de apresentação que
facilite um olhar criativo e a colaboração.
