Vincius Massuchetto

Exercícios Ceramax, McNamee & Celona Cap. 6

## Exercício 6.2

In the decision analysis cycle, there is a deterministic structuring phase.
This phase often includes deterministic sensitivity analysis. In Chapter 3,
there is a discussion of probabilistic sensitivity analysis.  What are the
differences between the two kinds of sensitivity analysis?  What effects (or
functions) does deterministic sensitivity analysis have in dealing with complex
problems? Compare these two sensitivities with the sensitivity to risk
tolerance seen in Chapter 5.

---

Se em um primeiro momento as incertezas podem ser classificadas e estruturadas
meramente de maneira condicional, situações reais aproximam-se mais de uma
divisão probabilística, diversificando as possibilidades e afetando a maneira
como uma decisão é tomada de acordo com o seu retorno esperado, balanceando o
valor das informações que ajudam na tomada de decisão.

Uma outra distribuição de riscos sobre as alternativas reflete o nível de fator
humano a que cada decisão está sujeita. O perfil do tomador de decisão e o
contexto de cada situação leva a um diferente risco a ser considerado. Uma
configuração probabilística requere mais análise e discussão sobre suas
possíveis decisões -- já que os retornos esperados tornam-se relativos à cada
escolha. Situações reais raramente se reproduzem em um universo determinístico
e que não comporta distribuição de riscos, sendo portanto um modelo menos
adequado para problemas complexos.

## Exercício 6.4

The ways of making decisions can be divided into normative methods and
descriptive methods. Normative methods describe what people should do in a
given situation. Descriptive methods focus on what people typically do.

For instance, if you face a decision on whether to hold on to a stock or sell
it, a decision analysis (normative method) would tell you what you should do.
Descriptively, many people make this kind of decision by asking their spouse,
broker, or friends to effectively make the decision for them.

Is it possible to reconcile the two methods of decision-making? Provide an
argument and example to support your judgment.

---

Sim, é possível a partir do momento que um método normativo admite frações
descritivas e interpretativas. No exemplo acima, teríamos que um dos
procedimentos do método normativo seria a obtenção de opinião dos amigos sobre
o investimento em questão, e que estas opiniões ajudariam a constituir o fator
de risco a ser considerado na decisão sobre vender ou manter o investimento.

Um outro exemplo é a implementação de políticas públicas de infraestrutura.
Embora muitas obras públicas possam ser úteis para a indústria e o comércio,
muitas vezes elas não atendem um determinado interesse comunitário, e que
depende da situação social e do momento político desta comunidade. Assim, o
sucesso de implantação de uma política pública neste sentido está sujeito ao
risco de aceitação da comunidade que deve ser expressado como parte do fator de
risco da decisão como um todo, juntamente com demais variáveis como o ganho
econômico e capacidade de amparo aos que forem afetados.

## Ceramax

Ceramax: crescimento da venda 15% a.a.

Alternativas identificadas:

* Adicionar 5000 ton de capacidade na fábrica atual em Lockport (NY)
* Adicionar 12000 ton de capacidade numa nova fábrica em Lebanon (IN), região
  com maior demanda atual
* Adicionar 12000 ton de capacidade numa nova fábrica em Birmingham (AL),
  região com maior crescimento da venda

Perguntas:

* É possível montar uma tabela de estratégias a partir das três alternativas?
  Quais colunas estão implícitas nela?
* Que colunas devem existir para soluções deste tipo?
* Os parâmetros fornecidos são suficientes?

---

Colunas implícitas:

* Local
* Capacidade
* Criar fábrica
* Demanda
* Crescimento

Alternativa 1:

* Local: Lockport (NY)
* Capacidade: 5000ton
* Criar fábrica: Não
* Demanda: Null
* Crescimento de venda: Null

Alternativa 2:

* Local: Lebanon (IN)
* Capacidade: 1200ton
* Criar fábrica: Sim
* Demanda: Maior
* Crescimento de venda: Nul

Alternativa 3:

* Local: Birmingham (AL)
* Capacidade: 1200ton
* Criar fábrica: Sim
* Demanda: Null
* Crescimento de venda: Maior

Colunas adicionais:

* Crescimento estimado nas vendas com a implementação

As alternativas não possuem parâmetros suficientes, já que as colunas 'demanda'
e 'crescimento de venda' podem ser convertidas para 'nível de demanda' e 'nível
de crescimento de venda', e assim serem melhor comparadas com os demais
valores.
