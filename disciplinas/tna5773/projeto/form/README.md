## Geração do template de tradução

    xgettext \
        --language=php \
        --from-code=utf-8 \
        --keyword=_e \
        -o lang/form.pot \
        *.php
