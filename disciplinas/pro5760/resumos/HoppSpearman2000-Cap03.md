## Hopp, Spearman -- Cap 3: The MRP Crusade -- Vinicius Massuchetto

O MRP (_material requirements planning_) surge como uma resposta às
ineficiências no agendamento da produção, e busca introduzir as ideias de
demanda independente e dependente -- aquelas respectivamente surgidas fora e
dentro do sistema de produção, e que devem ser atendidas através da
determinação de quantidades e tempos adequados.

A relação entre os itens de produção é dada pela lista de materiais, elencando
os componentes e peças a serem tratados em hierarquia, sugerindo também uma
forma de numeração destes materiais. A agenda da produção (MPS) determina a
programação de recebimentos com as entradas do sistema a partir da matéria
prima, inventário disponível, ordens de compra e de serviço.

O processo básico ocorre através do abatimento da matéria prima disponível para
com a necessária, divisão da demanda em lotes, cálculo de tempo com
determinação dos momentos de início, explosão da lista de materiais de acordo
com o tamanho do lote e relação entre os componentes. Estes procedimentos devem
ser reiterados para todos os níveis de produção assim que seus ciclos forem
completados.

A política de dimensionamento do lote pode dar-se, entre outros métodos, por
(1) 'lot-for-lot' -- um método simples em que a quantidade a ser produzida em
um período é igual às demandas para esta faixa de tempo, sendo compatível com a
filosofia _just in time_, e mais propício a gerar agendas de produção
tranquilas; (2) obter a mesma quantidade de material quando uma ordem é
liberada -- usado para ajustar o custo às particularidades de um sistema de
produção ou do próprio mercado, (3) reduzindo o número de _setups_ ao
combiná-los e realizar os pedidos em períodos fixos -- ao mesmo tempo que lotes
grandes necessitam de menores _setups_, entradas menores podem ser utilizadas
para ajustar a capacidade do sistema; (4) balanceamento por período, buscando
fazer com que o custo de _setup_ seja o mais próximo quanto for possível do
custo de condução das peças ao longo do tempo.

Os principais problemas apresentados pelo MRP constituem-se em: (1)
inviabilização de capacidade dos agendamentos -- uma vez que o tempo de
produção é fixo e não depende da quantidade de trabalho no sistema, o MRP
assume que todas as linhas possuem capacidade infinita, o que torna necessário
o planejamento de capacidade envolvendo cada célula de produção (CRP); (2)
longos planejamentos dos tempos de produção -- devido à pressão para aumento
dos tempos de produção planejada e redução dos custos, grandes inventários
podem vir a ser gerados, e por causa dos tempos constantes de produção o
sistema perde sua responsividade; (3) nervosismo do sistema -- que ocorre
quando pequenas alterações em uma agenda de produção resultam em uma grande
mudança no planejamento, muitas vezes até inviabilizando-o, e para aliviar este
problema recomendam-se ações como a estipualção de diferentes regras de tamanho
de lote somente entre os diferentes níveis da lista de materiais, ou realizar o
congelamento de modificações em certas seções de tempo no MPS.

A identificação destes problemas e a continuação dos estudos no MRP deu origem
ao MRPII (_manufacturing resources planning_), que busca integrar de um modo
efetivo as gerências de planejamento de produção. Introduz os conceitos de
planejamento de longo prazo -- como aumento ou diminuição de recursos,
capacidade, inventário, demanda, e previsões dentre seis meses e cinco anos com
uma certa frequência de replanejamento; médio prazo -- com a conversão das
questões de longo prazo em questões menores e de prazo reduzido, certo
adiantamento sobre as ordens ainda a serem liberadas, e checagens de capacidade
mais rápidas e detalhadas de recursos críticos; e curto prazo -- controles de
entrada e saída com o monitoramento do WIP em cada célula de produção.

A sucessão dos sistemas MRP e MRPII surgiu com o ERP (_enterprise resources
planning_), que busca tratar de todas as operações de uma empresa através de
funcionalidades integradas -- apesar de ter seus problemas de custo e
incompatibilidade. Outros conceitos como SCM (_supply chain management_) e BRP
(_business process reengineering_) tomaram campo na gestão empresarial e hoje
ocupam um papel cruzado, pois de uma lado demandam altos investimentos e
envolvem casos notáveis de falhas de implementação de softwares, e de outro,
casos de sucesso e aumento considerável de desempenho. Um olhar das
contribuições, facilidades e aplicabilidade destes modelos para a gestão de
empresas deve também ser acompanhado da ciência dos problemas que eles
apresentam, pois assim é possível utilizá-los dentro de seus contextos
empregando as contingências adequadas.
