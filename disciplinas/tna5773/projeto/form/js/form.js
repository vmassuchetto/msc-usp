$(function() {
    $('.arquetipo').each(function(){
        var id = $(this).attr('rel');
        if ($(this).find('input').is(':checked'))
            $('#arquetipo-slider-panel' + id).slideDown(500);
    });

    $('.arquetipo').click(function(){
        var id = $(this).attr('rel');
        if ($('#arquetipo' + id).is(':checked'))
            $('#arquetipo-slider-panel' + id).slideDown(500);
        else
            $('#arquetipo-slider-panel' + id).slideUp(500);
    });

    $('#participar_promocao').click(function(){
        if ($(this).is(':checked'))
            $('.promocao').slideDown();
        else
            $('.promocao').slideUp();
    });

    $('.arquetipo-slider').slider({
        min: 0,
        max: 100,
        slide: function( event, ui ){
            var el_display = $(this).parent().next().find('label');
            var el_input = $(this).parent().next().find('input');
            el_display.html(ui.value + '%');
            el_input.val(ui.value);
        }
    });

    $('form').submit(function(e){
        e.preventDefault();
        
        var e;
        var id;
        var field;
        var int_error = false;
        var errors = '';
        var checked = 0;

        $('.general-error').fadeOut(200);
        $('.general-error ul').html('');
        
        var fields = Array('idade', 'sexo', 'escolaridade', 'empresa',
            'tempo_de_estudo', 'tempo_de_atividade', 'alocacao',
            'interacao_com_pessoas');
        for (f in fields) {
            e = false;
            field = $('input[name="' + fields[f] + '"]');
            if (field.attr('type') == 'radio' && !field.is(':checked'))
                e = true;
            else if (field.attr('type') == 'text' && !field.val())
                e = true;
            if (e)
                errors += '<li>' + messages.field + ' "' + fields[f].replace(/_/g, ' ') + '"</li>';
        }

        var profiles = $(this).find('input.arquetipo');
        $.each(profiles, function(){
            if ($(this).is(':checked')) {
                checked++;
            } else {
                id = $(this).attr('rel');
                $('#arquetipo-intensidade' + id).val(0);
            }
        });

        if (checked != 3) {
            errors += '<li>' + messages.select3 + '</li>';
        }

        var checked = $(this).find('input.arquetipo:checked');
        $.each(checked, function(){
            if (int_error)
                return false;

            id = $(this).attr('rel');
            if ($('#arquetipo-intensidade' + id).val() <= 0) {
                int_error = true;
                errors += '<li>' + messages.intensity + '</li>';
            }
        });

        var postdata = $(this).serialize();
        if (errors.length == 0) {
            $('#submit-button').addClass('disabled').val(messages.sending);
            $.post(location.href, postdata, function(data){
                if (parseInt(data) == 1) {
                    $('.main').fadeOut(500);
                    $('.alert-success').delay(500).fadeIn(500);
                } else {
                    errors += '<li>' + messages.problem + '</li>';
                }
            });
        }

        if (errors.length > 0) {
            $('.general-error ul').html(errors);
            $('.general-error').fadeIn(200);
        }        
        
    });

});
