# _Cynefin_, Statistics and Decision Analysis

Simon French -- University of Manchester

## Introdução

Apresentação sobre o autor e suas pesquisas

* Conceito de _Cynefin_ definido por Snowden (2002) que oferece uma
  categorização para contextos de decisão.
* A interpretação inicial é de que o método não teria muito a oferecer do que
  outras formas já descritas.
* Artigo como um 'pequeno pedido de desculpas' a Snowden (2002)

## _Cynefin_

_Cynefin_

* Palavra oriunda do galês que significa 'habitat', mas que também indica
  conceitos de familiaridade e conhecimento.
* Similar à palavra _Ba_ que remete a um lugar de interações para criação,
  gerenciamento e uso do conhecimento (Nonaka, 1991, 1999; Nonaka e Toyama,
  2003).
* _Cynefin_ associa-se mais à comunidades e experiências compartilhadas.

Significado

* Caracteriza várias formas de incerteza.
* Ajuda a estruturar o pensamento sobre inferência estatística e modelos de pesquisa.
* Relaciona-se à tomada, análise e suporte de decisões.
* Relaciona-se ao conhecimento sobre os próprios valores -- e os valores que
  deveríamos nos lembrar sendo a linha de direção da tomada de decisão (KEENEY,
  1992).

Divisões de contexto de decisão

![Reino do conhecimento científico](img/realm.png)

* Espaço conhecido: casos conhecidos, completamente modeláveis. Cursos de ação
  podem ser previstos com boa certeza. Perceber, categorizar e responder
  (KURTZ; SNOWDEN, 2002).
* Espaço conhecível: causa e efeito são geralmente conhecidos, mas mas qualquer
  decisão específica requere mais dados e análises. Onde os métodos usuais de
  pesquisa e análise de decisões são feitos (TAHA, 2006; CLEMEN; REILLY, 1996).
  Perceber, analisar e responder (SNOWDEN, 2002).
* Espaço complexo: muitas interações para classificar as causas e os efeitos.
  Sem modelos quantitativos precisos. Análise de decisões é possível, mas com
  pouca ênfase em detalhes. Aprofundar, perceber e responder (SNOWDEN, 2002).
* Espaço caótico: situações e eventos além da experiência atual. Ações de
  examinação e observação são necessárias até que se possa gradualmente
  contextualizar a decisão entre os outros espaços. Agir, perceber e responder
  (SNOWDEN, 2002).

Comparação com a pirâmide de decisões estratégicas de Simon (1960)

![Comparação com a pirâmide estratégica de Simon (1960)](img/comparison.png)

Os quatro mecanismos de criação, exploração e compartilhamento de conhecimento
(NONAKA, 1991; 1999).

* Socialização: compartilhar conhecimento tácito.
* Extenalização: articular o conhecimento tácito em melhores formatos para
  compartilhamento.
* Combinação: sistematizar o conhecimento explícito em formas mais simples,
  genéricas e aplicáveis.
* Internalização: entender as implicações do conhecimento explícito genérico e
  implementá-lo em nosso comportamento e tomada de decisão.

## Ilustrações de uso do _Cynefin_

Gerenciamento de emergências

* Autoridades pensam estar lidando com os espaços conhecidos e conhecíveis, e
  quando no campo político, com o espaço complexo. Há um deslocamento entre
  esta percepção e a realidade (FRENCH; NICULAE, 2005).
* Exemplo de Chernobyl: decisões imediatamente após o acidente eram conhecidas,
  mas o plano de longo prazo ignorou os aspectos culturais da população que
  negou as recomendações de segurança.

Níveis de suporte a ser oferecido para as decisões

* Nível 0: aquisição, checagem e apresentação de dados.
* Nível 1: Análise e previsão de cenários.
* Nível 2: Simulação e análise de consequências das estratégias, determinação
  da viabilidade, vantagens e desvantagens.
* Nível 3: Avaliação e ranking de estratégias sobre incertezas, balanceando
  vantagens e desvantagens.

Categorização de processos e sistemas de suporte à decisão

![Categorização segundo o Cynefin](img/categorization.png)

Comportamento humano, análise de risco e organizações de alta confiabilidade

* O comportamento humano está envolvido em cerca de 75% dos erros sistêmicos
  (ADHIKARI et al, 2008; FRENCH et al., 2010).
* Análise de confiabilidade humana (HRA) podem basear-se na categorização do
  Cynefin para serem utilizados.
* Organizações de alta confiabilidade (HRO) devem buscar o consenso de questões
  conhecidas em torno de uma única mentalidade, e de questões complexas através
  do gerenciamento de de múltiplas perspectivas de modelos mentais
  compartilhados.

Uso na estruturação de problemas

* Fases de estruturação: Formulação e _sense-making_, análise e decisão.
* Forte relação com a fase de formulação, em que é muito mais visível a área
  conhecível e a área vcomplexa.

_Sense-making_ e inferência estatística

* Comparação do processo de desenvolvimento científico com o _Cynefin_:
  movimentação do conhecimento dentre as áreas descritas.
* Descrição simplória porém útil para iniciar um _sense-making_ em ambiente
  caótico, já que a questão ão tem recebido a devida atenção no campo da
  filosofia da ciência.
* A repetitividade tornou-se a base da indução científica (CHALMERS, 1999).

Escolas de pensamento: posição objetiva ou subjetiva, e como interpreta a
probabilidade

* Subjetiva subjetivista:
* Subjetiva objetivista:
* Lógica objetivista:
* Empírica objetivista:
