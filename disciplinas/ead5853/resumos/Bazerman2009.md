Resumo do Livro: Judgement in Managerial Decision Making -- Max H. Bazerman &
Don A. Moore (7th Edition)

## Capítulo 1: Introdução à Administração da Tomada de Decisões

A anatomia de uma decisão:

1. Definição do problema: Gerentes geralmente (a) definem o problema pela
   solução proposta, (b) não percebem um problema maior, ou (c) definem o
   problema pelos sintomas.
2. Identificação dos critérios: A maioria das decisões exige o atendimento de
   mais de um critério.
3. Pesos dos critérios: Os critérios possuem valores diferentes.
4. Geração de alternativas: Cursos de ação que devem continuar até que o custo
   de obtenção de novas alternativas não sejam maiores que os resultados que
   elas irão fornecer.
5. Valorar cada alternativa em cada critério: Quão bem cada alternativa irá
   solucionar os critérios definidos? Geralmente consiste a etapa mais difícil
   do processo decisório.
6. Calcular a melhor decisão: Depois dos 5 passos se deve (a) multiplicar os
   valores do passo 5 pelo peso de cada critério, (b) adicionar os valores
   pesados através de todos os critérios para cada alternativa, e (c) escolher
   a solução com a maior soma.

Este método assume que:

1. O problema está perfeitamente definido;
2. Todos os critérios estão identificados;
3. O peso de cada critério é preciso e correspondente à sua preferência;
4. As alternativas relevantes são sabidas;
5. Cada alternativa é baseada precisamente em cada critério;
6. Os cálculos são precisos e a alternativa com maior valor é escolhida.

Sistemas de pensamento 1 e 2:

1. Cognitivo, rápido, automático, implícito e emocional. A maioria das decisões
   ocorre no sistema 1, e os erros de decisão são muito mais prováveis neste
   sistema;
2. Raciocínio, devagar, consciente, explícito e lógico;

Os limites da racionalidade humana:

* Prescritivo -- métodos de tomada de melhores decisões utilizando a ciência e
  a razão, e descritivo -- que tomam em conta como as decisões são realmente
  tomadas.
* Frequentemente faltam informações importantes que ajudam a definir o
  problema, e os decisores acabam optando pela primeira solução satisfatória ao
  invés de procurar pela melhor possível.

Julgamentos heurísticos:

* Pessoas buscam tomar decisões baseando-se em estratégias de simplificação dos
  problemas chamadas de heurísticas.
* Heurísticas resultam em julgamentos parcialmente corretos os quais as pessoas
  não são conscientes de que estão usando heurística.

Tipos de heurística:

* Disponibilidade: O quanto um evento está disponível na memória das pessoas
  afeta decisões correlatas.
* Representatividade: Atenção aos estereótipos de um problema. Se uma
  determinada solução foi satisfatória para um problema semelhante.
* Hipótese positiva: Utilização intuitiva de dados seletivos para decisões de
  acordo com os dados de interesse, sem levar em conta o verdadeiro universo
  aplicável do problema.
* Afetiva: Tomar a emoção como base de uma decisão ao invés de adentrar em uma
  análise mais completa e racional.

## Capítulo 2: Vieses Comuns

Heurística da disponibilidade:

1. Facilidade de lembrança baseada nas experiências e vivências anteriores.
2. Recuperabilidade na estrutura de memória que mantemos para mantermos certo
   tipo de conhecimento -- na maneira de pensa.

Heurística da representatividade:

3. Intensidade das frequências básicas -- ignorando dados estruturantes para
   determinadas incertezas, o que leva à uma consideração indevida de dados
   mais superficiais.
4. Intensidade do tamanho da amostra -- acentuações de probabilidade para um
   evento são mais comuns quando o espectro de dados é menor.
5. Desentendimento da chance -- eventos aleatórios nem sempre possuem expressão
   aleatória.
6. Regressão à média -- eventos extremos tendem a ser normalizados à média com
   o passar do tempo.
7. A falácia das conjunções -- uma conjunção cria combinações mais intuitivas e
   tende a ser colocada como mais provável do que um evento singular que é seu
   subconjunto.

Heurística da confirmação:

8. Armadilha da confirmação: indivíduos normalmente buscam dados para confirmar
   uma hipótese, e não para desmentir.
9. Ancoragem: interferência por dados iniciais, em que valores não são
   ajustados suficientemente para estabelecer o valor final.
10. Eventos conjuntivos e disjuntivos: indivíduos superestimam a probabilidade
    de eventos conjuntos e subestimam eventos disjuntos.
11. Superconfiança: Falsa ideia de eficiência ao responder moderadamente a
    questões difíceis.
12. Curso do conhecimento: após saber como um evento ocorreu, as pessoas
    superestimam o grau com que elas teriam previsto este evento.

## Capítulo 3: Consciência Limitada

* Cegueira desatenta: Pessoas tendem a não ver o que não estão procurando, e se
  atentam somente à sua atividade corrente -- como um motorista falando ao
  celular.
* Cegueira de mudança: Em ambientes que existe familiarização, as pessoas
  possuem dificuldades em notar mudanças.
* Focalismo e ilusão focal: Aumentar o peso de subconjunto de um problema ao
  qual se está analisando.
* Consciência limitada em grupos: Grupos discutem mais informações já
  compartilhadas do que aquelas particulares a um integrante, e é necessário
  prover estratégias de compartilhamento de informações para sanar este
  problema.

## Capítulo 4: Formulação e Inversão de Preferências

Formulação e a irracionalidade da soma de nossas escolhas

* A sequência natural de algumas decisões podem potencializar a inconsistência
  e irracionalidade da escolha.
* Normalmente as pessoas buscam alternativas que mostram-se mais aversas à riscos

Preferimos certezas, mesmo pseudo-certezas

* A criação de certezas é mais valorizada do que uma mudança em mesma escala de
  incertezas. Exemplo: reduzir um risco de 0.1 para 0 significa mais do que
  reduzir de 0.2 para 0.1.
* Certezas menores e mutuamente exclusivas que são probabilisticamente menores
  do que alternativas variadas, mutuamente relacionadas, são chamadas de
  pseudoincertezas

A formulação e o abuso das apólices de seguro

* O abuso consiste no oferecimento de quantias adicionais nas apólices de
  seguro para cobrir incertezas menores e que no final das contas não são
  significativas na mesma escala de custo para os clientes.
* As pessoas são dispostas a pagar uma quantia se ela for encarada como um
  seguro ao invés de uma perda monetária. Os clientes ganhariam mais se não
  pagassem por garantias adicionais de um seguro básico e guardassem o dinheiro
  para os acidentes eventuais.

O que vale a pena para você?

* Comprar algo normalmente barato por um preço muito alto por conta da
  necessidade momentânea, ou deixar de comprar algo pelo qual se pode
  perfeitamente pagar porque o preço é considerado muito alto.
* Descontos de preço em coisas mais caras são melhores vistos, mesmo se forem
  dados na mesma proporção do que de coisas baratas.

O valor que colocamos no que possuímos

* Pessoas tendem a colocar muito valor no que possuem, e a baixar o valor no
  que é dos outros.
* A posse gera um tipo de valor irracional com o retorno que um determinado bem
  dá a um indivíduo.

Cálculos mentais

* As pessoas tendem a gastar mais se forem premiadas de maneira simbólica, sem
  que isso as deixem substancialmente mais ricas.
* Perdas pequenas e constantes são mais frustrantes do que uma grande perda
  isolada.

Não faça mal, o viés da omissão e o status quo

* A permanência do 'status quo' e o risco de mudanças fazem com que as pessoas
  tenham uma aversão irracional à mudanças de modo geral

Formulação do bônus/desconto

* Se uma cobrança for justificada como um 'bônus', ela é melhor vista do que se
  se como um 'desconto'.

Inversão de preferências conjunta vs. separada

* Reversão de importância entre duas alternativa quando mais delas são
  apresentadas.

Conclusões

* Os vieses dos capítulos anteriores obedecem heurísticas de julgamento. Os
  efeitos aqui apresentado sugerem processos mentais implícitos.
* Nossa confiança nestas formulações é uma contradição biológica problemática.

