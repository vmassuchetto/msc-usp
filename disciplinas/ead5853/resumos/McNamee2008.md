Resumo do Livro: Decision Analysis For The Professional -- Peter McNamee & John
Celona (4th Edition)

## Capítulo 1: Introdução

Verdadeiras decisões são o balanceamento de interesses conflitantes em
situações complexas, e que teve teve um crescimento após a segunda guerra e a
introdução de computadores. Consiste em duas disciplinas: análise de decisões e
análise de sistemas.

Tomada de decisão:

* Filosofia: Nunca temos toda a informação disponível, e boas decisões nem
  sempre correspondem a bons resultados.
* Estrutura: Conhecimento, alternativas e valores. Decisão que estão sob
  controle, e chances que não estão.
* Processo: Passos práticos com uma abordagem iterativa, focando na decisão e
  possibilitando um facilitador. Modelar o determinístico e probabilístico.
* Metodologia: Diferente de fazer diagramas e árvores, consiste em definir um
  problema e identificar os verdadeiros pontos de decisão e alternativas.

Lidando com problemas complexos:

É comum as análises serem ou muito superficiais, ou muito amplas -- tentando
abranger todos os detalhes de todos os problemas -- tornando-se dispendiosas.

* Descobrir o verdadeiro problema: A tarefa mais crucial de todas que
  corresponde às respostas de quais as alternativas e quais os critérios de
  decisão.
* Manter a análise gerenciável: A angariação de informação na decisão deve
  parar quando os custos (tempo e dinheiro) para consegui-la é maior do que o
  benefício que ela dará ao decisor.
* Perspicácia: O objetivo não é descrever alternativas, mas sim dar ao decisor
  a ideia necessária para escolher dentre as alternativas, e corresponde ao que
  é importante para tomar a decisão, porquê, e quão importante isto é.

Lidando com organizações complexas:

* Abordagem em equipe: DDP (Dialog Decision Process) procura retirar
  informações e alternativas das organizações, importante em decisões entre
  organizações.
* Diálogo estruturado: DDP propõe um diálogo estruturado entre equipes, levando
  ao melhor entendimento (e talvez redefinição) do projeto, à redução do tempo
  de análise, e expondo os participantes às realidades de outras organizações.
* Decisões de qualidade: Definição de quando uma decisão é consistentemente boa
  para ser tomada -- o que é difícil entre vários tomadores de decisão. Uma
  definição de Qualidade de Decisão torna-se útil neste sentido.

Tópicos avançados:

* Lidando com incertezas: Manutenção da clareza pelo método de tomada de
  decisão.
* Lidando com relações complexas: Diagramas de influência para descrever o
  estado das informações.
* Informações confiáveis: Obtenção de informações sobre incertezas -- o que
  normalmente não é bem feito.

## Capítulo 2: Incerteza e Probabilidade

Diagramas de influências:

São uma forma natural de desenvolver e entender uma panorama geral. Incertezas
são círculos, relevâncias são flechas, círculos duplos são incertezas sanáveis
determinadas por influências que podem ser conhecidas.

1. Determinar uma incerteza chave que deve ser resolvida;
2. Descobrir incertezas que ajuda na definição dela;
3. Repetir o passo 2 até que toda as incertezas _importantes_ que influenciam a
   incerteza 1 estejam identificadas.
4. Descobrir incertezas que ajudam na definição das incertezas 2.
5. Quando uma incerteza identificada possui todas as informações em
   recorrência, então forma-se um círculo duplo com a incerteza em questão.

Sobre a utilização das probabilidades:

* Árvores de decisão fornecem um modelo que facilita cálculos de probabilidade
  e desenvolvimento da abordagem ao problema.
* Probabilidades representam o estado de sabedoria sobre determinada abordagem.
  "Não existe probabilidade correta".

Usando a intuição efetivamente:

* Dividir e conquistar: Redução da complexidade e escopo do problema. A
  redução da incerteza ajuda na comunicação de diferentes especialidades em uma
  empresa.
* Teste de perspicácia: Exercício mental que determina se é possível premeditar
  uma chance imediatamente, ou se é necessário saber outras coisas antes de
  responder. Uma previsão imediata significa uma incerteza claramente definida.

Nível de detalhamento de árvores de decisão:

* Mais dados modificam muito os valores, a distribuição de probabilidades, e a
  perspectiva de abordagem?
* Probabilidade do valor esperado: Multiplicação do valor esperado pelas
  probabilidades do galho.

## Capítulo 3: Decisões Sob Incerteza

* Estabelecer critérios de que uma decisão foi bem tomada ajuda a justificá-la,
  principalmente aquelas de cunho estratégico e que possuem mais incertezas.

Nós de decisão e valores:

* Retângulo: decisão.
* Octágono: valor.
* Octágono duplo: valor que não é mais uma incerteza pois se expressa através
  da influência de outros valores.
* Flecha: fluxo de conhecimento.

Regras para diagramas de influência:

1. Sem loops.
2. Um tomador de decisão, somente.
3. Não desconsiderar decisões anteriores.
4. Não desconsiderar informações previamente conhecidas.

Construção da árvore de decisão -- transformação de diagramas de influência:

1. Organizar os nós de forma que as flechas impliquem em uma cronologia
2. Nós de incerteza não devem ficar à esquerda de uma decisão.
3. Se possível todas as flechas devem apontar à esquerda.
4. Fazer o ponto determinístico um ponto final.
5. Finalizar a árvore de decisão em sua forma.


