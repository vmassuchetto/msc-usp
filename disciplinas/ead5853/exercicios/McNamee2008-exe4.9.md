Vinicius Massuchetto

Exercícios McNamee & Celona

### Exercício 4.9

Ursa Major Movies (UMM) has been trying a blind test on all its movies before
releasing them. The test labels a movie as a "Hit" or a "Dud". To make the test
blind, UMM released all movies regardless of the test result. The test result
and the actual history of the movies are shown in the following table.

    Test Result
    Broke box office records  H 5  D1
    Run of the mill           H13  D7
    Disaster                  H 8  D9

**a)** Draw the influence diagram for the test and movie results. Is it in the
order of Nature’s tree? Why or why not?

![](McNamee2008-exe4.9a.png)

A decisão depende de incertezas criadas pelo próprio mecanismo de avaliação do
teste, que virá a oferecer resultados mais substanciais para a decisão de
lançar um filme ou não.

**b)** What is the probability that a film chosen at random out of the studio’s
past movies was a disaster? Run of the mill? Broke records?

    BR     6    RM    20    DI     17
    BR 14.2%    RM 47.6%    DI 40.47%

**c)** What is the probability that a disaster had previously been labeled a “Hit”?
What is the probability that a box office record breaker had been labeled a
“Dud”? Is the test better at detecting good or bad movies?

    H     26    D     17
    H 60.46%    D 39.53%

    Distater + Hit      = 0.4047 * 0.6046 = 24.46%
    Broke records + Dud = 0.1420 * 0.3953 =  5.61%

O teste possui uma melhor sensibilidade para filmes bons, pois eles
dificilmente serão mal classificados nos testes.

**d)** The producer thinks that a new movie is really quite good (a 5 in 10 chance
of being a box office hit, a 3 in 10 chance of being run of the mill). After
learning that the test came up “Dud,” how should the producer revise her
probabilities?

    BR 0.5 * 0.3953 = 0.19765
    RM 0.3 * 0.3953 = 0.11859

Que ao invés de 70% de chance de resultado, o filme possui na verdade 31.62%

**e)** The president thinks that this new movie is like all the others, meaning
that the historical frequencies above apply.  What should he think after
learning that the test result for this movie was “Dud”?

    BR 0.142 * 0.3953 = 0.0561
    RM 0.476 * 0.3953 = 0.1881

Que existe uma probabilidade de 24.42% de que o filme trará resultados.

**f)** Assume a record breaker gives the company a net profit of $20 million; a run
of the mill, $2 million; and a disaster, a net loss of $2 million. What value
would the producer place on the new film before learning the test results?
After learning the test results? How about the president?

Antes do teste:

    Produtor:   20 * 0.500 + 2 * 0.300 = $10.6
    Presidente: 20 * 0.142 + 2 * 0.476 = $3.79

Depois do teste:

    Produtor:   20 * 0.19765 + 2 * 0.11859 = $4.19
    Presidente: 20 * 0.05610 + 2 * 0.18810 = $1.49
