# Forecasting practices of Canadian firms: Survey results and comparisons

Robert D. Klassen , Benito E. Flores

## Método de pesquisa

### Amostragem

* Amostra aleatório de 800 empresas;
* Banco de dados com 12,000;
* Removidas as com rendimentos não declarados ou menores que $10 milhões;
* 800 firmas selecionadas para a pesquisa final;

### Procedimento

* Contato telefônico;
* Carta via correio com lembretes via fax;
* Taxa de resposta de 20% -- 118 respostas;


## Informações gerais

### Demografia

* Portes variados, de 12 a 63,400 empregados;
* 50% são empresas de fabricação e serviços.

### Idade

* 76% das empresas tinham mais de 25 anos.


## Design

### Propósito e uso das previsões

* 86% usam para orçamentos;
* 31% para introdução de produtos.

### Nível das previsões

* 66% usam o mercado nacional;
* 50% usam o mercado regional;
* 43% usam o mercado internacional.

### Horizonte de tempo e frequência de preparação das previsões

* A maioria coleta previsões mensalmente, quadrimestralmente e anualmente;
* Canadenses usam mais expert systems do que os Estados Unidos.

### Recursos comprometidos com a previsão

* A maioria ds empresas não gastam mais do que $50,000/ano em previsão;
* Valor médio anual com previsões é de $1440,000/ano.

### Uso de computadores

* 81% usam computadores com frequência;
* 6% usam mainframes.

### Preparação da previsão

* Responsabilidade distribuída em preparar os resultados;
* Responsabilidade final é do CEO.

### Usuários das previsões

### Fontes de dados

* 36% possuem somente uma fonte de dados para as previsões;
* 31% possuem quatro ou mais.


## Seleção e especificação

### Familiaridade com técnicas de previsão

* Muitos métodos populares de série de tempo nunca foram usados por um bom
  número de empresas;
* O perfil de familiaridade canadense é similar ao estadunidense.

### Uso de métodos alternativos

* O uso de múltiplas técnicas é comum (entre 1 e 7 métodos);
* Méotodo de pesos variados foi o mais reportado.


## Questões sobre avaliação

### Apresentação e gerenciamento

* Previsões com intervalos é algo bastante ensinado na academia;
* 28% das empresas usam intervalos de confiança.

### Revisão

* 80% das empresas que usam computadores modificam o resultado final de
  previsões;
* 7% dizem que as previsões melhoram depois dessas modificações (índice
  arriscado).

### Padrões de avaliação

* Era esperada um alto índice de empresas que monitoram suas previsões, mas 38%
  dizem que não monitoram;
* Monitoramento visual é o método mais usado;
* Métodos mais sofisticados são usados de modo esparso.

### Desempenho da previsão

* Muitas empresas não consideram a sazonalidade;
* 49% não usam coeficientes de sazonalidade;
* 5% usam decomposição claśsica, um método acadêmico altamente recomendado.


## Conclusões

* Empresas nos Estados Unidos e Canadá possuem um perfil parecido com relação
  às previsões;
* Praticantes de previsão não estão usando muitas das ferramentas que
  acadêmicos ensinam.
