## Aula 1

Sistemas de Planejamento e Controle da Produção

    Sistemas de Produção
    Planejamento e Controle da Produção
    Trabalho de Campo

Próxima Aula:

    Hopp & Spearman (2000, cap.13)
    Klassen & Flores (2001)

Leitura Complementar:

    Hopp & Spearman (2000, cap.1)
    Wiendahl et al. (2005)

    Cap. 1 - Introdução ao PCP PDF document
    Klassen & Flores (2001) PDF document
    Wiendahl et al (2005) PDF document
    Mesquita & Castro (2008) PDF document
    Forum - Aula 1
