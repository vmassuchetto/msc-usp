# Tópicos Especiais em Gestão do Conhecimento

## Lista de Exercícios 1

#### Vinicius Massuchetto

### Questão 1

Análise de redes sociais (ARS) é um corpo de conhecimento que evoluiu muito nos
últimos quarenta anos. Faça uma pesquisa na internet e prepare um texto de 2
páginas (máximo de 500 palavras) descrevendo a evolução da ARS do anos 50 até
hoje. Acrescente uma tabela resumo (fora das 500 palavras) com a seguinte
estrutura: 

* Época
* Conceitos introduzidos
* Métodos e ferramentas
* Autores e instituições

Segundo Scott (1996), embora as ideias de análise de redes sociais tenham sido
encontradas em textos gregos antigos, os principais desenvolvimentos nesta área
começaram a ocorrer na década de 30 através do trabalho de diversos grupos
independentes em diferentes áreas do conhecimento. O autor remete estas grandes
áreas ao analisar publicações sobre psicologia, antropologia e a matemática
datadas a partir da década de 20, construindo uma linha do tempo sobre a
análise de redes sociais a partir e que seriam as primeiras contribuições para
este novo campo.

A psicologia investigou as configurações sociais, o comportamento de grupos e
as percepções humanas, atendo-se principalmente às implicações do ambiente
sobre o indivíduos. Os conflitos de grupo passaram a ser investigados na área
da antropologia na década de 50, com questões sobre como as estruturas sociais
afetavam não só o indivíduo e sua produtividade, mas também a sociedade como um
todo.

Com os conceitos de relações sociais publicados e melhor estudados, foi
possível na década de 60 iniciar algumas traduções matemáticas destas relações,
tendo importantes conceitos das ciências sociais -- tal como a noção de 'papel
social' -- na forma matemática voltada à mensurações e modelos.

Pinheiro (2011) apresenta a análise de redes sociais como uma premissa de que a
vida social é a interação entre indivíduos, e que existem influências entre
eles para a ocorrência de certos eventos. Isto se traduziria no mundo dos
negócios, por exemplo, como as definições de quem são líderes e seguidores, e
de como eles interagem para constituir as regras de mercado.

A definição formal de uma rede social, por outro lado, é um conjunto de nós que
consiste em uma rede de membros, e que são conectados por diferentes tipos de
relações denominadas de links, cada qual possuindo um conjunto de atributos que
seja conveniente para uma determinada pesquisa.

Matrizes e grafos são os principais modelos matemáticos formais para a análise
de redes sociais. Pinheiro (2011) discute que ambos estes métodos não são muito
eficientes para a representação de grandes redes, pois uma matriz ou um grafo
com mais de uma centena de componentes torna-se facilmente ininteligível para
interpretação humana, sendo necessário nestes casos a quantificação de
atributos que expressem relevância entre os nós e relações para que eles possam
ser representados simplificadamente. Esta tarefa muitas vezes exige o uso de
algoritmos específicos para tratamento de grafos e construção gráfica das
representações, e tem sido o background de uma recente vertente computacional
na área de banco de dados para armazenamento e pesquisa em quantidades
extremamente grandes de informação.

A INSA -- Rede Internacional para a Análise de Redes Sociais -- deu o nome de
grandes pesquisadores a seus principais prêmios acadêmicos: Linton Freeman --
para estudos acerca de estruturas sociais, Georg Simmel -- para o principal
palestrante do congresso anual da entidade, William D.  Richards -- para a
produção de softwares imprescindíveis à análise de redes sociais.

Uma busca pelo termo _social network analysis_ no Google Scholar referente a um
número de citações maior do que 900 traz os principais autores: John Scott,
Stanley Wasserman, Katherine Faust e Linton Freeman. Uma grande diversidade de
autores possui entre 700 e 500 citações. As instituições pioneiras destacadas
por Scott (1996) durante os anos 50 a 70 são as universidades de Manchester e
de Harvard. Segundo Pinheiro (2011) os grupos que mais destacam-se hoje nesta
área são os centros de pesquisa e universidades situados no Vale do Silício, na
Califórnia.

__Referências__:

PINHEIRO, C. A. R. Social Network Analysis in Telecommunications. Hoboken, New
Jersey: John Wiley & SonsInc. 2011. p. 3-26.

SCOTT, J. Social Network Analysis: A Handbook. Thousand Oaks, CA: Sage
Publications. 1996

__Tabela__:

* Época: Primeiros estudos na década de 30, intensificação na década de 50 e
  auge de estudos atualmente.
* Conceitos introduzidos: Nós, arestas, links, relações.
* Métodos e ferramentas: Teoria dos grafos, modelos matemáticos e bancos de
  dados.
* Autores e instituições: Vale do Silício e institutos de pesquisa em
  antropologia.

### Questão 2

Se você estiver representando uma rede social por meio de um grafo, qual o
significado dos nós (vértices) e das arestas (arcos)?

A representação mais intuitiva é aquela em que pessoas e grupos de pessoas são
nós, e as relações de interesse entre elas são as arestas.

### Questão 3

Suponha que o grafo seja direcionado e valorado. O que isto adiciona no
significado das arestas?

Significa que existe uma orientação específica para cada relação que ocorre no
grafo, e que há um nó de origem e um nó de destino para cada relação
representada. O valor da aresta representa o custo desta relação.

### Questão 4

Dê vários exemplos de relações direcionais e não direcionais ("bounded").

Relações não direcionais podem ser interpretadas como aquelas em que a origem e
o destino não são relevantes ou possuem igual importância a ponto de não serem
representadas, tais como: amizade, conexão com a Internet, relações de
conjuntos, etc.

As relações direcionais precisam ter sentido na origem e destino, tal como:
envio de mensagens e e-mails, transações bancárias, hereditariedade,
hierarquias, linhas aéreas, etc.

### Questão 5

Em que uma relação direcional retribuída (recíproca) difere de uma relação não
direcional?

Quando os custos são equivalentes a representação de relações pode se dar tanto
em um grafo com direções recíprocas ou em um grafo não direcionado. O
direcionamento de uma reciprocidade pode também ser útil para explicitação das
relações, ou quando ela não ocorre de maneira uniforme em um grafo, precisando
ser representada somente em uma parte dele.

### Questão 6

Dê um exemplo de uma rede social com relações multiplexadas. Como isto pode ser
representado em um grafo?

A Internet pode ser um exemplo: Os nós são os dispositivos conectados, arestas
como as conexões estabelecidas entre os servidores, valores das arestas como a
distância ou o custo de transporte dos pacotes de informação. Os servidores
centrais ou _backbones_ terão uma infinidade de relações de conexão com os
provedores menores.

### Questão 7

O que você espera aprender neste curso e qual o uso que pretende fazer destes
conhecimentos?

* Entender o estudo de mapeamento de redes, influências e iterações sociais
* Ter contato com bibliografia adicional para o projeto de mestrado 
* Enfim fazer algum uso prático (e relembrar) as disciplinas de Matemática
  Discreta e Teoria dos Grafos do curso de Ciência da Computação

### Questão 8

Estimulando suas habilidades com o ORA (adaptado de curso de SNA da
universidade de Essex) Considere a rede da figura a seguir:

Entre com estes dados no ORA de 3 modos diferentes:

* a. Criando as entidades e depois puxando as ligações no gráfico;
* b. Criando as entidades e depois definido as ligações no editor da matriz da
  rede;
* c. Criando a matriz de adjacência em planilha excel e depois fazendo copiar e
  colar para o ORA.

