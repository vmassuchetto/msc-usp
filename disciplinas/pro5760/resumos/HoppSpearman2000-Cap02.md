## Hopp, Spearman -- Cap 2: Inventory Control -- Vinicius Massuchetto

O EOQ é uma das primeiras aplicações da matemática em sistemas fabris que para
o cálculo do tamanho do lote leva em conta que (1) a produção é instantânea,
(2) a entrega é imediata, (3) a demanda é determinística (4) e constante com o
tempo, (5) o custo de _setup_ é o mesmo para diferentes tamanhos de lote, e que
(6) os produtos podem ser analisados individualmente.

Estes princípios são questionáveis, tal como o custo de _setup_ que é algo
considerado difícil de determinar. A adição de regras e parâmetros ao EOQ tem
por finalidade a diminuição da sensividade do método, sendo que as fórmulas
iniciais têm sido modificadas em uma infinidade de maneiras. A modificação dos
parâmetros fixos levaram à definição dos lotes dinâmicos que vieram mais tarde
a influenciar a composição do MRP. O procedimento Wagner-Within, por exemplo,
busca um equilíbrio entre o custo de _setup_ e de produção ao alternar o
tamanho dos lotes em cálculos computacionais.

Porém, quando não se possui uma demanda fixa, é necessário simular a
aleatoriedade ou trabalhar os ajustes sobre um modelo determinístico. Os
modelos probabilísticos mais comuns são do tipo _statistical reorder point_,
que define tanto a quantidade em que um inventário precisa ser reposto(_news
vendor_), quanto o momento em que uma reposição precisa ser iniciada (_base
stock_), sendo que o modelo $(Q,r)$ busca determinar ambos os valores em
conjunto baseado no tamanho de uma ordem $Q$ juntamente com o nível de
inventário $r$.

O modelo _news vendor_ assume que (1) os produtos são separáveis, (2) o
planejamento é feito para somente um período, (3) a demanda é aleatória, (4) as
demandas são para pronta-entrega, e (5) os custos de falta e excedente são
lineares. As quantidades apropriadas para este método dependem da da
distribuição da demanda e de comparações relativas entre produzir muito e pouco
do que o necessário.

O modelo _base stock_ assume que (1) os produtos podem ser analisados
separadamente, (2) as demandas ocorrem uma por vez, (3) demandas não atendidas
são adiadas, (4) os tempos de reposição são conhecidos, e (5) as reposição são
feitas uma por vez. Neste sistema o controle sobre pontos de abastecimento é
feito através dos estoques de segurança, cujos níveis em uma produção com
diversos estágios se assimila ao modelo _kanban_.

Já o modelo $(Q,r)$, além de assumir as premissas do _base stock_, também
estipula que (1) existe um custo fixo associado a um pedido de reposição, ou
(2) existe uma restrição no número de reabastecimentos por ano. Neste modelo,
as variáveis $Q$ e $r$ têm um propósito distinto. Valores altos para $Q$
resultam em poucos reabastecimentos por ano mas também em um grande nível de
inventário. Assim como grandes valores para $r$ resultam em grandes inventários
e uma baixa probabilidade de falta de material. Em outras palavras, $min(Q,r)$
resulta em custos de adiamento de pedidos enquanto $max(Q,r)$ resulta em custos
de falta de materiais.

As abordagens apresentadas para a utilização deste modelo são em função do (1)
custo do adiamento de pedidos, (2) custo da falta de estoque e (3)
variabilidade do tempo de entrega. Alguns dos parâmetros nestes modelos são
extremamente difíceis de se obter, tornando a sua especificação impossível. O
objetivo, no entanto, é obter valores razoáveis para representação equilibrada
dos parâmetros. A real qualidade de uma destas políticas deve ser constatada a
partir do acompanhamento do desempenho do sistema de produção.

A complexidade e variedade dos sistemas de gestão de estoques estendem-se por
um grande leque de modelos possíveis e abordagens de decisão com relação à sua
utilização. As alternativas apresentadas aqui são somente uma pequena parte doq
ue foi gerada dada a motivação de se ter os estoques repostos de uma maneira
eficiente e adequada às estratégias de cada negócio.
