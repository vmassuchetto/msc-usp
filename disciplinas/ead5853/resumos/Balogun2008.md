# Cognitively skilled organizational decision making: making sense of deciding

Julia Balogun, Annie Pye, Gerard P. Hodgkinson

## A pesquisa em tomadas de decisão

* Maioria das pesquisas em tomada de decisão deu-se como "processamento de
  informação", analisando as opiniões e preferências pessoais do tomador de
  decisão
* Houve foco na área de heurísticas e vieses
* Pouca atenção no sentido sociológico e de _sensemaking_ -- o entendimento das
  conversações e práticas sociais pelas quais as pessoas constantemente
  negociam e renegociam seus mundos sociais
* Melhores decisões não ocorrem com mais e dados cada vez mais exatos, mas sim
  com um entendimento dos processos sociais que envolvem a decisão

## _Sensemaking_ e as decisões

* _Sensemaking_: fazer algo sensível
* Processo dualístico, cíclico e constante de sentir e descrever para melhor
  abordar a prática, e esclarecer as relações entre _sensemaking_ e
  _sensegiving_.
* Quando as pessoas fazem _sensemaking_, também fazem _sensgiving_ ao fornecer
  pistas e interpretações que elas fizeram através de seu comportamento e
  orientação.

## _Sensereading_ e _sensewrighting_ como processos multiconstitutivos

* _Sensewright_: vem do termo _wrigth_, de fazer, ocupar-se em uma tradição, e
  ser capaz de avançá-la e melhorá-la.
* Quem está no comando podem "estabelecer" significados e influenciar o modo
  como os demais interpretam o ambiente.

## Significados e interpretações

* Lukes (1974) estabelece três tipos de poderes empresariais: recursos,
  processos e significados.
* Significado é o poder simbólico, rituais e co-opções. Envolve a mudança de
  percepções, cognições e preferências (Pfeffer, 1981).
* O poder precisa ser estudado como uma atividade socialmente situada, pois
  mesmo aqueles que o manipulam nas diferentes dimensões estão sujeitos a tomar
  como certa a sabedoria que recebem, tanto dentro como fora de sua organização.
* Para saber fazer _sensewrighting_, é preciso também saber _sensereading_.

## Conclusões

* As perspectivas computacionais e interpretativas na cognição organizacional
  são necessárias para melhores decisões, mas são também muito desconectadas.
* Só através do monitoramento das mudanças em _sensereading_ e _sensewrighting_
  é que se pode observar práticas hábeis, seus impactos e resultados.
* Análises convencionais das tomadas de decisão não são somente mais estáticas,
  elas perdem significado de tempo e contexto para a sensibilidade construída.
* Deve-se focar em fazer a decisão ter sentido do que tomar a decisão,
  compreendendo as interrelações entre os praticantes da cognição e continuando
  os processos organizacionais ao invés de se deparar com diferentes episódios
  de tomada de decisão.
