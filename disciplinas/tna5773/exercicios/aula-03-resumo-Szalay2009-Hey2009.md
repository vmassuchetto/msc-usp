Gray’s Laws: Database-centric Computing in Science
Alexander S. Szalay, José A. Blakeley

Leis de Gray

* A computação científica envolve cada vez mais dados
* A solução está em uma arquitetura "scale-out"
* Trazer computação aos dados, não dados aos computadores
* Iniciar o projeto com "20 consultas"
* Ir de "funcionando para funcionando"

Termos destas leis:

* scale-out: Blocos de particionamento de dados entre nós. Quanto menor e mais
  simples, melhor o desempenho computacional e de rede do sistema em questão;
* computação aos dados: Investigação de técnicas que tornam possível o
  armazenamento e recuperação de grandes quantidades de dados, como o
  MapReduce;
* 20 consultas: Quais são os 20 principais questionamentos que um sistema de
  banco de dados deve responder? O número 20 (2^4.5) é um bom dosador de
  projeto para banco de dados, mostrando-se na prática como um bom tradutor de
  termos científicos para entidades de banco de dados;
* funcionando para funcionando: Construir sistemas modulares que possam
  acompanhar a evolução das tecnologias de armazenamento

Teras a Petas

Dados tornam-se cada vez mais particionados, tornando joins distribuídos cada
vez mais difíceis. Data-crawlers não funcionam bem com joins em tabelas de
cardinalidades muito diferentes.

Melhorias simples consistem em analisar grandes consultas e quebrá-las em
outras menores e mais simples.

O papel da computação em nuvem ainda não é claro para a computação científica,
embora o seja para as aplicações comerciais. Na ciência, ou o dado precisa
estar muito perto do experimentou, ou uma latência muito grande não é
aceitável.

Conclusões

Os projetos científicos geram cada vez mais dados, mas os sistemas de
armazenamento para grandes volumes ainda não estão adequadamente disponíveis.

Cientistas precisam de uma coleção de melhores práticas entre hardware e
software para lidar com grandes volumes.

Podemos lidar com a demanda atual, mas devemos pensar na próxima geração de
demanda de dados que será amplamente aplicável em alguns anos.

As leis de Gray são um excelente guia de princípios para os sistemas de
armazenamento de dados do futuro.
