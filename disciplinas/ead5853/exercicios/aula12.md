# Gestão estratégica e análise de decisões: abordagem da metodologia BSC

### Vinicius Massuchetto

Dentre as diversas escolas de gestão estratégica na administração de
organizações, como por exemplo: SWOT, Porter, RBV, BSC, Blue Ocean, Rumelt,
etc, recorre-se ao método BSC -- Balanced Scorecard -- para ilustrar como a
análise de decisões pode ser associada à gestão estratégica. Para tal,
utilizamos o artigo de Kaplan e Norton (1996) sobre BSC e estratégia, a partir
do que é colocado no capítulo 8 de McNamee e Celona (2008) sobre os processos
de tomada de decisão.

Neste capítulo, McNamee e Celona (2008, p.228) apresentam os chamados
'diálogos' para compor um processo de tomada de decisão, formando o DDP --
Dialog Decision Process, estruturado pelas seguintes fases:

* Definição do problema -- ver o mesmo problema e da mesma maneira;
* Alternativas -- ter um bom conjunto de escolhas disponível;
* Análise -- aprender com as alternativas;
* Decisão -- saber o que fazer.

Para os autores, o DDP vem de encontro à necessidade de comunicação formal
entre a equipe que trabalha sobre o problema e o tomador de decisão durante o
desenvolvimento do processo de tomada de decisão e com isso pode-se acabar
trabalhando sobre o problema errado, escolher alternativas controversas entre
si, e ter uma solução com a qual ninguém está realmente comprometido.

No artigo de Kaplan e Norton (1996, p.56) -- que também são os criadores da
metodologia BSC, apresenta-se uma perspectiva de utilização das medições de
desempenho de uma organização para definição de uma estratégia de negócios.
Embora estas medições tradicionalmente sejam úteis para manter a sintonia entre
indivíduos e organizações com relação a planos pré-estabelecidos, pode-se
também ter uma estruturação para que estes dados sirvam para comunicação,
informações e sistemas de aprendizado; fornecendo um embasamento para que as
estratégias possam ser revisadas, melhor definidas e melhor entendidas.

Para tal, quatro perspectivas de medição são apresentadas:

* Finanças -- crescimento, sustentação e investimentos;
* Clientes -- fração de mercado, retenção, aquisição, satisfação,
  rentabilidade;
* Processos internos;
* Crescimento e aprendizado.

A primeira fase do DDP denominada de _framing_ ou simplesmente 'entendimento',
enfatiza a importância de se definir o problema certo para que se possa
trabalhar nele. McNamee e Celona (2008, p. 230) apresentam algumas ferramentas
e metodologias que auxiliam nesta definição através da discussão e levantamento
das preocupações dos gestores e a importância das decisões. Por outro lado,
para que se possa utilizar o BSC uma empresa precisa passar por um processo
semelhante de identificação e apontamento de indicadores ao empregar discussões
que levem os gestores a apresentar o que mais afeta a sua área. Neste aspecto,
podemos considerar que o BSC traz também um melhor conhecimento do que é
importante para ter melhores resultados, aproximando mais a gestão das questões
estratégicas do que se nenhuma discussão neste sentido fosse feita, e também
contribuindo para o entendimento dos problemas da organização.

Em relação às questões comunicacionais, o vocabulário básico das definições do
DDP e da visão estratégica do BSC assemelham-se porque ambas as discussões
estão preocupadas em buscar as melhores oportunidades através de uma
comunicação consistente. O BSC pode ser considerado como uma ferramenta de
comunicação porque ele constantemente mostra o desempenho de diferentes áreas
de uma organização, e neste sentido segundo Kaplan e Norton (1996, p.64) as
medições podem vir a ajudar a definir uma estratégia da organização. Este ponto
vai de encontro a um dos principais problemas que a DDP procura abordar, que é
a falta de comunicação entre os atores de uma tomada de decisão (MCNAMEE;
CELONA, 2008, p. 227), fazendo do BSC uma possível via de informações comuns
entre a equipe e o tomador de decisão.

Um BSC é mais do que uma coleção de fatores de sucesso, pois um bom
levantamento e balanceamento de medições apresenta objetivos que são
consistentes e que se reforçam mutuamente (KAPLAN; NORTON, 1996, p. 64). Uma
grande preocupação colocada por McNamee e Celona (2008, p. 235) na fase de
alternativas do DDP é o _rationale_ -- a força de coerência, convencimento e
completude das alternativas. Uma boa relação de indicadores de BSC deve também
resultar em medições que transpareçam este comportamento, o que nos leva a
considerar que um sistema de BSC ajuda a embasar as alternativas que estejam
sendo discutidas para determinado problema acerca dos resultados.

McNamee e Celona (2008, p. 236) apresentam a fase de análise como uma avaliação
quantitativa das alternativas -- não como uma mera apresentação de números, mas
como uma reflexão para entendimento, visão e comunicação. Em contexto
semelhante, Kaplan e Norton (1996) apresentam como uma abordagem dos resultados
financeiros no BSC pode relacionar a quantificação dos indicadores com os
resultados de longo prazo: 

> (...) quanto mais dados e evidências forem acumulados, mais as organizações
> serão capazes de prover estimativas consistentes e relações entre causa e
> efeito. (...) Com a especificação destas relações, tanto em tempo quanto
> magnitude entre os indicadores e resultados, é possível transformar as
> reuniões de revisão em uma oportunidade de aprendizado da validade da
> estratégia e quão bem ela está sendo executada (KLAPLAN; NORTON, 1996, p.
> 67, tradução nossa).

Neste contexto, uma visão estratégica em um BSC bem implantado é capaz de
auxiliar a análise de um processo decisório ao ter quantificações substanciais
para as alternativas apontadas na fase anterior do DDP. McNamee e Celona (2008,
p.  236) reforçam a necessidade de não se buscar somente meras respostas nestas
quantificações, mas também de enxergar relações e visões, produzindo melhores
noções sobre o que é intangível num primeiro momento.

A fase final do DDP exposta por McNamee e Celona (2008, p. 241) preocupa-se em
ter decisões que tenham uma ampla aceitação e comprometimento. Como visto ao
longo do capítulo, isto pode ser alcançado mediante o bom embasamento das
alternativas e da comunicação constante entre os atores da tomada de decisão.
Ao deixar transparente o desempenho da organização dentre métricas já
pré-estabelecidas, o BSC está também proporcionando uma aceitação das decisões
que venham de encontro não só à melhoria destes indicadores, mas também à
estratégias que sejam admitidas para o futuro da organização.

O que podemos retirar das comparações entre os diálogos do DDP com a
perspectiva estratégica do BSC é que ambas as discussões somam-se bastante,
inserindo um potencial de promover melhorias processuais uma à outra, e
integrando um ferramental de gestão estratégica para organizações complexas.

## Referências

KAPLAN, Robert S; NORTON, David P. Linking the Balanced Scorecard to Strategy.
California Management Review. vol. 39, n. 1, 1996.

MCNAMEE, Peter; CELONA, John. Decision Analysis for the Professional. 4ª ed.
SmartOrg, Inc. 2008.
