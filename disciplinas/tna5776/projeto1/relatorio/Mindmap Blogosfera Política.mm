<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1371813781941" ID="ID_595218134" MODIFIED="1372277364191" TEXT="Blogosfera Politica">
<node CREATED="1372274482358" ID="ID_1459784375" MODIFIED="1372274488061" POSITION="right" TEXT="Objetivos">
<node CREATED="1372274499429" ID="ID_664569901" MODIFIED="1372274558125" TEXT="Estudar padroes  de liga&#xe7;oes e t&#xf3;picos de discuss&#xe3;o entre blogueiros pol&#xed;ticos">
<node CREATED="1372276739292" ID="ID_1501917108" MODIFIED="1372276818385" TEXT="Primeira hip&#xf3;tese: pessoas com diferentes visoes pol&#xed;ticas s&#xe3;o expostas apenas a informa&#xe7;&#xf5;es que concordam com suas vis&#xf5;es pr&#xe9;vias"/>
<node CREATED="1372276819196" ID="ID_183793917" MODIFIED="1372276866381" TEXT="Segunda hip&#xf3;tese: Blogueiros frequentemente comentam uns dos outros, mesmo que discordando entre si"/>
</node>
<node CREATED="1372274563273" ID="ID_1497106238" MODIFIED="1372274603619" TEXT="Medir o grau de intera&#xe7;&#xe3;o entre blogs liberais e conservadores"/>
<node CREATED="1372274604398" ID="ID_1597708829" MODIFIED="1372274715525" TEXT="Avaliar diferen&#xe7;as nas estruturas das duas comunidades">
<node CREATED="1372274676450" ID="ID_1793502982" MODIFIED="1372274710399" TEXT="Conseguimos separar no ORA as duas comunidades?">
<icon BUILTIN="help"/>
</node>
</node>
</node>
<node CREATED="1372274748195" ID="ID_459879072" MODIFIED="1372274753882" POSITION="right" TEXT="Como?">
<node CREATED="1371813846510" ID="ID_121829163" MODIFIED="1372278732165" TEXT="Estudo 1 (Snapshot de 1 dia) 1000 blogs pol&#xed;ticos">
<node CREATED="1371814014195" ID="ID_1637242387" MODIFIED="1372275074239" TEXT="Blogrolls (lista de links para  outros blogs)"/>
<node CREATED="1371814063966" ID="ID_1909948261" MODIFIED="1371814094249" TEXT="Fotografia da mais ampla blogosphera"/>
</node>
<node CREATED="1371813805747" ID="ID_1527938696" MODIFIED="1372278735674" TEXT="Estudo 2 ( Analise por 2 meses dos posts de 40 blogs A-list)">
<node CREATED="1371813904620" ID="ID_347325635" MODIFIED="1371813918285" TEXT="Com que frequencia eles se referem um ao outro"/>
<node CREATED="1371813921858" ID="ID_1705891353" MODIFIED="1371813939969" TEXT="Sobreposi&#xe7;&#xf5;es de t&#xf3;picos discutidos"/>
<node CREATED="1371813950421" ID="ID_921764073" MODIFIED="1371813961170" TEXT="Dentro das comunidades e entre as comunidades"/>
</node>
</node>
<node CREATED="1372277375488" ID="ID_1244484971" MODIFIED="1372615953336" POSITION="right" TEXT="Entendendo a metodologia">
<node CREATED="1372278890823" ID="ID_575288716" MODIFIED="1372278895806" TEXT="Estudo 1">
<node CREATED="1372277388649" ID="ID_1145910632" MODIFIED="1372277497109" TEXT="Lista de blogs pol&#xed;ticos advindas de diret&#xf3;rios de weblogs">
<node CREATED="1372277463977" ID="ID_219765445" MODIFIED="1372277471587" TEXT="Apenas Democratas e REpublicanos"/>
<node CREATED="1372277502989" ID="ID_1775754019" MODIFIED="1372277536661" TEXT="Contagem de cita&#xe7;&#xf5;es para outros blogs que nao estavam na lista original"/>
</node>
<node CREATED="1372277473146" ID="ID_787565584" MODIFIED="1372277596503" TEXT="Adi&#xe7;&#xe3;o de 30 blogs nao constantes na lista de blogs pol&#xed;ticos mas com mais de 17 cita&#xe7;&#xf5;es por outros blogs da lista"/>
<node CREATED="1372277633813" ID="ID_1882434518" MODIFIED="1372277686213" TEXT="1494 blogs no total">
<node CREATED="1372277650881" ID="ID_606261415" MODIFIED="1372277656777" TEXT="759 liberais"/>
<node CREATED="1372277657773" ID="ID_1809210192" MODIFIED="1372277673166" TEXT="735 conservadores"/>
</node>
<node CREATED="1372277687687" ID="ID_1175362000" MODIFIED="1372277713976" TEXT="Mantidos apenas os que  tinham no m&#xed;nimo 8 kb">
<node CREATED="1372277713978" ID="ID_436876458" MODIFIED="1372277730165" TEXT="676 liberais"/>
<node CREATED="1372277730790" ID="ID_928451304" MODIFIED="1372277806847" TEXT="659 conservadores"/>
</node>
<node CREATED="1372277808016" ID="ID_1288340481" MODIFIED="1372279825645" TEXT="Construida rede de citac&#xe3;o (URL em uma front page de um blog que referenciava outro blog pol&#xed;tico (em fevereiro de 2005)">
<node CREATED="1372277867466" ID="ID_649001685" MODIFIED="1372277883690" TEXT="page link">
<node CREATED="1372277893459" ID="ID_1904706838" MODIFIED="1372277909155" TEXT="link encontrado em qualquer lugar da front page"/>
</node>
<node CREATED="1372277884358" ID="ID_885210250" MODIFIED="1372277891971" TEXT="post citation">
<node CREATED="1372277911104" ID="ID_1546031934" MODIFIED="1372277923658" TEXT="link encontrado dentro de um post"/>
</node>
</node>
</node>
<node CREATED="1372279099368" ID="ID_1773687391" MODIFIED="1372280388002" TEXT="Estudo 2">
<node CREATED="1372278919400" ID="ID_269132182" MODIFIED="1372279803271" TEXT="Contagem de page links usada para encontrar os top 100 conservadores e top 100 liberais da lista dos 1494 blogs (escolheram dois meses passados - setembro e outubro de 2004)"/>
<node CREATED="1372279113143" ID="ID_1352362465" MODIFIED="1372279268250" TEXT="Contagem de cita&#xe7;oes (post citations) nos 200blogs por 2 meses usando o index de posts de weblog do BlogPulse"/>
<node CREATED="1372279279927" FOLDED="true" ID="ID_27882134" MODIFIED="1372688383406" TEXT="defini&#xe7;&#xe3;o dos top 20 conservadores e top20 liberais pelo maior n&#xfa;mero de post citation">
<icon BUILTIN="idea"/>
<node CREATED="1372279393850" ID="ID_1454155484" MODIFIED="1372279421385" TEXT="os 20 conservadores escolhidos estao entre os 44 mais dos top100"/>
<node CREATED="1372279422611" ID="ID_574276249" MODIFIED="1372279442619" TEXT="os 20 liberais escolhidos estao entre os 77 mais dos top 100">
<node CREATED="1372279479229" ID="ID_123643560" MODIFIED="1372279506101" TEXT="Evid&#xea;ncia de que conservadores se ligam mais entre si">
<icon BUILTIN="help"/>
</node>
</node>
</node>
<node CREATED="1372279579556" ID="ID_1314430986" MODIFIED="1372279600311" TEXT="Tabela 1 &#xe9; importante para reconhecer os blogs ">
<node CREATED="1372279972945" ID="ID_1517667361" MODIFIED="1372280092237" TEXT="Confirmam a popularidade do blog">
<icon BUILTIN="help"/>
</node>
<node CREATED="1372280069211" ID="ID_966002644" MODIFIED="1372280096879" TEXT="Confirmam que poucos blogueiros ultrapassam as comunidades ">
<icon BUILTIN="help"/>
</node>
<node CREATED="1372279989374" ID="ID_1691772664" MODIFIED="1372280056155" TEXT="Legenda">
<node CREATED="1372279600313" ID="ID_1318405650" MODIFIED="1372279610525" TEXT="r = classificacao no Top100"/>
<node CREATED="1372280466168" ID="ID_618966495" MODIFIED="1372280507138" TEXT="post = n&#xfa;mero de posts de cada blog coletado nos dois meses de pesquisa"/>
<node CREATED="1372279611521" ID="ID_1619746855" MODIFIED="1372280011250" TEXT="lL = n&#xfa;mero de post citations de blogs  liberais"/>
<node CREATED="1372279665391" ID="ID_1579146534" MODIFIED="1372279961623" TEXT="lR = n&#xfa;mero de post citations de blogs conservadores"/>
</node>
<node CREATED="1372280162935" ID="ID_1253762148" MODIFIED="1372280341934" TEXT="Problema na metodologia: o page link por ser est&#xe1;tico e ter sido coletado em fevereiro, nem sempre representa a popularidade do blog nos meses em que foram colhidas as post citation. SEgundo os autores,  comparacoes feitas com outros rankings de blogs pol&#xed;ticos n&#xe3;o mudaram qualitativamente os resultados alcan&#xe7;ados neste estudo">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1372280388003" FOLDED="true" ID="ID_1515475063" MODIFIED="1372280638060" TEXT="Coletado os posts individuais de weblogs dos top20 liberais e top20 conservadores">
<node CREATED="1372280518857" ID="ID_1040629312" MODIFIED="1372280529262" TEXT="12470 liberais"/>
<node CREATED="1372280530374" ID="ID_241507276" MODIFIED="1372280585179" TEXT="10414 conservadores"/>
</node>
<node CREATED="1372280639490" ID="ID_454045220" MODIFIED="1372280778725" TEXT="an&#xe1;lise apenas a cita&#xe7;&#xe3;o ativa de outro blog pol&#xed;tico dentro do post (sem usar blogrolls)">
<node CREATED="1372280695856" ID="ID_1600191418" MODIFIED="1372280728369" TEXT="objetivo: monitorar as discuss&#xf5;es ativas entre os Blogs A-list"/>
<node CREATED="1372280779971" ID="ID_1458494366" MODIFIED="1372280790073" TEXT="sem posts duplicados "/>
</node>
</node>
</node>
<node CREATED="1372280807323" ID="ID_892036398" MODIFIED="1372616138788" POSITION="right" TEXT="Entendendo a An&#xe1;lise dos Dados">
<node CREATED="1372280819761" ID="ID_1794047168" MODIFIED="1372281016244" TEXT="Contagem do n&#xfa;mero de posts onde cada blog citava outro blog (uma citacao por post, mesmo que citado mais de uma vez)">
<node CREATED="1372280916497" ID="ID_503787963" MODIFIED="1372688355216" TEXT="liberais se citavam 1511 vezes">
<icon BUILTIN="yes"/>
<icon BUILTIN="idea"/>
</node>
<node CREATED="1372280968648" ID="ID_1158380764" MODIFIED="1372688359569" TEXT="Liberais citavam conservadores 247 vezes">
<icon BUILTIN="yes"/>
<icon BUILTIN="idea"/>
</node>
<node CREATED="1372280936895" ID="ID_1966310918" MODIFIED="1372688363044" TEXT="conservadores se citavam 2110 vezes">
<icon BUILTIN="yes"/>
<icon BUILTIN="idea"/>
</node>
<node CREATED="1372280984128" ID="ID_812455497" MODIFIED="1372688368046" TEXT="conservadores citavam liberais 312 vezes">
<icon BUILTIN="yes"/>
<icon BUILTIN="idea"/>
<icon BUILTIN="idea"/>
</node>
</node>
<node CREATED="1372281003804" ID="ID_912462498" MODIFIED="1372281013875" TEXT="Apenas 15% de cross-citacoes entre comunidades"/>
<node CREATED="1372281035209" ID="ID_898010511" MODIFIED="1372688495355" TEXT="conservadores postam menos que liberais (16% menos) mas fazem 40% mais citacoes que liberais">
<icon BUILTIN="button_cancel"/>
<node CREATED="1372281101380" ID="ID_453602709" MODIFIED="1372281126833" TEXT="0,20 links por post para conservadores">
<icon BUILTIN="help"/>
</node>
<node CREATED="1372281113243" ID="ID_508304917" MODIFIED="1372281131573" TEXT="0,12 links por post para liberais">
<icon BUILTIN="help"/>
</node>
</node>
<node CREATED="1372616138789" ID="ID_1038031689" MODIFIED="1372616562388" TEXT="For&#xe7;a da comunidade">
<node CREATED="1372616012178" ID="ID_537785509" MODIFIED="1372616050105" TEXT="Dos Top20 conservadores">
<node CREATED="1372616050106" ID="ID_1986468428" MODIFIED="1372688428131" TEXT="278 ligacoes internas (post citations)">
<icon BUILTIN="idea"/>
</node>
</node>
<node CREATED="1372616094115" ID="ID_1905187040" MODIFIED="1372616101661" TEXT="Dos Top20 liberais">
<node CREATED="1372616101662" ID="ID_937940906" MODIFIED="1372688431619" TEXT="218 liga&#xe7;oes internas (post citations)">
<icon BUILTIN="idea"/>
</node>
</node>
<node CREATED="1372616155265" ID="ID_1234950849" MODIFIED="1372616163489" TEXT="Entre comunidades">
<node CREATED="1372616163491" ID="ID_163395420" MODIFIED="1372688435373" TEXT="210 ligacoes">
<icon BUILTIN="idea"/>
</node>
</node>
<node CREATED="1372616232785" ID="ID_1444507966" MODIFIED="1372688444867" TEXT="mantendo apenas links reciprocos (com 5 ou mais citacoes em uma ou ambas dire&#xe7;oes">
<icon BUILTIN="button_cancel"/>
<node CREATED="1372616281550" ID="ID_1014926348" MODIFIED="1372616320537" TEXT="40 ligacoes internas entre a comunidade conservadora"/>
<node CREATED="1372616321387" ID="ID_335585692" MODIFIED="1372616333110" TEXT="25 ligacoes internas entre a comunidade liberal"/>
<node CREATED="1372616333717" ID="ID_1138006241" MODIFIED="1372616357359" TEXT="3 ligacoes entre comunidades (apenas)"/>
</node>
<node CREATED="1372616378484" ID="ID_138319818" MODIFIED="1372689254836" TEXT="mantendo apenas links com 25 citacoes ou mais ">
<icon BUILTIN="button_cancel"/>
<node CREATED="1372616449818" ID="ID_1599840602" MODIFIED="1372616463480" TEXT="23 ligacoes internas entre a comunidade conservadora"/>
<node CREATED="1372616473138" ID="ID_1083863005" MODIFIED="1372616482602" TEXT="12 ligacoes internas entre a comunidade liberal"/>
<node CREATED="1372616425277" ID="ID_149406188" MODIFIED="1372616433477" TEXT="comunidades totalmente separadas"/>
</node>
</node>
<node CREATED="1372617027595" ID="ID_648215690" MODIFIED="1372617169704" TEXT="Uniformidade das discussoes">
<node CREATED="1372617169706" FOLDED="true" ID="ID_119927763" MODIFIED="1372689390225" TEXT="Medida pela similaridade de conte&#xfa;do  entre cada par de blogs">
<icon BUILTIN="button_cancel"/>
<node CREATED="1372617209272" ID="ID_251407250" MODIFIED="1372617984786" TEXT="usada a m&#xe9;trica de similaridade do cosseno para verificar a similaridade de URLs e frases entre os posts  (an&#xe1;lise sem&#xe2;ntica) "/>
<node CREATED="1372617986836" ID="ID_1967336860" MODIFIED="1372618136831" TEXT="tirou-se as citacoes diretas entre blogs para evitar a similaridade obvia e buscar o eco de fontes externas diretamente e nao dentro dos pr&#xf3;prios blogs"/>
<node CREATED="1372618268649" ID="ID_853568847" MODIFIED="1372618329582" TEXT="Para mediar a similaridade textual entre blogs foram identificadas frases que sao mais informativas com respeito a um modelo de frequencias de termos nos blogs"/>
</node>
</node>
<node CREATED="1372618532796" ID="ID_720334866" MODIFIED="1372689404539" TEXT="Links com a midia tradicional (sites de noticias on-line)">
<icon BUILTIN="button_cancel"/>
<node CREATED="1372618571666" ID="ID_854234111" MODIFIED="1372618601713" TEXT="comunidade conservadora">
<node CREATED="1372618601715" ID="ID_1729836216" MODIFIED="1372618665591" TEXT="Fox News e National Review"/>
<node CREATED="1372618719839" ID="ID_1803210951" MODIFIED="1372618853267" TEXT="Dentre os Top Blogs de noticias politicos: NY Post, WSJ Opinion Journal e Washington Times"/>
</node>
<node CREATED="1372618666318" ID="ID_1355849075" MODIFIED="1372618671700" TEXT="comunidade liberal">
<node CREATED="1372618671702" ID="ID_1016518557" MODIFIED="1372618697724" TEXT="Salon (86% das citacoes recebidas advindas da comunidade liberal)"/>
<node CREATED="1372618757338" ID="ID_363786503" MODIFIED="1372619407710" TEXT="Dentre os Top Blogs de noticias politicos: LA Times, The New REpublic e Wall Street Journal"/>
</node>
</node>
<node CREATED="1372619415480" ID="ID_876929497" MODIFIED="1372689645263" TEXT="Ocorrencia de nomes de figuras pol&#xed;ticas">
<icon BUILTIN="button_cancel"/>
<icon BUILTIN="yes"/>
<node CREATED="1372619531256" ID="ID_831191727" MODIFIED="1372619537522" TEXT="comunidade conservadora">
<node CREATED="1372619537524" ID="ID_269511388" MODIFIED="1372619555153" TEXT="citam mais os democratas (criticando)"/>
<node CREATED="1372619555749" ID="ID_498208192" MODIFIED="1372619587048" TEXT="Dan Rather, Michael Moore, Yasser Arafat e Terry McAuliffe"/>
</node>
<node CREATED="1372619589534" ID="ID_1257555902" MODIFIED="1372619594855" TEXT="comunidade liberal">
<node CREATED="1372619594857" ID="ID_1747347643" MODIFIED="1372619605391" TEXT="citam mais os republicanos (criticando)"/>
<node CREATED="1372619605537" ID="ID_1995570978" MODIFIED="1372619632032" TEXT="Donald Rumsfeld, Colin Powell, Zell Miller e Tim Russert"/>
</node>
<node CREATED="1372619735627" ID="ID_1641428735" MODIFIED="1372619804716" TEXT="Os nomes foram extraidos por um extrator  e as ocorrencias do mesmo nome foram contadas. Na mao, foram verificadas diferentes variantes do mesmo nome e o nome dos autores dos 40 weblogs foram cortados da lista"/>
</node>
</node>
<node CREATED="1372275075716" ID="ID_1927347721" MODIFIED="1372277355384" POSITION="right" TEXT="Resultados">
<node CREATED="1372278655886" ID="ID_60094711" MODIFIED="1372278936098" TEXT="Estudo 1">
<node CREATED="1372276982060" ID="ID_929626577" MODIFIED="1372277053651" TEXT="Conservadores e Liberais possuem diferentes listas de fontes favoritas de not&#xed;cias, pessoas e t&#xf3;picos de discuss&#xe3;o"/>
<node CREATED="1372277004917" ID="ID_1362178451" MODIFIED="1372689738862" TEXT="Eventualmente, h&#xe1; sobreposi&#xe7;&#xe3;o de assuntos entre as comunidades nas discussoes de artigos jornal&#xed;sticos e eventos"/>
<node CREATED="1372277184208" ID="ID_229507245" MODIFIED="1372277203221" TEXT="A grande maioria dos links &#xe9; interno &#xe0;s comunidades">
<node CREATED="1372277942132" ID="ID_1459164031" MODIFIED="1372689661426" TEXT="91% dos links originados em uma comunidade  ficavam dentro da mesma">
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1372275092316" ID="ID_686400289" MODIFIED="1372277314451" TEXT="Conservadores se ligam entre si com maior  frequencia "/>
<node CREATED="1372275111657" ID="ID_1040903618" MODIFIED="1372277332705" TEXT="Conservadores possuem rede mais densa (se ligam a um maior n&#xfa;mero de blogs dentro da comunidade)">
<node CREATED="1372277994968" ID="ID_613601472" MODIFIED="1372689693525" TEXT="84% dos conservadores se ligam a pelo menos 1 blog e 82% recebem um link pelo menos e possuem m&#xe9;dia de 15,1  de ligacoes">
<icon BUILTIN="idea"/>
</node>
<node CREATED="1372278032901" ID="ID_981033658" MODIFIED="1372689703486" TEXT="74% dos liberais se ligam a pelo menos 1 blog e 67 recebem pelo menos um link e possuem m&#xe9;dia de 13,6 liga&#xe7;oes e alta propor&#xe7;&#xe3;o  de blogs sem nenhuma ligacao">
<icon BUILTIN="idea"/>
</node>
</node>
<node CREATED="1372278766931" ID="ID_1405596681" MODIFIED="1372278805560" TEXT="Um n&#xfa;mero pequenos de blogs recebe uma fra&#xe7;&#xe3;o significante da aten&#xe7;&#xe3;o (amostra para estudo 2)">
<node CREATED="1372278497108" ID="ID_1881883913" MODIFIED="1372689750978" TEXT="Maiores cita&#xe7;oes recebidas">
<icon BUILTIN="idea"/>
<node CREATED="1372278415827" ID="ID_1677882737" MODIFIED="1372278532998" TEXT="Links liberais ">
<node CREATED="1372278452751" ID="ID_151972306" MODIFIED="1372278461665" TEXT="Daily Kos">
<node CREATED="1372278461666" ID="ID_78874330" MODIFIED="1372278467222" TEXT="338 links"/>
</node>
<node CREATED="1372278469919" ID="ID_1379240294" MODIFIED="1372278476331" TEXT="Eschaton">
<node CREATED="1372278476333" ID="ID_542331581" MODIFIED="1372278482436" TEXT="264 links"/>
</node>
</node>
<node CREATED="1372278483368" ID="ID_1302335732" MODIFIED="1372278557314" TEXT="Links conservadores">
<node CREATED="1372278559786" ID="ID_1848510004" MODIFIED="1372689757827" TEXT="Instapundit">
<node CREATED="1372278575833" ID="ID_736966565" MODIFIED="1372278587711" TEXT="277 links"/>
</node>
</node>
</node>
</node>
<node CREATED="1372619466003" ID="ID_709449937" MODIFIED="1372619720637" TEXT="Os interesses na midia tradicional de noticias dos 1400 blogs refletem os mesms dos 40 TopBlogs do Estudo 2"/>
</node>
<node CREATED="1372616990339" ID="ID_1740493057" MODIFIED="1372616995827" TEXT="Estudo 2">
<node CREATED="1372616566933" ID="ID_472054649" MODIFIED="1372616973196" TEXT="A comunidade conservadora possui uma estrutura mais densa de conexoes fortes que a comunidade liberal embora alguns poucos blogs liberais possuam excepcionalmente conexoes reciprocas fortes"/>
<node CREATED="1372617714985" ID="ID_109181930" MODIFIED="1372618256189" TEXT="retirando as URLs de blogs pol&#xed;ticos, a similaridade dentro das comunidades &#xe9; mais ou menos a mesma j&#xe1; a similaridade entre comunidades &#xe9; mais baixa"/>
<node CREATED="1372618333022" ID="ID_669482584" MODIFIED="1372618384395" TEXT="Alguns termos ou frases tinham a leve tendencia de serem usados mais numa comunidade que em outra"/>
<node CREATED="1372618418969" ID="ID_1203142603" MODIFIED="1372618456574" TEXT="Existe uma grande uniformidade de conte&#xfa;do nas referencias externas de cada comunidade"/>
<node CREATED="1372618458087" ID="ID_1397187545" MODIFIED="1372618513670" TEXT="Os blogs tanto liberais quanto conservadores agem como camaras de eco por discutir frequentemente itens separados de not&#xed;cias e webpages"/>
<node CREATED="1372619637143" ID="ID_1292878087" MODIFIED="1372619697038" TEXT="Os top40 blogs afirmam suas posi&#xe7;oes criticando aquelas figuras pol&#xed;ticas que eles desgotam"/>
</node>
</node>
<node CREATED="1372274743320" ID="ID_721239766" MODIFIED="1372274770719" POSITION="left" TEXT="Amostra">
<node CREATED="1371813820910" ID="ID_51452916" MODIFIED="1371813833780" TEXT="Blogs Conservadores"/>
<node CREATED="1371813849833" ID="ID_466652849" MODIFIED="1371813860347" TEXT="Blogs Liberais"/>
<node CREATED="1372619833305" ID="ID_1642412104" MODIFIED="1372619844486" TEXT="Excluidos blogs moderados e independentes"/>
</node>
</node>
</map>
