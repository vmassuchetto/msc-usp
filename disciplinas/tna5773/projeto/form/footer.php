  <script src="js/vendor/zepto.js"></script>
  <script src="js/vendor/jquery.js"></script>
  <script src="js/vendor/jquery-ui.js"></script>
  <script src="js/foundation.min.js"></script>
  <script type="text/javascript">
  /* <![CDATA[ */
    var messages = {
        'sending': '<?php echo addslashes( _('Enviando dados...') ); ?>',
        'field': '<?php echo addslashes( _('Você não selecionou o campo') ); ?>',
        'select3': '<?php echo addslashes( _('Selecione três perfis comportamentais.') ); ?>',
        'intensity': '<?php echo addslashes( _('Selecione uma intensidade para todos os perfis selecionados.') ); ?>',
        'problem': '<?php echo addslashes( _('Houve um problema ao enviar os dados. Por favor tente mais tarde. Desculpe o inconveniente.') ); ?>'
    }
  /* ]]> */
  </script>
  <script src="js/form.js"></script>
  <script>
    $(document).foundation();
  </script>
</body>
</html>

